import { IScreenShoot, IResource } from 'Common/Models';

/*
 * Actions
 */
export const screenShootsPageActions = {
  LOAD_SCREEN_SHOOTS: 'LOAD_SCREEN_SHOOTS',
  LOAD_SCREEN_SHOOTS_AVAILABLE_IMAGES: 'LOAD_SCREEN_SHOOTS_AVAILABLE_IMAGES',
  EDIT_SCREEN_SHOOT: 'EDIT_SCREEN_SHOOT',
  SAVE_SCREEN_SHOOT: 'SAVE_SCREEN_SHOOT',
  DELETE_SCREEN_SHOOT: 'DELETE_SCREEN_SHOOT'
};

export interface IScreenShootsPageActions {
  loadScreenShoots: () => Promise<IScreenShoot[]>;
  loadScreenShootsAvailableImages: () => Promise<IResource[]>;
  editScreenShoot: (screenShoot: IScreenShoot) => void;
  saveScreenShoot: (screenShoot: IScreenShoot) => Promise<IScreenShoot>;
  deleteScreenShoot: (screenShoot: IScreenShoot) => Promise<string>;
}
