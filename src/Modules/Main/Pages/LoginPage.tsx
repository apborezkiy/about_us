import * as _ from 'lodash';
import * as React from 'react';
import * as Redux from 'redux';
import * as ReactRedux from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router';
import { Accordion, Button, Carousel, Col, ControlLabel, FormControl } from 'react-bootstrap';
import { FormGroup, Grid, HelpBlock, Image, Media, Panel, Row } from 'react-bootstrap';

import { i18n } from 'Core/Utils';
import { IValidationState, IValidation } from 'Core/Models/Validation';
import { IUser, TUserKey } from 'Common/Models';

import { ILoginPageActions } from 'Modules/Main/Redux/Actions/ILoginPageActions';
import { IAppStore } from 'Modules/store';
import { LoginPageActions } from 'Modules/Main/Redux/Actions/LoginPageActions';
import { loginPageService } from 'Modules/Main/Services/LoginPageService';
import { ECompositePages, EMainPages } from 'Modules/Main/Pages';
import { MainMenuActions } from 'Modules/Main/Redux/Actions/MainMenuActions';

/*
 * Передаваемые параметры
 */
interface IProps {
  isLoggedIn: boolean;
  user: IUser;
  actions: ILoginPageActions;
  redirectToMainPage: () => void;
}

/*
 * Внутреннее состояние
 */
interface IState {
  user: IUser;
  validation: IValidation<IUser>;
  isSended: boolean;
}

/*
 * Страница входа для администрирования
 */
class CLoginPage extends React.Component<RouteComponentProps<IProps> & IProps, IState> {

  state: IState = {
    user: {} as IUser,
    validation: {
      email: { pattern: /.+/i, required: true } as IValidationState,
      password: { pattern: /.+/i, required: true } as IValidationState
    },
    isSended: false
  };

  handleLogin = () => {
    let formValid = true;
    for (let field of _.keys(this.state.validation) as TUserKey[]) {
      formValid = formValid && this.handleInput(field);
    }

    if (formValid) {
      this.props.actions.login(this.state.user as IUser)
        .then(() => {
          this.setState({ isSended: true });

          // перенаправить на главную
          this.props.redirectToMainPage();
        })
    }
  };

  handleInput = (key: TUserKey, e?) => {
    let user = _.assign({}, this.state.user);
    user[key] = e ? e.target.value : user[key];
    let validation = _.assign({}, this.state.validation);
    let validationField = validation[key];
    let isValid = true;

    if (validationField.required && (!user[key] || user[key].length === 0)) {
      isValid = false;
      validationField.state = 'warning';
      validationField.text = i18n.t('Required field', 'Обязательное поле');
    } else {
      isValid = validationField.pattern.test(user[key]);
      validationField.state = isValid ? 'success' : 'error';
      validationField.text = isValid ? '' : i18n.t('Incorrect field', 'Некорректное поле');
    }

    this.setState({user, validation});

    return isValid;
  };

  /*
   * Login form
   */
  renderEmailInput () {
    return (
      <FormGroup className="contacts-page__form-item"
                 controlId="formHorizontalEmail"
                 validationState={this.state.validation.email.state}>
        <Col componentClass={ControlLabel}>
          {i18n.t('Email')}
        </Col>
        <Col>
          <FormControl className="contacts-page__form-item-email"
                       type="text"
                       placeholder={i18n.t('Email')}
                       onInput={e => this.handleInput('email', e)} />
          <HelpBlock>{this.state.validation.email.text}</HelpBlock>
        </Col>
      </FormGroup>
    );
  }

  renderPasswordInput () {
    return (
      <FormGroup className="login-page__form-item"
                 controlId="formControlsTextarea"
                 validationState={this.state.validation.password.state}>
        <Col componentClass={ControlLabel}>
          {i18n.t('Password', 'Пароль')}
        </Col>
        <Col>
          <FormControl className="login-page__form-item-password"
                       type="password"
                       placeholder={i18n.t('Password', 'Пароль')}
                       onInput={e => this.handleInput('password', e)} />
          <HelpBlock>{this.state.validation.password.text}</HelpBlock>
        </Col>
      </FormGroup>
    );
  }

  renderSubmitButton () {
    return (
      <FormGroup className="login-page__form-item">
        <Col>
          <Button type="submit" onClick={this.handleLogin}>
            {i18n.t('Login', 'Войти')}
          </Button>
        </Col>
      </FormGroup>
    );
  }

  renderLoginForm () {
    if (!this.state.isSended) {
      return (
        <form className="login-page__form">
          {this.renderEmailInput()}
          {this.renderPasswordInput()}
          {this.renderSubmitButton()}
        </form>
      );
    } else {
      return (
        <form className="login-page__form">
          <div className="login-page__form-notification">
            {i18n.t('You logged in successfully!', 'Вы вошли в систему!')}
          </div>
        </form>
      )
    }
  }

  // отрисовка
  render () {
    return (
      <div className="login-page">
        <div className="login-page__title">

        </div>
        <div className="login-page__body">
          {this.renderLoginForm()}
        </div>
      </div>
    )
  }
}

/*
 * Контейнер
 */
const mapStateToProps = function (state: IAppStore): IProps {
  const isLoggedIn = state.main.loginPage.isLoggedIn;
  const user = state.main.loginPage.user;

  return { isLoggedIn, user } as IProps;
};

const mapDispatchToProps = function (dispatch: Redux.Dispatch<IAppStore>): IProps {
  const actions: ILoginPageActions = new LoginPageActions(loginPageService, dispatch);
  const redirectToMainPage = () => new MainMenuActions(null, dispatch).navigateMainMenu(EMainPages.MAIN_PAGE as ECompositePages);

  return { actions, redirectToMainPage } as any as IProps;
};

const LoginPage = withRouter(ReactRedux.connect(mapStateToProps, mapDispatchToProps)(CLoginPage) as React.ComponentClass<RouteComponentProps<IProps>>);

export { LoginPage };
