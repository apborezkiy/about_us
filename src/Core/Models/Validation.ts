export interface IValidationState {
  state: string;
  text: string;
  pattern: RegExp;
  required: boolean;
}

export type IValidation<T> = {
  [key in keyof Partial<T>]: IValidationState;
}
