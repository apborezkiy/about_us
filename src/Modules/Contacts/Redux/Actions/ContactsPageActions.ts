import { push } from 'connected-react-router';

import { GenericActions } from 'Core/Generic';
import { ActionUtils } from 'Core/Redux';
import { IOffer } from 'Common/Models';

import { IContactsPageActions, contactsPageActions } from 'Modules/Contacts/Redux/Actions/IContactsPageActions';
import { IContactsPageService } from 'Modules/Contacts/Services/IContactsPageService';

/*
 * Действия страницы контактов
 */
class ContactsPageActions extends GenericActions implements IContactsPageActions {
  constructor (private api: IContactsPageService, dispatch) {
    super(dispatch);
  }

  // Оформить предложение
  createOffer (offer: IOffer): Promise<void> {
    const createOffer = () => this.api.createOffer(offer).catch(this.redirectError);
    return ActionUtils.dispatchAsyncResponseBound<void>(this.dispatch, contactsPageActions.CREATE_OFFER, createOffer);
  }
}

export { ContactsPageActions };
