import * as _ from 'lodash';
import * as Redux from 'redux';
import * as ReduxActions from 'redux-actions';

import { SUCCESS } from 'Core/Models/Actions';
import { ILoginPageStore } from 'Modules/Main/Redux/Store';
import { loginPageActions } from 'Modules/Main/Redux/Actions/ILoginPageActions';

import { ERoles, IUser } from 'Common/Models';

/*
 * Редьюсер страницы
 */
export const loginPageReducer: Redux.Reducer<ILoginPageStore> = ReduxActions.handleActions({
  [`${loginPageActions.LOGIN}_${SUCCESS}`]: (state: ILoginPageStore, action: ReduxActions.Action<IUser>) => {
    return _.assign({}, state, { isLoggedIn: true, user: action.payload });
  },
  [`${loginPageActions.LOGOUT}_${SUCCESS}`]: (state: ILoginPageStore, action: ReduxActions.Action<IUser>) => {
    return _.assign({}, state, { isLoggedIn: false, user: { role: ERoles.guest } });
  },
  [`${loginPageActions.GET_USER}_${SUCCESS}`]: (state: ILoginPageStore, action: ReduxActions.Action<IUser>) => {
    return _.assign({}, state, { isLoggedIn: true, user: action.payload });
  }
}, {})

/*
 * Начальное состояние страницы
 */
export const loginPageInitialStore: ILoginPageStore = {
  isLoggedIn: false,
  user: {
    role: ERoles.guest
  } as IUser
}
