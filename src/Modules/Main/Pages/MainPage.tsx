import * as _ from 'lodash';
import * as React from 'react';
import * as Redux from 'redux';
import * as ReactRedux from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router';
import { Grid, Row, Col, Image, Media, Button, FormGroup, FormControl, ControlLabel, Carousel, Accordion, Panel } from 'react-bootstrap';

import { i18n } from 'Core/Utils';

import { IMainPageActions } from 'Modules/Main/Redux/Actions/IMainPageActions';
import { IAppStore } from 'Modules/store';
import { MainPageActions } from 'Modules/Main/Redux/Actions/MainPageActions';
import { mainPageService } from 'Modules/Main/Services/MainPageService';

/*
 * Передаваемые параметры
 */
interface IProps {
  actions: IMainPageActions;
}

/*
 * Главная страница
 */
class CMainPage extends React.Component<RouteComponentProps<IProps> & IProps> {

  renderAvatar() {
    return (
      <Grid>
        <Row>
          <Col className="main-page__carousel-wrapper">
            <div className="main-page__carousel">
              <Carousel>
                <Carousel.Item>
                  <Image width={500} height={200} className="main-page__photo-avatar" src="assets/avatar.jpg" rounded />
                  <Carousel.Caption>
                    <h3>First slide label</h3>
                    <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                  </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                  <Image width={500} height={200} className="main-page__photo-avatar" src="assets/avatar.jpg" rounded />
                  <Carousel.Caption>
                    <h3>Second slide label</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                  </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                  <Image width={500} height={200} className="main-page__photo-avatar" src="assets/avatar.jpg" rounded />
                  <Carousel.Caption>
                    <h3>Third slide label</h3>
                    <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                  </Carousel.Caption>
                </Carousel.Item>
              </Carousel>
            </div>
          </Col>
        </Row>
      </Grid>
    )
  }

  renderPiterDruker() {
    let config = [
      { header: 'What is our mission', text: 'Our mission' },
      { header: 'Who is our customer', text: 'Our customer' },
      { header: 'What does our customer value', text: 'Customer value' },
      { header: 'What results do we seek', text: 'Results' },
      { header: 'What is our plan', text: 'Our plan' },
    ];

    return (
      <Accordion>
        {_.map(config, (item, i) =>
          <Panel header={item.header} eventKey={i} key={i}>
            {item.text}
          </Panel>
        )}
      </Accordion>
    )
  }

  // отрисовка
  render () {
    return (
      <div className="main-page">
        <div className="main-page__title">
          {this.renderAvatar()}
        </div>
        <div className="main-page__body">
          {this.renderPiterDruker()}
        </div>
      </div>
    )
  }
}

/*
 * Контейнер
 */
const mapStateToProps = function (state: IAppStore): IProps {
  return { } as IProps;
};

const mapDispatchToProps = function (dispatch: Redux.Dispatch<IAppStore>): IProps {
  const actions: IMainPageActions = new MainPageActions(mainPageService, dispatch);

  return { actions } as IProps;
};

const MainPage = withRouter(ReactRedux.connect(mapStateToProps, mapDispatchToProps)(CMainPage) as React.ComponentClass<RouteComponentProps<IProps>>);

export { MainPage };
