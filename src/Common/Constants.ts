import { ERoles, EPriorities, ETechnologyKind } from 'Common/Models'
import { i18n } from  'Core/Utils'

export const localStorageSections = {
  session: 'session'
}

export const getRoleName = (role: ERoles) => {
  let roles = {
    [ERoles.guest]: i18n.t('guest', 'Гость'),
    [ERoles.member]: i18n.t('member', 'Сотрудник'),
    [ERoles.moderator]: i18n.t('moderator', 'Модератор'),
    [ERoles.admin]: i18n.t('admin', 'Администратор')
  };

  return roles[role];
}

export const getPriorityName = (priority: EPriorities) => {
  let priorities = {
    [EPriorities.low]: i18n.t('low', 'низкий'),
    [EPriorities.medium]: i18n.t('medium', 'средний'),
    [EPriorities.high]: i18n.t('high', 'высокий'),
    [EPriorities.highest]: i18n.t('highest', 'наивысший')
  };

  return priorities[priority];
}

export const getTechnologyKindName = (kind: ETechnologyKind) => {
  let names = {
    [ETechnologyKind.FRONT]: i18n.t('front-end'),
    [ETechnologyKind.BACK]: i18n.t('back-end')
  };

  return names[kind];
}
