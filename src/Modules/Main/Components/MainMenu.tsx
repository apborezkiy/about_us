import * as _ from 'lodash';
import * as React from 'react';
import * as Redux from 'redux';
import * as ReactRedux from 'react-redux';
import { Button, Navbar, Nav, NavItem, NavDropdown, MenuItem, Image } from 'react-bootstrap';

import { i18n, pageToRouteInfo } from 'Core/Utils';

import { IUser } from 'Common/Models';

import { ECompositePages, EMainPages, mainRoutes } from 'Modules/Main/Pages';
import { EProjectsPages, projectsRoutes } from 'Modules/Projects/Pages';
import { EExperiencePages, experienceRoutes } from 'Modules/Experience/Pages';
import { EUsersPages, usersRoutes } from 'Modules/Users/Pages';
import { contactsRoutes, EContactsPages } from 'Modules/Contacts/Pages';
import { IMainMenuActions } from 'Modules/Main/Redux/Actions/IMainMenuActions';
import { MainMenuActions } from 'Modules/Main/Redux/Actions/MainMenuActions';
import { ILoginPageActions } from 'Modules/Main/Redux/Actions/ILoginPageActions';
import { LoginPageActions } from 'Modules/Main/Redux/Actions/LoginPageActions';
import { loginPageService } from 'Modules/Main/Services/LoginPageService';
import { IAppStore } from 'Modules/store';

/*
 * Передаваемые параметры
 */
interface IProps {
  actions: IMainMenuActions;
  loginActions: ILoginPageActions;
  route: string;
  user: IUser;
  isLoggedIn: string;
}

/*
 * Главное меню
 */
class CMainMenu extends React.Component<IProps> {

  handleNavigate (page: ECompositePages) {
    this.props.actions.navigateMainMenu(page);
  }

  handleLogout = () => {
    this.props.loginActions.logout(this.props.user);
  }

  renderAdminMenuItem = (i: number, page: ECompositePages, j: number) => {
    let routeInfo = pageToRouteInfo<ECompositePages>(page, _.assign({}, mainRoutes, projectsRoutes, experienceRoutes, contactsRoutes, usersRoutes));
    let isActive = this.props.route === routeInfo.path;

    let index = i * 10 + j;

    return (
      <MenuItem className={isActive ? 'active' : ''} key={index} eventKey={index} onClick={this.handleNavigate.bind(this, [page])}>{routeInfo.name}</MenuItem>
    )
  }

  renderMenuItem = (page: ECompositePages, i: number) => {
    let routeInfo = pageToRouteInfo<ECompositePages>(page, _.assign({}, mainRoutes, projectsRoutes, experienceRoutes, contactsRoutes));
    let isActive = this.props.route === routeInfo.path;

    return (
      <NavItem className={isActive ? 'active' : ''} key={i} eventKey={i} onClick={this.handleNavigate.bind(this, [page])}>{routeInfo.name}</NavItem>
    )
  }

  renderProfileActions = () => {
    let loginPage = EMainPages.LOGIN_PAGE;
    let routeInfo = pageToRouteInfo<ECompositePages>(loginPage as ECompositePages, _.assign({}, mainRoutes));

    if (!this.props.isLoggedIn) {
      return (
        <Nav pullRight>
          <NavItem eventKey={1} onClick={this.handleNavigate.bind(this, [loginPage])}>{routeInfo.name}</NavItem>
        </Nav>
      );
    } else {
      return (
        <Nav pullRight>
          <NavItem eventKey={2} onClick={this.handleLogout}>{i18n.t('Logout', 'Выйти')}</NavItem>
        </Nav>
      );
    }
  }

  renderCommonActions = () => {
    const pages = [
      EMainPages.MAIN_PAGE,
      EExperiencePages.EXPERIENCE_LIST_PAGE,
      EProjectsPages.PROJECTS_LIST_PAGE,
      EContactsPages.CONTACTS_PAGE
    ];

    return _.map(pages, this.renderMenuItem);
  }

  renderAdminActions = () => {
    const pages = [
      EUsersPages.USER_LIST_PAGE,
      EExperiencePages.TECHNOLOGY_LIST_PAGE,
      EProjectsPages.SCREENSHOOTS_LIST_PAGE
    ];

    if (this.props.isLoggedIn) {
      return (
        <NavDropdown eventKey={pages.length} title={i18n.t('Administration', 'Администрирование')} id="main-menu__nav-dropdown">
          {_.map(pages, this.renderAdminMenuItem.bind(this, pages.length))}
          <MenuItem divider />
          <MenuItem key={-1} eventKey={-1}>{this.props.user.name}</MenuItem>
        </NavDropdown>
      )
    }
  }

  // отрисовка
  render () {
    return (
      <div className="main-menu">
        <Navbar inverse collapseOnSelect>
          <Navbar.Header>
            <Navbar.Brand>
              <Image className="main-menu__avatar" src="assets/logo-light.png" onClick={this.handleNavigate.bind(this, [EMainPages.MAIN_PAGE])} />
            </Navbar.Brand>
            <Navbar.Toggle />
          </Navbar.Header>
          <Navbar.Collapse>
            <Nav>
              {this.renderCommonActions()}
              {this.renderAdminActions()}
            </Nav>
            {this.renderProfileActions()}
          </Navbar.Collapse>
        </Navbar>
      </div>
    )
  }
}

/*
 * Контейнер
 */
const mapStateToProps = function (state: IAppStore): IProps {
  const route = (state as any).router.location.pathname;
  const isLoggedIn = state.main.loginPage.isLoggedIn;
  const user = state.main.loginPage.user;

  return { route, isLoggedIn, user } as any as IProps;
}

const mapDispatchToProps = function (dispatch: Redux.Dispatch<IAppStore>): IProps {
  const actions: IMainMenuActions = new MainMenuActions(null, dispatch);
  const loginActions: ILoginPageActions = new LoginPageActions(loginPageService, dispatch);

  return { actions, loginActions } as IProps;
}

const MainMenu = ReactRedux.connect(mapStateToProps, mapDispatchToProps)(CMainMenu);

export { MainMenu };
