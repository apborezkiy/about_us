import { IUser } from 'Common/Models';

/*
 * API
 */
export interface ILoginPageService {
  login: (user: IUser) => Promise<IUser>;
  logout: (session: string) => Promise<void>;
  getUser: (session: string) => Promise<IUser>;
}
