# Our Site

## Based on

- [Typescript](https://www.typescriptlang.org/)
- [React](https://facebook.github.io/react/)
- [React Bootstrap](https://react-bootstrap.github.io/)
- [React Router 4](https://www.npmjs.com/package/react-router4)
- [Redux](https://github.com/reactjs/redux/)
- [Less](http://lesscss.org/)
- [Webpack 2](https://webpack.github.io/)

## Dev tools

- [React Dev Tools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi)
- [Redux DevTools Extension](https://github.com/zalmoxisus/redux-devtools-extension)

## Setup

Install node.js from here: https://nodejs.org/en/

In the root of the project:
```
$ npm install
```

## Running

```
$ npm start
```

## Deployment

```
$ npm run build-dev
$ npm run upload-dev
```

On the server:
```
$ cd ~/team-site
$ tar -vxf ./build.tar
```

Open Chrome and press CTRL + F5.

Ready.
