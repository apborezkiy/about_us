import { i18n } from 'Core/Utils';

/*
 * Сраницы модуля
 */
export enum EContactsPages {
  LAYOUT = 30,
  CONTACTS_PAGE = 31
}

export const contactsRoutes = {
  [EContactsPages.LAYOUT]: { path: '/main/contacts', name: i18n.t('Contacts and feedback layout', 'Раздел контактной информации и обратной связи') },
  [EContactsPages.CONTACTS_PAGE]: { path: '/main/contacts', name: i18n.t('Contacts', 'Контакты') }
}

import { ContactsPage } from 'Modules/Contacts/Pages/ContactsPage';
import { ContactsLayout } from 'Modules/Contacts/Pages/ContactsLayout';

/*
 * Компоненты страниц
 */
export {
  ContactsLayout,
  ContactsPage
};
