import * as _ from 'lodash';
import * as React from 'react';
import * as Redux from 'redux';
import * as ReactRedux from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router';
import { Grid, Row, Col, Image, Media, Button } from 'react-bootstrap';
import { FormGroup, FormControl, ControlLabel, HelpBlock } from 'react-bootstrap';

import { i18n } from 'Core/Utils';
import { IValidationState, IValidation } from 'Core/Models/Validation';

import { TUserKey, IUser, ERoles, EPriorities } from 'Common/Models';
import { getRoleName, getPriorityName } from 'Common/Constants';
import { RadioGroup, IRadioGroupSelectOption } from 'Common/Components/RadioGroup';

import { IUsersPageActions } from 'Modules/Users/Redux/Actions/IUsersPageActions';
import { UsersPageActions } from 'Modules/Users/Redux/Actions/UsersPageActions';
import { usersPageService } from 'Modules/Users/Services/UsersPageService';
import { IAppStore } from 'Modules/store';

type TRoleRadioGroup = new() => RadioGroup<ERoles>;
const RoleRadioGroup = RadioGroup as TRoleRadioGroup;

type TPriorityRadioGroup = new() => RadioGroup<EPriorities>;
const PriorityRadioGroup = RadioGroup as TPriorityRadioGroup;

/*
 * Передаваемые параметры
 */
interface IProps {
  accounts: IUser[]; // users list
  account: IUser; // user to edit
  actions: IUsersPageActions; // CRUD actions
  user: IUser; // current user
}

/*
 * Внутреннее состояние
 */
interface IState {
  account: IUser;
  validation: IValidation<IUser>;
  isSended: boolean;
}

/*
 * Отображения для RadioGroup
 */
function getRoleOptions (): IRadioGroupSelectOption<ERoles>[] {
  return _.map([ERoles.admin, ERoles.moderator, ERoles.member], (value: ERoles, index: number) => {
    return {
      id: index,
      label: getRoleName(value),
      value: value
    };
  });
}

function getRoleOption (value: ERoles): IRadioGroupSelectOption<ERoles> {
  return _.find(getRoleOptions(), (selector: IRadioGroupSelectOption<ERoles>) => selector.value === value);
}

function getPriorityOptions (): IRadioGroupSelectOption<EPriorities>[] {
  return _.map([EPriorities.low, EPriorities.medium, EPriorities.high, EPriorities.highest], (value: EPriorities, index: number) => {
    return {
      id: index,
      label: getPriorityName(value),
      value: value
    };
  });
}

function getPriorityOption (value: EPriorities): IRadioGroupSelectOption<EPriorities> {
  return _.find(getPriorityOptions(), (selector: IRadioGroupSelectOption<EPriorities>) => selector.value === value);
}

/*
 * Cтраница редактирования пользователя.
 */
class CUsersEditPage extends React.Component<RouteComponentProps<IProps> & IProps, IState> {

  state: IState = {
    account: {} as IUser,
    validation: {
      name: { pattern: /.+/i, required: true } as IValidationState,
      email: { pattern: /.+/i, required: true } as IValidationState,
      role: { pattern: /.+/i, required: true } as IValidationState,
      priority: { pattern: /.+/i, required: true } as IValidationState
    },
    isSended: false
  };

  componentWillMount () {
    this.setState({ account: _.assign({}, this.state.account, this.props.account) });

    if (this.props.accounts.length === 0) {
      this.props.actions.loadUsers();
    }
  }

  handleSubmit = () => {
    let formValid = true;
    for (let field of _.keys(this.state.validation) as TUserKey[]) {
      formValid = formValid && this.handleInput(field);
    }

    if (formValid) {
      let account: IUser = _.assign({}, this.props.account, this.state.account);
      this.props.actions.saveUser(account)
        .then(() => this.setState({ isSended: true }));
    }
  };

  handleInput = (key: TUserKey, e?) => {
    let account = _.assign({}, this.state.account);
    account[key] = e ? e.target.value : account[key];
    let validation = _.assign({}, this.state.validation);
    let validationFiled = validation[key];
    let isValid = true;

    if (validationFiled.required && (!account[key] || account[key].length === 0)) {
      isValid = false;
      validationFiled.state = 'warning';
      validationFiled.text = i18n.t('Required field', 'Обязательное поле');
    } else {
      isValid = validationFiled.pattern.test(account[key]);
      validationFiled.state = isValid ? 'success' : 'error';
      validationFiled.text = isValid ? '' : i18n.t('Incorrect field', 'Некорректное поле');
    }

    this.setState({ account, validation });

    return isValid;
  };

  renderNameInput () {
    return (
      <FormGroup className="users-page__form-item"
        controlId="formHorizontalUsers"
        validationState={this.state.validation.name.state}>
        <Col componentClass={ControlLabel}>
          {i18n.t('User', 'Пользователь')}
        </Col>
        <Col>
          <FormControl className="users-page__form-item-users"
            type="text"
            defaultValue={this.props.account.name}
            onInput={e => this.handleInput('name', e)}/>
          <HelpBlock>{this.state.validation.name.text}</HelpBlock>
        </Col>
      </FormGroup>
    );
  }

  renderEmailInput () {
    return (
      <FormGroup className="users-page__form-item"
        controlId="formHorizontalUsers"
        validationState={this.state.validation.email.state}>
        <Col componentClass={ControlLabel}>
          {i18n.t('Email', 'Электронная почта')}
        </Col>
        <Col>
          <FormControl className="users-page__form-item-users"
            type="text"
            defaultValue={this.props.account.email}
            onInput={e => this.handleInput('email', e)}/>
          <HelpBlock>{this.state.validation.email.text}</HelpBlock>
        </Col>
      </FormGroup>
    );
  }

  renderRoleInput () {
    return (
      <FormGroup className="users-page__form-item"
        controlId="formHorizontalUsers"
        validationState={this.state.validation.role.state}>
        <Col componentClass={ControlLabel}>
          {i18n.t('Role', 'Роль')}
        </Col>
        <RoleRadioGroup
          option={getRoleOption(this.state.account.role)}
          options={getRoleOptions()}
          callback={e => this.handleInput('role', e)}
        />
      </FormGroup>
    );
  }

  renderPriorityInput () {
    return (
      <FormGroup className="users-page__form-item"
                 controlId="formHorizontalPriority"
                 validationState={this.state.validation.priority.state}>
        <Col componentClass={ControlLabel}>
          {i18n.t('Priority', 'Приоритет')}
        </Col>
        <PriorityRadioGroup
          option={getPriorityOption(this.state.account.priority)}
          options={getPriorityOptions()}
          callback={e => this.handleInput('priority', e)}
        />
      </FormGroup>
    );
  }

  renderSubmitButton () {
    if (this.props.user.role !== ERoles.guest) {
      return (
        <FormGroup className="users-page__form-item">
          <Col>
            <Button type="submit" onClick={this.handleSubmit}>
              {i18n.t('Submit', 'Отправить')}
            </Button>
          </Col>
        </FormGroup>
      );
    }
  }

  renderTitle () {
    return (
      <FormGroup className="users-page__form-item">
        <Col componentClass={ControlLabel}>
          {i18n.t('Team account', 'Участник команды')}
        </Col>
      </FormGroup>
    );
  }

  renderContactsForm () {
    if (!this.state.isSended) {
      return (
        <form className="users-page__form">
          {this.renderTitle()}
          {this.renderNameInput()}
          {this.renderEmailInput()}
          {this.renderRoleInput()}
          {this.renderPriorityInput()}
          {this.renderSubmitButton()}
        </form>
      );
    } else {
      return (
        <form className="users-page__form">
          <div className="users-page__form-notification">
            {i18n.t('Data sended successfully!', 'Данные успешно отправлены!')}
          </div>
        </form>
      );
    }
  }

  // отрисовка
  render () {
    return (
      <div className="users-page">
        <div className="users-page__title">

        </div>
        <div className="users-page__body">
          {this.renderContactsForm()}
        </div>
      </div>
    );
  }
}

/*
 * Контейнер
 */
const mapStateToProps = function (state: IAppStore): IProps {
  const { account, users } = state.users.usersPage;
  const { user } = state.main.loginPage;
  const accounts = users;

  return { account, accounts, user } as IProps;
};

const mapDispatchToProps = function (dispatch: Redux.Dispatch<IAppStore>): IProps {
  const actions: IUsersPageActions = new UsersPageActions(usersPageService, dispatch);

  return { actions } as IProps;
};

const UsersEditPage = withRouter(ReactRedux.connect(mapStateToProps, mapDispatchToProps)(CUsersEditPage) as React.ComponentClass<RouteComponentProps<IProps>>);

export { UsersEditPage };
