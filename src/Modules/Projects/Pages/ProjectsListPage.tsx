import * as _ from 'lodash';
import * as React from 'react';
import * as Redux from 'redux';
import * as ReactRedux from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import { Grid, Row, Col, Image, Media, Button, Form, FormGroup, ControlLabel, FormControl } from 'react-bootstrap';

import { i18n } from 'Core/Utils';
import { pageToRouteInfo } from 'Core/Utils';

import { IProject, IUser, ERoles } from 'Common/Models';
import { IProjectsPageActions } from 'Modules/Projects/Redux/Actions/IProjectsPageActions';
import { ProjectsPageActions } from 'Modules/Projects/Redux/Actions/ProjectsPageActions';
import { projectsPageService } from 'Modules/Projects/Services/ProjectsPageService';
import { projectsRoutes, EProjectsPages } from 'Modules/Projects/Pages';
import { IAppStore } from 'Modules/store';

const projectsViewPage = pageToRouteInfo<EProjectsPages>(EProjectsPages.PROJECTS_VIEW_PAGE, projectsRoutes);

/*
 * Передаваемые параметры
 */
interface IProps {
  actions: IProjectsPageActions;
  projects: IProject[];
  user: IUser;
}

/*
 * Cтраница проектов
 */
class CProjectsListPage extends React.Component<RouteComponentProps<IProps> & IProps> {

  componentWillMount() {
    if (this.props.projects.length === 0) {
      this.props.actions.loadProjects();
    }
  }

  //
  // handlers
  //
  handleCreate () {
    this.props.actions.editProject({} as IProject);
  }

  handleEdit (id: string) {
    this.props.actions.editProject(_.find(this.props.projects, ((project) => project.id === id)));
  }

  handleView (id: string) {
    this.props.actions.viewProject(_.find(this.props.projects, ((project) => project.id === id)));
  }

  handleDelete (id: string) {
    this.props.actions.deleteProject(_.find(this.props.projects, ((project) => project.id === id)));
  }

  //
  // elements
  //
  renderCreateButton () {
    if (this.props.user.role !== ERoles.guest) {
      return (
        <Button bsStyle="link" onClick={this.handleCreate.bind(this)}>
          {i18n.t('[create]', '[создать]')}
        </Button>
      );
    }
  }

  renderEditButton (id: string) {
    if (this.props.user.role !== ERoles.guest) {
      return (
        <Button bsStyle="link" onClick={this.handleEdit.bind(this, id)}>
          {i18n.t('[edit]', '[редактировать]')}
        </Button>
      )
    }
  }

  renderDeleteButton (id: string) {
    if (this.props.user.role !== ERoles.guest) {
      return (
        <Button bsStyle="link" onClick={this.handleDelete.bind(this, id)}>
          {i18n.t('[delete]', '[удалить]')}
        </Button>
      )
    }
  }

  renderProjectName (project: IProject) {
    return (
      <Button bsStyle="link" onClick={this.handleView.bind(this, project.id)}>{project.name}</Button>
    )
  }

  renderProject (project: IProject) {
    return (
      <Row>
        <Col className="projects-page__media-wrapper">
          <div className="projects-page__media">
            <Media>
              <Media.Left>
                <Image width={128} height={128} className="projects-page__photo" src={project.avatar} rounded />
              </Media.Left>
              <Media.Body>
                <Media.Heading>{this.renderProjectName(project)} {this.renderEditButton(project.id)} {this.renderDeleteButton(project.id)}</Media.Heading>
                <p>{project.description}</p>
              </Media.Body>
            </Media>
          </div>
        </Col>
      </Row>
    );
  }

  renderProjects() {
    const comparator = (project: IProject) => -project.priority;

    return (
      <Grid>
        {_.map(_.sortBy(this.props.projects, comparator), (project, i) => <Row key={i}>{this.renderProject(project)}</Row>)}
      </Grid>
    )
  }

  // отрисовка
  render () {
    return (
      <div className="projects-page">
        <div className="projects-page__title">
          {this.renderCreateButton()}
        </div>
        <div className="projects-page__body">
          {this.renderProjects()}
        </div>
      </div>
    )
  }
}

/*
 * Контейнер
 */
const mapStateToProps = function (state: IAppStore): IProps {
  const { projects } = state.projects.projectsPage;
  const { user } = state.main.loginPage;

  return { projects, user } as IProps;
};

const mapDispatchToProps = function (dispatch: Redux.Dispatch<IAppStore>): IProps {
  const actions: IProjectsPageActions = new ProjectsPageActions(projectsPageService, dispatch);

  return { actions } as IProps;
};

const ProjectsListPage = withRouter(ReactRedux.connect(mapStateToProps, mapDispatchToProps)(CProjectsListPage) as React.ComponentClass<RouteComponentProps<IProps>>);

export { ProjectsListPage };
