import { i18n } from 'Core/Utils';

/*
 * Сраницы модуля
 */
export enum EUsersPages {
  LAYOUT = 50,
  USER_LIST_PAGE = 51,
  USER_EDIT_PAGE = 52,
}

export const usersRoutes = {
  [EUsersPages.LAYOUT]: {
    path: '/main/users',
    name: i18n.t('Users layout', 'Раздел пользователи')
  },
  [EUsersPages.USER_LIST_PAGE]: {
    path: '/main/users',
    name: i18n.t('Users', 'Пользователи')
  },
  [EUsersPages.USER_EDIT_PAGE]: {
    path: '/main/users/edit',
    name: i18n.t('Users editor', 'Редактировать пользователя')
  }
};

import { UsersListPage } from 'Modules/Users/Pages/UsersListPage';
import { UsersLayout } from 'Modules/Users/Pages/UsersLayout';

/*
 * Компоненты страниц
 */
export {
  UsersLayout,
  UsersListPage
};
