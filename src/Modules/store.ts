import { IMainStore, mainInitialStore } from 'Modules/Main';
import { IProjectsStore, projectsInitialStore } from 'Modules/Projects';
import { IExperienceStore, experienceInitialStore } from 'Modules/Experience';
import { IContactsStore, contactsInitialStore } from 'Modules/Contacts';
import { IUsersStore, usersInitialStore } from 'Modules/Users';

/*
 * Main application data store
 */
interface IAppStore {
  main: IMainStore,
  projects: IProjectsStore,
  experience: IExperienceStore,
  contacts: IContactsStore,
  users: IUsersStore,
}

/*
 * Main application data store init values
 */
const appInitialStore: IAppStore = {
  main: mainInitialStore,
  projects: projectsInitialStore,
  experience: experienceInitialStore,
  contacts: contactsInitialStore,
  users: usersInitialStore
};

export {
  IAppStore,
  appInitialStore
};
