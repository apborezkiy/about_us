import { IUser } from 'Common/Models';

/*
 * Actions
 */
export const loginPageActions = {
  LOGIN: 'LOGIN',
  LOGOUT: 'LOGOUT',
  GET_USER: 'GET_USER'
};

export interface ILoginPageActions {
  login: (user: IUser) => Promise<IUser>
  logout: (user: IUser) => Promise<void>
  getUser: () => Promise<IUser>
}
