import { i18n } from 'Core/Utils';

/*
 * Страницы модуля
 */
export enum EProjectsPages {
  LAYOUT = 40,
  PROJECTS_LIST_PAGE = 41,
  PROJECTS_EDIT_PAGE = 42,
  PROJECTS_VIEW_PAGE = 43,
  SCREENSHOOTS_LIST_PAGE = 44,
  SCREENSHOOTS_EDIT_PAGE = 45
}

export const projectsRoutes = {
  [EProjectsPages.LAYOUT]: {
    path: '/main/projects',
    name: i18n.t('Projects layout', 'Раздел проектов')
  },
  [EProjectsPages.PROJECTS_LIST_PAGE]: {
    path: '/main/projects',
    name: i18n.t('Projects', 'Проекты')
  },
  [EProjectsPages.PROJECTS_EDIT_PAGE]: {
    path: '/main/projects/edit',
    name: i18n.t('Project editor', 'Редактировать проекты')
  },
  [EProjectsPages.PROJECTS_VIEW_PAGE]: {
    path: '/main/projects/view',
    name: i18n.t('Project viewer', 'Просмотр проектов')
  },
  [EProjectsPages.SCREENSHOOTS_LIST_PAGE]: {
    path: '/main/projects/screen_shoots',
    name: i18n.t('Screen shoots', 'Скриншоты')
  },
  [EProjectsPages.SCREENSHOOTS_EDIT_PAGE]: {
    path: '/main/projects/screen_shoots/edit',
    name: i18n.t('Screen shoots editor', 'Редактировать скриншоты')
  },
};

import { ProjectsListPage } from 'Modules/Projects/Pages/ProjectsListPage';
import { ProjectsEditPage } from 'Modules/Projects/Pages/ProjectsEditPage';
import { ProjectsViewPage } from 'Modules/Projects/Pages/ProjectsViewPage';
import { ScreenShootsListPage } from 'Modules/Projects/Pages/ScreenShootsListPage';
import { ScreenShootsEditPage } from 'Modules/Projects/Pages/ScreenShootsEditPage';
import { ProjectsLayout } from 'Modules/Projects/Pages/ProjectsLayout';

/*
 * Компоненты страниц
 */
export {
  ProjectsLayout,
  ProjectsListPage,
  ProjectsEditPage,
  ProjectsViewPage
};
