import { IProjectsPageStore } from 'Modules/Projects/Redux/Store/ProjectsPageStore';
import { IScreenShootsPageStore } from 'Modules/Projects/Redux/Store/ScreenShootsPageStore';
import { projectsInitialStore } from 'Modules/Projects/Redux/Reducers';

/*
 * Описание хранилища модуля
 */

interface IProjectsStore {
  projectsPage: IProjectsPageStore;
  screenShootsPage: IScreenShootsPageStore;
}

export {
  IProjectsPageStore,
  IScreenShootsPageStore,
  IProjectsStore,
  projectsInitialStore
}
