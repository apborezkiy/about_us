import * as _ from 'lodash';
import * as Redux from 'redux';
import * as ReduxActions from 'redux-actions';

import { SUCCESS } from 'Core/Models/Actions';
import { IScreenShoot } from 'Common/Models';
import { IScreenShootsPageStore } from 'Modules/Projects/Redux/Store';

import { screenShootsPageActions } from '../Actions/IScreenShootsPageActions';

/*
 * Начальное состояние страницы
 */
export const screenShootsPageInitialStore: IScreenShootsPageStore = {
  screenShoots: [],
  screenShoot: {} as IScreenShoot,
  images: []
}

/*
 * Редьюсер страницы
 */
export const screenShootsPageReducer: Redux.Reducer<IScreenShootsPageStore> = ReduxActions.handleActions({
  [`${screenShootsPageActions.LOAD_SCREEN_SHOOTS}_${SUCCESS}`]: (state: IScreenShootsPageStore, action) => {
    return _.assign({}, state, { screenShoots: action.payload });
  },
  [`${screenShootsPageActions.LOAD_SCREEN_SHOOTS_AVAILABLE_IMAGES}_${SUCCESS}`]: (state: IScreenShootsPageStore, action) => {
    return _.assign({}, state, { images: action.payload });
  },
  [`${screenShootsPageActions.EDIT_SCREEN_SHOOT}_${SUCCESS}`]: (state: IScreenShootsPageStore, action: ReduxActions.Action<IScreenShoot>) => {
    return _.assign({}, state, { screenShoot: action.payload });
  },
  [`${screenShootsPageActions.SAVE_SCREEN_SHOOT}_${SUCCESS}`]: (state: IScreenShootsPageStore, action: ReduxActions.Action<IScreenShoot>) => {
    let newScreenShoot = action.payload;

    let screenShoots = _.filter(state.screenShoots, (screenShoot) => screenShoot.id !== newScreenShoot.id) as IScreenShoot[];
    let newScreenShoots = _.sortBy([...screenShoots, newScreenShoot], (o: IScreenShoot) => o.fileName);

    return _.assign({}, state, { screenShoots: newScreenShoots });
  },
  [`${screenShootsPageActions.DELETE_SCREEN_SHOOT}_${SUCCESS}`]: (state: IScreenShootsPageStore, action: ReduxActions.Action<IScreenShoot>) => {
    let screenShootId = action.payload;

    let screenShoots = _.filter(state.screenShoots, (screenShoot) => screenShoot.id !== screenShootId) as IScreenShoot[];
    let newScreenShoots = _.sortBy([...screenShoots], (o: IScreenShoot) => o.fileName);

    return _.assign({}, state, {screenShoots: newScreenShoots});
  }
}, screenShootsPageInitialStore)
