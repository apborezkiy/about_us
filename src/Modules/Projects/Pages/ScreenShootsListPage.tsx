import * as _ from 'lodash';
import * as React from 'react';
import * as Redux from 'redux';
import * as ReactRedux from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import { Grid, Row, Col, Image, Media, Button, Form, FormGroup, ControlLabel, FormControl } from 'react-bootstrap';

import { i18n } from 'Core/Utils';

import { IScreenShoot, IUser, ERoles } from 'Common/Models';
import { IScreenShootsPageActions } from 'Modules/Projects/Redux/Actions/IScreenShootsPageActions';
import { ScreenShootsPageActions } from 'Modules/Projects/Redux/Actions/ScreenShootsPageActions';
import { screenShootsPageService } from 'Modules/Projects/Services/SnapShootsPageService';
import { IAppStore } from 'Modules/store';

/*
 * Передаваемые параметры
 */
interface IProps {
  actions: IScreenShootsPageActions;
  screenShoots: IScreenShoot[];
  user: IUser;
}

/*
 * Cтраница скриншотов
 */
class CScreenShootsListPage extends React.Component<RouteComponentProps<IProps> & IProps> {

  componentWillMount() {
    if (this.props.screenShoots.length === 0) {
      this.props.actions.loadScreenShoots();
    }
  }

  //
  // handlers
  //
  handleCreate () {
    this.props.actions.editScreenShoot({} as IScreenShoot);
  }

  handleEdit (id: string) {
    this.props.actions.editScreenShoot(_.find(this.props.screenShoots, ((screenShoot) => screenShoot.id === id)));
  }

  handleDelete (id: string) {
    this.props.actions.deleteScreenShoot(_.find(this.props.screenShoots, ((screenShoot) => screenShoot.id === id)));
  }

  //
  // elements
  //
  renderCreateButton () {
    if (this.props.user.role !== ERoles.guest) {
      return (
        <Button bsStyle="link" onClick={this.handleCreate.bind(this)}>
          {i18n.t('[create]', '[создать]')}
        </Button>
      );
    }
  }

  renderEditButton (id: string) {
    if (this.props.user.role !== ERoles.guest) {
      return (
        <Button bsStyle="link" onClick={this.handleEdit.bind(this, id)}>
          {i18n.t('[edit]', '[редактировать]')}
        </Button>
      )
    }
  }

  renderDeleteButton (id: string) {
    if (this.props.user.role !== ERoles.guest) {
      return (
        <Button bsStyle="link" onClick={this.handleDelete.bind(this, id)}>
          {i18n.t('[delete]', '[удалить]')}
        </Button>
      )
    }
  }

  renderScreenShoot (screenShoot: IScreenShoot) {
    return (
      <Row>
        <Col className="screen-shoots-page__media-wrapper">
          <div className="screen-shoots-page__media">
            <Media>
              <Media.Left>
                <Image width={128} height={128} className="screen-shoots-page__photo" src={screenShoot.image} rounded />
              </Media.Left>
              <Media.Body>
                <Media.Heading>{screenShoot.fileName} {this.renderEditButton(screenShoot.id)} {this.renderDeleteButton(screenShoot.id)}</Media.Heading>
                <p>{screenShoot.description}</p>
              </Media.Body>
            </Media>
          </div>
        </Col>
      </Row>
    );
  }

  renderScreenShoots() {
    const comparator = (screenShoot: IScreenShoot) => screenShoot.fileName;

    return (
      <Grid>
        {_.map(_.sortBy(this.props.screenShoots, comparator), (screenShoot, i) => <Row key={i}>{this.renderScreenShoot(screenShoot)}</Row>)}
      </Grid>
    )
  }

  // отрисовка
  render () {
    return (
      <div className="screen-shoots-page">
        <div className="screen-shoots-page__title">
          {this.renderCreateButton()}
        </div>
        <div className="screen-shoots-page__body">
          {this.renderScreenShoots()}
        </div>
      </div>
    )
  }
}

/*
 * Контейнер
 */
const mapStateToProps = function (state: IAppStore): IProps {
  const { screenShoots } = state.projects.screenShootsPage;
  const { user } = state.main.loginPage;

  return { screenShoots, user } as IProps;
};

const mapDispatchToProps = function (dispatch: Redux.Dispatch<IAppStore>): IProps {
  const actions: IScreenShootsPageActions = new ScreenShootsPageActions(screenShootsPageService, dispatch);

  return { actions } as IProps;
};

const ScreenShootsListPage = withRouter(ReactRedux.connect(mapStateToProps, mapDispatchToProps)(CScreenShootsListPage) as React.ComponentClass<RouteComponentProps<IProps>>);

export { ScreenShootsListPage };
