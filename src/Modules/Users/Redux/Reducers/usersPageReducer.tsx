import * as _ from 'lodash';
import * as Redux from 'redux';
import * as ReduxActions from 'redux-actions';

import { SUCCESS } from 'Core/Models/Actions';
import { IUser } from 'Common/Models';
import { IUsersPageStore } from 'Modules/Users/Redux/Store';

import { usersPageActions } from '../Actions/IUsersPageActions';

/*
 * Начальное состояние страницы
 */
export const usersPageInitialStore: IUsersPageStore = {
  users: [],
  account: {} as IUser
}

/*
 * Редьюсер страницы
 */
export const usersPageReducer: Redux.Reducer<IUsersPageStore> = ReduxActions.handleActions({
  [`${usersPageActions.LOAD_USERS}_${SUCCESS}`]: (state: IUsersPageStore, action) => {
    return _.assign({}, state, { users: action.payload });
  },
  [`${usersPageActions.EDIT_USER}_${SUCCESS}`]: (state: IUsersPageStore, action: ReduxActions.Action<IUser>) => {
    return _.assign({}, state, { account: action.payload });
  },
  [`${usersPageActions.SAVE_USER}_${SUCCESS}`]: (state: IUsersPageStore, action: ReduxActions.Action<IUser>) => {
    let newMember = action.payload;

    let users = _.filter(state.users, (account) => account.id !== newMember.id) as IUser[];
    let newUsers = _.sortBy([...users, newMember], (o: IUser) => o.name);

    return _.assign({}, state, { users: newUsers });
  },
  [`${usersPageActions.DELETE_USER}_${SUCCESS}`]: (state: IUsersPageStore, action: ReduxActions.Action<IUser>) => {
    let accountId = action.payload;

    let users = _.filter(state.users, (account) => account.id !== accountId) as IUser[];
    let newUsers = _.sortBy([...users], (o: IUser) => o.name);

    return _.assign({}, state, { users: newUsers });
  }
}, usersPageInitialStore)
