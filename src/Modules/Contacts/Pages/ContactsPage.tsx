import * as _ from 'lodash';
import * as React from 'react';
import * as Redux from 'redux';
import * as ReactRedux from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router';
import { Grid, Row, Col, Image, Media, Button, FormGroup, FormControl, ControlLabel, HelpBlock } from 'react-bootstrap';

import { i18n } from 'Core/Utils';
import { IValidationState, IValidation } from 'Core/Models/Validation';
import { IOffer, TOfferKey } from 'Common/Models';

import { IContactsPageActions } from 'Modules/Contacts/Redux/Actions/IContactsPageActions';
import { IAppStore } from 'Modules/store';
import { ContactsPageActions } from 'Modules/Contacts/Redux/Actions/ContactsPageActions';
import { contactsPageService } from 'Modules/Contacts/Services/ContactsPageService';

/*
 * Передаваемые параметры
 */
interface IProps {
  actions: IContactsPageActions;
  isSended: boolean
}

/*
 * Внутреннее состояние
 */
interface IState {
  offer: IOffer;
  validation: IValidation<IOffer>;
  isSended: boolean;
}

/*
 * Cтраница контактов
 */
class CContactsPage extends React.Component<RouteComponentProps<IProps> & IProps, IState> {

  state: IState = {
    offer: {} as IOffer,
    validation: {
      id: { pattern: /.+/i, required: false } as IValidationState,
      text: { pattern: /.+/i, required: true } as IValidationState,
      userEmail: { pattern: /.+/i, required: true } as IValidationState,
      userPhone: { pattern: /.+/i, required: false } as IValidationState
    },
    isSended: false
  };

  handleCreateOffer = () => {
    let formValid = true;
    for (let field of _.keys(this.state.validation) as TOfferKey[]) {
      formValid = formValid && this.handleInput(field);
    }

    if (formValid) {
      this.props.actions.createOffer(this.state.offer as IOffer)
    }
  };

  handleInput = (key: TOfferKey, e?) => {
    let offer = _.assign({}, this.state.offer);
    offer[key] = e ? e.target.value : offer[key];
    let validation = _.assign({}, this.state.validation);
    let validationFiled = validation[key];
    let isValid = true;

    if (validationFiled.required && (!offer[key] || offer[key].length === 0)) {
      isValid = false;
      validationFiled.state = 'warning';
      validationFiled.text = i18n.t('Required field', 'Обязательное поле');
    } else {
      isValid = validationFiled.pattern.test(offer[key]);
      validationFiled.state = isValid ? 'success' : 'error';
      validationFiled.text = isValid ? '' : i18n.t('Incorrect field', 'Некорректное поле');
    }

    this.setState({offer, validation});

    return isValid;
  };

  /*
   * Contacts form
   */
  renderEmailInput () {
    return (
      <FormGroup className="contacts-page__form-item"
        controlId="formHorizontalEmail"
        validationState={this.state.validation.userEmail.state}>
        <Col componentClass={ControlLabel}>
          {i18n.t('Email')}
        </Col>
        <Col>
          <FormControl className="contacts-page__form-item-email"
            type="text"
            placeholder={i18n.t('Email')}
            onInput={e => this.handleInput('userEmail', e)} />
          <HelpBlock>{this.state.validation.userEmail.text}</HelpBlock>
        </Col>
      </FormGroup>
    );
  }

  renderTextInput () {
    return (
      <FormGroup className="contacts-page__form-item"
        controlId="formControlsTextarea"
        validationState={this.state.validation.text.state}>
        <Col componentClass={ControlLabel}>
          {i18n.t('Offer', 'Предложение')}
        </Col>
        <Col>
          <FormControl className="contacts-page__form-item-offer"
            componentClass="textarea"
            placeholder={i18n.t('Offer', 'Предложение')}
            onInput={e => this.handleInput('text', e)} />
          <HelpBlock>{this.state.validation.text.text}</HelpBlock>
        </Col>
      </FormGroup>
    );
  }

  renderSubmitButton () {
    return (
      <FormGroup className="contacts-page__form-item">
        <Col>
          <Button type="submit" onClick={this.handleCreateOffer}>
            {i18n.t('Submit', 'Отправить')}
          </Button>
        </Col>
      </FormGroup>
    );
  }

  renderTitle () {
    return (
      <FormGroup className="contacts-page__form-item">
        <Col componentClass={ControlLabel}>
          {i18n.t('Our contacts', 'Наши котакты')}
        </Col>
      </FormGroup>
    );
  }

  renderContactsForm () {
    if (!this.props.isSended) {
      return (
        <form className="contacts-page__form" autoComplete="off">
          {this.renderTitle()}
          {this.renderEmailInput()}
          {this.renderTextInput()}
          {this.renderSubmitButton()}
        </form>
      );
    } else {
      return (
        <form className="contacts-page__form" autoComplete="off">
          <div className="contacts-page__form-notification">
            {i18n.t('Your offer sended successfully!', 'Предложение успешно отправлено!')}
          </div>
        </form>
      )
    }
  }

  renderProjectManagers () {
    return (
      <Grid>
        <Row>
          <Col className="contacts-page__photo-wrapper" xs={6} sm={4}>
            <Image className="contacts-page__photo" src="assets/Apborezkiy_Small.jpg" circle/>
          </Col>
          <Col className="contacts-page__photo-wrapper" xs={6} sm={4}>
            <Image className="contacts-page__photo" src="assets/Apborezkiy_Small.jpg" circle/>
          </Col>
          <Col className="contacts-page__photo-wrapper" xsHidden sm={4}>
            <Image className="contacts-page__photo" src="assets/Apborezkiy_Small.jpg" circle/>
          </Col>
        </Row>
      </Grid>
    );
  }

  // отрисовка
  render () {
    return (
      <div className="contacts-page">
        <div className="contacts-page__title">
          {this.renderProjectManagers()}
        </div>
        <div className="contacts-page__body">
          {this.renderContactsForm()}
        </div>
      </div>
    );
  }
}

/*
 * Контейнер
 */
const mapStateToProps = function (state: IAppStore): IProps {
  const { isSended } = state.contacts.contactsPage;
  return { isSended } as IProps;
};

const mapDispatchToProps = function (dispatch: Redux.Dispatch<IAppStore>): IProps {
  const actions: IContactsPageActions = new ContactsPageActions(contactsPageService, dispatch);
  return { actions } as IProps;
};

const ContactsPage = withRouter(ReactRedux.connect(mapStateToProps, mapDispatchToProps)(CContactsPage) as React.ComponentClass<RouteComponentProps<IProps>>);

export { ContactsPage };
