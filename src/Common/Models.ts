/*
 * Приоритет сортировки
 */
export enum EPriorities {
  low = 1,
  medium = 2,
  high = 3,
  highest = 4
}

/*
 * Ресурс
 */
export interface IResource {
  id: string;
  url: string;
  name: string;
}

/*
 * Проект
 */
export interface IProjectHeader {
  name: string;
  id: string;
  priority: EPriorities;
}

export interface IScreenShoot {
  fileName: string;
  id: string;
  description: string;
  image: string;
}

export type TScreenShootKey = keyof IScreenShoot;

export interface IProject extends IProjectHeader {
  description: string;
  avatar: string;
  avatarFileName: string;
  screenShoots: IScreenShoot[];
}

export type TProjectKey = keyof IProject;

/*
 * Технология
 */

export enum ETechnologyKind {
  FRONT = 1,
  BACK = 2,
}

/*
 * Технологии, которыми владеет член команды
 */

export interface ITechnology {
  name: string;
  id: string;
  kind: ETechnologyKind
}

export type TTechnologyKey = keyof ITechnology;

/*
 * Член команды
 */
export interface ITeamMember {
  user: IUserHeader;
  id: string;
  position: string;
  experience: string;
  image: string; // big image of good quality
  technologies: ITechnology[];
  projects: IProjectHeader[];
  priority: EPriorities; // order to show
}

export type TTeamMemberKey = keyof ITeamMember;

/*
 * Предложение
 */
export interface IOffer {
  text: string;
  id: string;
  userEmail: string;
  userPhone: string;
}

export type TOfferKey = keyof IOffer;

/*
 * Роль доступа
 */
export enum ERoles {
  guest = 1, // not authorized
  member = 2, // member ot the team
  moderator = 3, // editor of the content
  admin = 4 // manager of the project
}

/*
 * Пользователь
 */
export interface IUserHeader {
  name: string;
  id: string;
  priority: EPriorities;
}

export interface IUser extends IUserHeader {
  email: string;
  password: string;
  role: ERoles;
  session: string;
  image: string;
}

export type TUserKey = keyof IUser;
