import * as React from 'react';
import * as Redux from 'redux';
import * as ReactRedux from 'react-redux';
import { Route, RouteComponentProps, withRouter } from 'react-router';
import { Button } from 'react-bootstrap';

import { pageToRouteInfo } from 'Core/Utils';

import { IAppStore } from 'Modules/store';
import { ProjectsListPage } from 'Modules/Projects/Pages/ProjectsListPage';
import { ProjectsEditPage } from 'Modules/Projects/Pages/ProjectsEditPage';
import { ProjectsViewPage } from 'Modules/Projects/Pages/ProjectsViewPage';
import { ScreenShootsListPage } from 'Modules/Projects/Pages/ScreenShootsListPage';
import { ScreenShootsEditPage } from 'Modules/Projects/Pages/ScreenShootsEditPage';
import { projectsRoutes, EProjectsPages } from 'Modules/Projects/Pages';

const projectsListPage = pageToRouteInfo<EProjectsPages>(EProjectsPages.PROJECTS_LIST_PAGE, projectsRoutes);
const projectsEditPage = pageToRouteInfo<EProjectsPages>(EProjectsPages.PROJECTS_EDIT_PAGE, projectsRoutes);
const projectsViewPage = pageToRouteInfo<EProjectsPages>(EProjectsPages.PROJECTS_VIEW_PAGE, projectsRoutes);
const screenShootsListPage = pageToRouteInfo<EProjectsPages>(EProjectsPages.SCREENSHOOTS_LIST_PAGE, projectsRoutes);
const screenShootsEditPage = pageToRouteInfo<EProjectsPages>(EProjectsPages.SCREENSHOOTS_EDIT_PAGE, projectsRoutes);

/*
 * Передаваемые параметры
 */
interface IProps {

}

/*
 * Каркас модуля
 */
class CProjectsLayout extends React.Component<RouteComponentProps<IProps> & IProps> {

  // отрисовка
  render () {
    return (
      <div className="projects-layout">
        <div className="projects-layout__header">

        </div>
        <div className="projects-layout__body">
          <Route exact path={projectsListPage.path} component={ProjectsListPage} />
          <Route exact path={projectsEditPage.path} component={ProjectsEditPage} />
          <Route exact path={projectsViewPage.path} component={ProjectsViewPage} />
          <Route exact path={screenShootsListPage.path} component={ScreenShootsListPage} />
          <Route exact path={screenShootsEditPage.path} component={ScreenShootsEditPage} />

        </div>
      </div>
    )
  }
}

/*
 * Контейнер
 */
const mapStateToProps = function (state: IAppStore): IProps {
  return {} as IProps;
}

const mapDispatchToProps = function (dispatch: Redux.Dispatch<IAppStore>): IProps {
  return {} as IProps;
}

const ProjectsLayout = withRouter(ReactRedux.connect(mapStateToProps, mapDispatchToProps)(CProjectsLayout) as React.ComponentClass<RouteComponentProps<IProps>>);

export { ProjectsLayout };
