import * as _ from 'lodash';
import { push } from 'connected-react-router';

import { GenericActions } from 'Core/Generic';
import { ActionUtils } from 'Core/Redux';

import { IMainPageService } from 'Modules/Main/Services/IMainPageService';
import { IMainMenuActions, mainMenuActions } from 'Modules/Main/Redux/Actions/IMainMenuActions';
import { ECompositePages, mainRoutes } from 'Modules/Main/Pages';
import { pageToRouteInfo } from 'Core/Utils';
import { projectsRoutes } from 'Modules/Projects/Pages';
import { experienceRoutes } from 'Modules/Experience/Pages';
import { contactsRoutes } from 'Modules/Contacts/Pages';
import { usersRoutes } from 'Modules/Users/Pages';

/*
 * Действия главного меню
 */
class MainMenuActions extends GenericActions implements IMainMenuActions {
  constructor (private api: IMainPageService, dispatch) {
    super(dispatch);
  }

  // Навигация по главному меню
  navigateMainMenu (page: ECompositePages) {
    const route: string = pageToRouteInfo<ECompositePages>(page, _.assign({}, mainRoutes, projectsRoutes, experienceRoutes, contactsRoutes, usersRoutes)).path;

    async function navigate () {
      this.dispatch(push(route));

      return await new Promise(resolve => {
        setTimeout(resolve, 100);
      });
    }

    ActionUtils.dispatchAsyncResponseBound<void>(this.dispatch, mainMenuActions.NAVIGATE_MAIN_MENU, navigate.bind(this));
  }
}

export { MainMenuActions };
