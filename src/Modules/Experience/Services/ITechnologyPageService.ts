import { ITechnology } from 'Common/Models';

/*
 * API
 */
export interface ITechnologyPageService {
  loadTechnologies: () => Promise<ITechnology[]>;
  createTechnology: (technology: ITechnology) => Promise<ITechnology>;
  deleteTechnology: (technology: ITechnology) => Promise<string>;
  saveTechnology: (technology: ITechnology) => Promise<ITechnology>
}
