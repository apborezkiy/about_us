import * as _ from 'lodash';
import * as React from 'react';
import * as Redux from 'redux';
import * as ReactRedux from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router';
import { Button, Grid, Row, Col, Image, Carousel, Well, Accordion, Panel } from 'react-bootstrap';

import { i18n } from 'Core/Utils';
import { EErrors, errorsMap } from 'Core/Models/Network';
import { IAppStore } from 'Modules/store';

/*
 * Сообщения об ошибках
 */
const config = {
  [EErrors.NETWORK]: {
    title: i18n.t('Check your internet connection', 'Проверьте подключение к интернету'),
    body: i18n.t('Error - connect your administrator', 'Ошибка - Свяжитесь с вашим администратором')
  },
  [EErrors.INTERNAL]: {
    title: i18n.t('Internal client error', 'Внутренняя ошибка клиента'),
    body: i18n.t('Error - connect support', 'Ошибка - Свяжитесь с поддержкой')
  },
  [EErrors.EXTERNAL]: {
    title: i18n.t('External client error', 'Внешняя ошибка сервера'),
    body: i18n.t('Error - connect support', 'Ошибка - Свяжитесь с поддержкой')
  }
}

/*
 * Передаваемые параметры
 */
interface IProps {

}

/*
 * Страница сообщения об ошибке
 */
class CErrorsPage extends React.Component<RouteComponentProps<IProps> & IProps> {

  // отрисовка
  render () {
    let error;
    let params = this.props.location;

    if (params.search) {
      error = JSON.parse(params.search.slice(1)) || { status: 0 };
    }

    let status = error.status;

    return (
      <div className="errors-page">
        <div className="errors-page__title">
          <div className="errors-page__title-status errors-page--center">{status}</div>
          <div className="errors-page__title-text">{config[errorsMap[status]].title}</div>
        </div>
        <div className="errors-page__body">
          {config[errorsMap[status]].body}
        </div>
      </div>
    )
  }
}

/*
 * Контейнер
 */
const mapStateToProps = function (state: IAppStore): IProps {
  return {} as IProps;
}

const mapDispatchToProps = function (dispatch: Redux.Dispatch<IAppStore>): IProps {
  return {} as IProps;
}

const ErrorsPage = withRouter(ReactRedux.connect(mapStateToProps, mapDispatchToProps)(CErrorsPage) as React.ComponentClass<RouteComponentProps<IProps>>);

export { ErrorsPage };
