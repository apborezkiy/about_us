import * as Redux from 'redux';

import { mainPageInitialStore, mainPageReducer } from 'Modules/Main/Redux/Reducers/mainPageReducer';
import { loginPageInitialStore, loginPageReducer } from 'Modules/Main/Redux/Reducers/loginPageReducer';
import { IMainStore } from 'Modules/Main/Redux/Store';

/*
 * Редьюсер модуля
 */
const reducer: Redux.ReducersMapObject = {
  mainPage: mainPageReducer,
  loginPage: loginPageReducer
}

const mainReducer: Redux.Reducer<IMainStore> = Redux.combineReducers<IMainStore>(reducer);

/*
 * Начальное состояние хранилища модуля
 */
const mainInitialStore: IMainStore = {
  mainPage: mainPageInitialStore,
  loginPage: loginPageInitialStore
}

export { mainReducer, mainInitialStore };
