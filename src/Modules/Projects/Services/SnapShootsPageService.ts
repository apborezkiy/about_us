import * as _ from 'lodash';

import { GenericService } from 'Core/Generic';

import { IScreenShoot, IResource } from 'Common/Models';
import { IScreenShootsPageService } from 'Modules/Projects/Services/ISnapShootsPageService';

/*
 * Restul сервис
 */
class ScreenShootsPageService extends GenericService implements IScreenShootsPageService {
  constructor () {
    super('/screen_shoots');
  }

  loadScreenShoots (): Promise<IScreenShoot[]> {
    return this.get<IScreenShoot[]>('');
  }

  loadScreenShootsImages (): Promise<IResource[]> {
    return this.get<IResource[]>('/images');
  }

  saveScreenShoot (screenShoot: IScreenShoot): Promise<IScreenShoot> {
    return this.put<IScreenShoot>(`/${screenShoot.id}`, screenShoot);
  }

  createScreenShoot (screenShoot: IScreenShoot): Promise<IScreenShoot> {
    return this.post<IScreenShoot>('', screenShoot);
  }

  deleteScreenShoot (screenShoot: IScreenShoot): Promise<string> {
    return this.del(`/${screenShoot.id}`);
  }

  setupMocks (agent) {
    // load screenShoots
    agent.get(this.url, (params) => {
      this.logGet(params);

      let screenShoots: IScreenShoot[] = [
        {
          id: '1',
          fileName: '1.png',
          description: '1 Description.',
          image: 'assets/avatar.jpg'
        },
        {
          id: '2',
          fileName: '2.png',
          description: '2 Description.',
          image: 'assets/avatar.jpg'
        }
      ];

      return { data: screenShoots };
    });

    agent.get(this.url + '/images', (params) => {
      this.logGet(params);

      let images: IResource[] = [
        {
          id: '1',
          name: '1.png',
          url: 'assets/avatar.jpg'
        },
        {
          id: '2',
          name: '2.png',
          url: 'assets/avatar.jpg'
        }
      ];

      return { data: images };
    });

    // save experience
    agent.put(this.url + '/:id', (params) => {
      this.logPut(params);

      let body = params.body;

      let screenShoot: IScreenShoot = _.assign({}, body, {
        name: body.name
      });

      return { data: screenShoot };
    });

    // create experience
    agent.post(this.url, (params) => {
      this.logPut(params);

      let body = params.body;

      let screenShoot: IScreenShoot = _.assign({}, body, {
        id: (new Date()).getMilliseconds().toString(),
        name: body.name
      });

      return { data: screenShoot };
    });

    // delete experience
    agent.del(this.url + '/:id', (params) => {
      this.logDelete(params);

      return { data: params.params.id };
    });
  }
}

const screenShootsPageService = new ScreenShootsPageService();

export { screenShootsPageService, IScreenShootsPageService };
