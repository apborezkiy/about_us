import * as _ from 'lodash';

import { ESystemPages, systemRoutes, RouteInfoMap } from '../Models/Pages';

export function pageToRouteInfo<T> (page: ESystemPages | T, routesEx: RouteInfoMap) {
  return _.merge({}, systemRoutes, routesEx)[page] || _.concat(systemRoutes, routesEx)[ESystemPages.NOT_FOUND];
}
