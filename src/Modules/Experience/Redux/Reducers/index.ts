import * as Redux from 'redux';

import { experiencePageInitialStore, experiencePageReducer } from 'Modules/Experience/Redux/Reducers/experiencePageReducer';
import { technologyPageInitialStore, technologyPageReducer } from 'Modules/Experience/Redux/Reducers/technologyPageReducer';
import { IExperienceStore } from 'Modules/Experience/Redux/Store';

/*
 * Редьюсер модуля
 */
const reducer: Redux.ReducersMapObject = {
  experiencePage: experiencePageReducer,
  technologyPage: technologyPageReducer,
}

const experienceReducer: Redux.Reducer<IExperienceStore> = Redux.combineReducers<IExperienceStore>(reducer);

/*
 * Начальное состояние хранилища модуля
 */
const experienceInitialStore: IExperienceStore = {
  experiencePage: experiencePageInitialStore,
  technologyPage: technologyPageInitialStore
}

export { experienceReducer, experienceInitialStore };
