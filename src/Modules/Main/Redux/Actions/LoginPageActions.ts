import * as _ from 'lodash';
import { push } from 'connected-react-router';

import { GenericActions } from 'Core/Generic';
import { ActionUtils } from 'Core/Redux';

import { IUser } from 'Common/Models';
import { localStorageSections } from 'Common/Constants';

import { ILoginPageService } from 'Modules/Main/Services/ILoginPageService';
import { ILoginPageActions, loginPageActions } from 'Modules/Main/Redux/Actions/ILoginPageActions';
import { ECompositePages, mainRoutes } from 'Modules/Main/Pages';
import { pageToRouteInfo } from 'Core/Utils';
import { projectsRoutes } from 'Modules/Projects/Pages';
import { experienceRoutes } from 'Modules/Experience/Pages';
import { contactsRoutes } from 'Modules/Contacts/Pages';

/*
 * Действия страницы
 */
class LoginPageActions extends GenericActions implements ILoginPageActions {
  constructor (private api: ILoginPageService, dispatch) {
    super(dispatch);
  }

  // Вход в систему для администрирования
  login (user: IUser): Promise<IUser> {
    const doLogin = () =>
      this.api.login(user)
        .then((data: IUser) => {
          // save user to local storage
          localStorage.setItem(localStorageSections.session, data.session);

          return data;
        })
        .catch(this.redirectError) as Promise<IUser>;
    return ActionUtils.dispatchAsyncResponseBound<IUser>(this.dispatch, loginPageActions.LOGIN, doLogin);
  }

  // Выход
  logout (user: IUser) {
    const doLogout = () =>
      this.api.logout(user.session)
        .then(() => {
          // delete user from local storage
          localStorage.setItem(localStorageSections.session, '');
        })
        .catch(this.redirectError);
    return ActionUtils.dispatchAsyncResponseBound<void>(this.dispatch, loginPageActions.LOGOUT, doLogout);
  }

  // Получить текущего пользователя
  getUser (): Promise<IUser> {
    let session = localStorage.getItem(localStorageSections.session);

    if (session) {
      const doGetUser = () => this.api.getUser(session).catch(this.redirectError) as Promise<IUser>;
      return ActionUtils.dispatchAsyncResponseBound<IUser>(this.dispatch, loginPageActions.GET_USER, doGetUser);
    }

    return null;
  }
}

export { LoginPageActions };
