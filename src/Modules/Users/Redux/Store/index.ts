import { IUsersPageStore } from 'Modules/Users/Redux/Store/UsersPageStore';
import { usersInitialStore } from 'Modules/Users/Redux/Reducers';

/*
 * Описание хранилища модуля
 */

interface IUsersStore {
  usersPage: IUsersPageStore;
}

export {
  IUsersPageStore,
  IUsersStore,
  usersInitialStore
}
