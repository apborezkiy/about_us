import { push } from 'connected-react-router';

import { pageToRouteInfo } from 'Core/Utils';
import { systemRoutes, ESystemPages } from 'Core/Models/Pages';

/*
 * Общие действия
 */
class GenericActions {
  constructor (protected dispatch) {

  }

  redirectError = (error) => {
    if (error.error)
      error = error.error;
    else
      error = { status: 0 };

    this.dispatch(() => {
      const route = pageToRouteInfo<ESystemPages>(ESystemPages.ERROR, systemRoutes).path;
      this.dispatch(push({ pathname: route, search: JSON.stringify(error) }));
    });

    throw error;
  }
}

export { GenericActions };
