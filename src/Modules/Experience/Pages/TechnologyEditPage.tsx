import * as _ from 'lodash';
import * as React from 'react';
import * as Redux from 'redux';
import * as ReactRedux from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router';
import { Grid, Row, Col, Image, Media, Button, FormGroup, FormControl, ControlLabel, HelpBlock, Radio } from 'react-bootstrap';
import { RadioGroup, IRadioGroupSelectOption } from 'Common/Components/RadioGroup';

import { i18n } from 'Core/Utils';
import { IValidationState, IValidation } from 'Core/Models/Validation';

import { IUser, ERoles, ITechnology, TTechnologyKey, ETechnologyKind } from 'Common/Models';
import { getTechnologyKindName } from 'Common/Constants';

import { ITechnologyPageActions } from 'Modules/Experience/Redux/Actions/ITechnologyPageActions';
import { TechnologyPageActions } from 'Modules/Experience/Redux/Actions/TechnologyPageActions';
import { technologyPageService } from 'Modules/Experience/Services/TechnologyPageService';
import { IAppStore } from 'Modules/store';

type TKindRadioGroup = new() => RadioGroup<ETechnologyKind>;
const KindRadioGroup = RadioGroup as TKindRadioGroup;

/*
 * Передаваемые параметры
 */
interface IProps {
  technologies: ITechnology[]; // technologies list
  technology: ITechnology; // member to edit
  actions: ITechnologyPageActions; // CRUD actions
  user: IUser; // current user
}

/*
 * Внутреннее состояние
 */
interface IState {
  validation: IValidation<ITechnology>;
  isSended: boolean;
  technology: ITechnology;
}

/*
 * Отображения для RadioGroup
 */
function getTypeOptions (): IRadioGroupSelectOption<ETechnologyKind>[] {
  return _.map([ETechnologyKind.BACK, ETechnologyKind.FRONT], (value: ETechnologyKind, index: number) => {
    return {
      id: index,
      label: getTechnologyKindName(value),
      value: value
    };
  });
}

function getTypeOption (value: ETechnologyKind): IRadioGroupSelectOption<ETechnologyKind> {
  return _.find(getTypeOptions(), (selector: IRadioGroupSelectOption<ETechnologyKind>) => selector.value === value);
}

/*
 * Cтраница технологий
 */
class CTechnologyEditPage extends React.Component<RouteComponentProps<IProps> & IProps, IState> {

  state: IState = {
    technology: {} as ITechnology,
    validation: {
      name: { pattern: /.+/i, required: true } as IValidationState,
      kind: { pattern: /.+/i, required: true } as IValidationState,
    },
    isSended: false
  };

  componentWillMount () {
    this.setState({ technology: _.assign({}, this.state.technology, this.props.technology) });

    if (this.props.technologies.length === 0) {
      this.props.actions.loadTechnologies();
    }
  }

  handleSubmit = () => {
    let formValid = true;
    for (let field of _.keys(this.state.validation) as TTechnologyKey[]) {
      formValid = formValid && this.handleInput(field);
    }

    if (formValid) {
      let technology: ITechnology = _.assign({}, this.props.technology, this.state.technology);
      this.props.actions.saveTechnology(technology)
        .then(() => this.setState({ isSended: true }));
    }
  };

  handleInput = (key: TTechnologyKey, e?) => {
    let technology = _.assign({}, this.state.technology);
    technology[key] = e ? e.target.value : technology[key];
    let validation = _.assign({}, this.state.validation);
    let validationFiled = validation[key];
    let isValid = true;

    if (validationFiled.required && (!technology[key] || technology[key].length === 0)) {
      isValid = false;
      validationFiled.state = 'warning';
      validationFiled.text = i18n.t('Required field', 'Обязательное поле');
    } else {
      isValid = validationFiled.pattern.test(technology[key]);
      validationFiled.state = isValid ? 'success' : 'error';
      validationFiled.text = isValid ? '' : i18n.t('Incorrect field', 'Некорректное поле');
    }

    this.setState({ technology, validation });

    return isValid;
  };

  renderNameInput () {
    return (
      <FormGroup className="technology-page__form-item"
                 controlId="formHorizontalTechnologyName"
                 validationState={this.state.validation.name.state}>
        <Col componentClass={ControlLabel}>
          {i18n.t('Name', 'Наименование')}
        </Col>
        <Col>
          <FormControl className="technology-page__form-item-users"
                       type="text"
                       defaultValue={this.props.technology.name}
                       onInput={e => this.handleInput('name', e)}/>
          <HelpBlock>{this.state.validation.name.text}</HelpBlock>
        </Col>
      </FormGroup>
    );
  }

  renderKindInput () {
    return (
      <FormGroup className="technology-page__form-item"
                 controlId="formHorizontalTechnologyKind"
                 validationState={this.state.validation.kind.state}>
        <Col componentClass={ControlLabel}>
          {i18n.t('Kind', 'Вид')}
        </Col>
        <KindRadioGroup
          option={getTypeOption(this.state.technology.kind)}
          options={getTypeOptions()}
          callback={e => this.handleInput('kind', e)}
        />
      </FormGroup>
    );
  }

  renderSubmitButton () {
    if (this.props.user.role !== ERoles.guest) {
      return (
        <FormGroup className="technology-page__form-item">
          <Col>
            <Button type="submit" onClick={this.handleSubmit}>
              {i18n.t('Submit', 'Отправить')}
            </Button>
          </Col>
        </FormGroup>
      );
    }
  }

  renderTitle () {
    return (
      <FormGroup className="technology-page__form-item">
        <Col componentClass={ControlLabel}>
          {i18n.t('Technology', 'Технология')}
        </Col>
      </FormGroup>
    );
  }

  renderTechnologyForm () {
    if (!this.state.isSended) {
      return (
        <form className="technology-page__form" autoComplete="off">
          {this.renderTitle()}
          {this.renderNameInput()}
          {this.renderKindInput()}
          {this.renderSubmitButton()}
        </form>
      );
    } else {
      return (
        <form className="technology-page__form" autoComplete="off">
          <div className="technology-page__form-notification">
            {i18n.t('Data sended successfully!', 'Данные успешно отправлены!')}
          </div>
        </form>
      );
    }
  }

  // отрисовка
  render () {
    return (
      <div className="technology-page">
        <div className="technology-page__title">

        </div>
        <div className="technology-page__body">
          {this.renderTechnologyForm()}
        </div>
      </div>
    );
  }
}

/*
 * Контейнер
 */
const mapStateToProps = function (state: IAppStore): IProps {
  const { technology, technologies } = state.experience.technologyPage;
  const { user } = state.main.loginPage;

  return { technology, technologies, user } as IProps;
};

const mapDispatchToProps = function (dispatch: Redux.Dispatch<IAppStore>): IProps {
  const actions: ITechnologyPageActions = new TechnologyPageActions(technologyPageService, dispatch);

  return { actions } as IProps;
};

const TechnologyEditPage = withRouter(ReactRedux.connect(mapStateToProps, mapDispatchToProps)(CTechnologyEditPage) as React.ComponentClass<RouteComponentProps<IProps>>);

export { TechnologyEditPage };
