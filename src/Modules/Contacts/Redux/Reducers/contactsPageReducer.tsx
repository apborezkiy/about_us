import * as _ from 'lodash';
import * as Redux from 'redux';
import * as ReduxActions from 'redux-actions';

import { IContactsPageStore } from 'Modules/Contacts/Redux/Store';
import { SUCCESS } from 'Core/Models/Actions';
import { contactsPageActions } from '../Actions/IContactsPageActions'

/*
 * Редьюсер главной страницы
 */
export const contactsPageReducer: Redux.Reducer<IContactsPageStore> = ReduxActions.handleActions({
  [`${contactsPageActions.CREATE_OFFER}_${SUCCESS}`]: (state: IContactsPageStore, action) => {
    return _.assign({}, state, { isSended: true });
  }
}, {} as IContactsPageStore)

/*
 * Начальное состояние главной страницы
 */
export const contactsPageInitialStore: IContactsPageStore = {
  isSended: false
}
