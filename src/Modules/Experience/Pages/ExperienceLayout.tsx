import * as React from 'react';
import * as Redux from 'redux';
import * as ReactRedux from 'react-redux';
import { Route, RouteComponentProps, withRouter } from 'react-router';
import { Button } from 'react-bootstrap';

import { pageToRouteInfo } from 'Core/Utils';

import { IAppStore } from 'Modules/store';
import { ExperienceListPage } from 'Modules/Experience/Pages/ExperienceListPage';
import { ExperienceEditPage } from 'Modules/Experience/Pages/ExperienceEditPage';
import { TechnologyListPage } from 'Modules/Experience/Pages/TechnologyListPage';
import { TechnologyEditPage } from 'Modules/Experience/Pages/TechnologyEditPage';
import { experienceRoutes, EExperiencePages } from 'Modules/Experience/Pages';

const experienceListPage = pageToRouteInfo<EExperiencePages>(EExperiencePages.EXPERIENCE_LIST_PAGE, experienceRoutes);
const experienceEditPage = pageToRouteInfo<EExperiencePages>(EExperiencePages.EXPERIENCE_EDIT_PAGE, experienceRoutes);
const technologyListPage = pageToRouteInfo<EExperiencePages>(EExperiencePages.TECHNOLOGY_LIST_PAGE, experienceRoutes);
const technologyEditPage = pageToRouteInfo<EExperiencePages>(EExperiencePages.TECHNOLOGY_EDIT_PAGE, experienceRoutes);

/*
 * Передаваемые параметры
 */
interface IProps {

}

/*
 * Каркас модуля
 */
class CExperienceLayout extends React.Component<RouteComponentProps<IProps> & IProps> {

  // отрисовка
  render () {
    return (
      <div className="experience-layout">
        <div className="experience-layout__header">

        </div>
        <div className="experience-layout__body">
          <Route exact path={experienceListPage.path} component={ExperienceListPage} />
          <Route exact path={experienceEditPage.path} component={ExperienceEditPage} />
          <Route exact path={technologyListPage.path} component={TechnologyListPage} />
          <Route exact path={technologyEditPage.path} component={TechnologyEditPage} />
        </div>
      </div>
    )
  }
}

/*
 * Контейнер
 */
const mapStateToProps = function (state: IAppStore): IProps {
  return {} as IProps;
}

const mapDispatchToProps = function (dispatch: Redux.Dispatch<IAppStore>): IProps {
  return {} as IProps;
}

const ExperienceLayout = withRouter(ReactRedux.connect(mapStateToProps, mapDispatchToProps)(CExperienceLayout) as React.ComponentClass<RouteComponentProps<IProps>>);

export { ExperienceLayout };
