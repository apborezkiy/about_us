import { IExperiencePageStore } from 'Modules/Experience/Redux/Store/ExperiencePageStore';
import { ITechnologyPageStore } from 'Modules/Experience/Redux/Store/TechnologyPageStore';
import { experienceInitialStore } from 'Modules/Experience/Redux/Reducers';

/*
 * Описание хранилища модуля
 */

interface IExperienceStore {
  experiencePage: IExperiencePageStore;
  technologyPage: ITechnologyPageStore;
}

export {
  IExperiencePageStore,
  IExperienceStore,
  ITechnologyPageStore,
  experienceInitialStore
}
