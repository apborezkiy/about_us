import { ITeamMember } from 'Common/Models';

/*
 * API
 */
export interface IExperiencePageService {
  loadExperience: () => Promise<ITeamMember[]>;
  createExperience: (member: ITeamMember) => Promise<ITeamMember>;
  deleteExperience: (member: ITeamMember) => Promise<string>;
  saveExperience: (member: ITeamMember) => Promise<ITeamMember>
}
