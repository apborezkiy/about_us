import { IScreenShoot, IResource } from 'Common/Models';

/*
 * Описание хранилища
 */
interface IScreenShootsPageStore {
  screenShoots: IScreenShoot[];
  screenShoot: IScreenShoot; // edit
  images: IResource[];
}

export {
  IScreenShootsPageStore
}
