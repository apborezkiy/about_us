import { GenericService } from 'Core/Generic';

import { ERoles, IUser } from 'Common/Models';

import { ILoginPageService } from 'Modules/Main/Services/ILoginPageService';

/*
 * Restul сервис
 */
class LoginPageService extends GenericService implements ILoginPageService {
  constructor () {
    super('/login');
  }

  login (user: IUser) {
    return this.post<IUser>('', user);
  }

  logout (session: string) {
    return this.post<void>('/logout', { session });
  }

  getUser (session: string) {
    return this.get<IUser>('', { session });
  }

  setupMocks (agent) {
    // login
    agent.post(this.url, (params) => {
      this.logPost(params);
      return {
        data: {
          name: 'Arseniy Borezkiy',
          id: '1',
          role: ERoles.admin,
          session: '12345'
        } as IUser
      };
    });

    // logout
    agent.post(this.url + '/logout', (params) => {
      this.logPost(params);
      return {};
    });

    // get user
    agent.get(this.url, (params) => {
      this.logGet(params);
      return {
        data: {
          name: 'Arseniy Borezkiy',
          id: '1',
          role: ERoles.admin,
          session: '12345'
        } as IUser
      };
    });
  }
}

const loginPageService = new LoginPageService();

export { loginPageService, ILoginPageService };
