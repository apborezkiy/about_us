import { ITeamMember } from 'Common/Models';

/*
 * Описание хранилища
 */
interface IExperiencePageStore {
  members: ITeamMember[];
  member: ITeamMember; // edit
}

export {
  IExperiencePageStore
}
