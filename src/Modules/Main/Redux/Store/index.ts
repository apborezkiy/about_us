import { IMainPageStore } from 'Modules/Main/Redux/Store/MainPageStore';
import { ILoginPageStore } from 'Modules/Main/Redux/Store/LoginPageStore';
import { mainInitialStore } from 'Modules/Main/Redux/Reducers';

/*
 * Описание хранилища главного модуля
 */

interface IMainStore {
  mainPage: IMainPageStore;
  loginPage: ILoginPageStore;
}

export {
  IMainPageStore,
  ILoginPageStore,
  IMainStore,
  mainInitialStore
}
