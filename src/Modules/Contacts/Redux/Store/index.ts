import { IContactsPageStore } from 'Modules/Contacts/Redux/Store/ContactsPageStore';
import { contactsInitialStore } from 'Modules/Contacts/Redux/Reducers';

/*
 * Описание хранилища главного модуля
 */

interface IContactsStore {
  contactsPage: IContactsPageStore;
}

export {
  IContactsPageStore,
  IContactsStore,
  contactsInitialStore
}
