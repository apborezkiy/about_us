import * as Redux from 'redux';

import { projectsPageInitialStore, projectsPageReducer } from 'Modules/Projects/Redux/Reducers/projectsPageReducer';
import { screenShootsPageInitialStore, screenShootsPageReducer } from 'Modules/Projects/Redux/Reducers/screenShootsPageReducer';
import { IProjectsStore } from 'Modules/Projects/Redux/Store';

/*
 * Редьюсер модуля
 */
const reducer: Redux.ReducersMapObject = {
  projectsPage: projectsPageReducer,
  screenShootsPage: screenShootsPageReducer
}

const projectsReducer: Redux.Reducer<IProjectsStore> = Redux.combineReducers<IProjectsStore>(reducer);

/*
 * Начальное состояние хранилища модуля
 */
const projectsInitialStore: IProjectsStore = {
  projectsPage: projectsPageInitialStore,
  screenShootsPage: screenShootsPageInitialStore
}

export { projectsReducer, projectsInitialStore };
