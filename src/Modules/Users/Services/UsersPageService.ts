import * as _ from 'lodash';

import { GenericService } from 'Core/Generic';

import { IUser, ERoles, EPriorities } from 'Common/Models';
import { IUsersPageService } from 'Modules/Users/Services/IUsersPageService';

/*
 * Restul сервис
 */
class UsersPageService extends GenericService implements IUsersPageService {
  constructor () {
    super('/users');
  }

  loadUsers (): Promise<IUser[]> {
    return this.get<IUser[]>();
  }

  saveUser (user: IUser): Promise<IUser> {
    return this.put<IUser>(`/${user.id}`, user);
  }

  createUser (user: IUser): Promise<IUser> {
    return this.post<IUser>('', user);
  }

  deleteUser (user: IUser): Promise<string> {
    return this.del(`/${user.id}`);
  }

  setupMocks (agent) {
    // load users
    agent.get(this.url, (params) => {
      this.logGet(params);

      let users: IUser[] = [
        {
          id: '1',
          name: 'Ильнар',
          email: 'stilnar92@gmail.com',
          password: '1234',
          role: ERoles.admin,
          priority: EPriorities.high,
          image: 'assets/Apborezkiy_Big.jpg',
          session: ''
        },
        {
          id: '2',
          name: 'Арсений',
          email: 'ars@gmail.com',
          password: '1234',
          role: ERoles.moderator,
          priority: EPriorities.highest,
          image: 'assets/Apborezkiy_Big.jpg',
          session: ''

        },
      ];

      return { data: users };
    });

    // save user
    agent.put(this.url + '/:id', (params) => {
      this.logPut(params);

      let body = params.body;

      let user: IUser = _.assign({}, body, {
        name: body.name
      });

      return { data: user };
    });

    // create user
    agent.post(this.url, (params) => {
      this.logPut(params);

      let body = params.body;

      let user: IUser = _.assign({}, body, {
        id: (new Date()).getMilliseconds().toString(),
        name: body.name
      });

      return { data: user };
    });

    // delete user
    agent.del(this.url + '/:id', (params) => {
      this.logDelete(params);

      return { data: params.params.id };
    });
  }
}

const usersPageService = new UsersPageService();

export { usersPageService, IUsersPageService };
