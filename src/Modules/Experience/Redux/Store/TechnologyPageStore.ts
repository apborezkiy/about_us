import { ITechnology } from 'Common/Models';

/*
 * Описание хранилища
 */
interface ITechnologyPageStore {
  technologies: ITechnology[];
  technology: ITechnology; // edit
}

export {
  ITechnologyPageStore
}
