import * as Redux from 'redux';

import { usersPageInitialStore, usersPageReducer } from 'Modules/Users/Redux/Reducers/usersPageReducer';
import { IUsersStore } from 'Modules/Users/Redux/Store';

/*
 * Редьюсер модуля
 */
const reducer: Redux.ReducersMapObject = {
  usersPage: usersPageReducer
}

const usersReducer: Redux.Reducer<IUsersStore> = Redux.combineReducers<IUsersStore>(reducer);

/*
 * Начальное состояние хранилища модуля
 */
const usersInitialStore: IUsersStore = {
  usersPage: usersPageInitialStore
}

export { usersReducer, usersInitialStore };
