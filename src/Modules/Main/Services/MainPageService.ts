import { GenericService } from 'Core/Generic';

import { IMainPageService } from 'Modules/Main/Services/IMainPageService';

/*
 * Restul сервис
 */
class MainPageService extends GenericService implements IMainPageService {
  constructor () {
    super('/main');
  }

  setupMocks (agent) {

  }
}

const mainPageService = new MainPageService();

export { mainPageService, IMainPageService };
