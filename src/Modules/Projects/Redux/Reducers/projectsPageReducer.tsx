import * as _ from 'lodash';
import * as Redux from 'redux';
import * as ReduxActions from 'redux-actions';

import { SUCCESS } from 'Core/Models/Actions';
import { IProject } from 'Common/Models';
import { IProjectsPageStore } from 'Modules/Projects/Redux/Store';

import { projectsPageActions } from '../Actions/IProjectsPageActions';

/*
 * Начальное состояние страницы
 */
export const projectsPageInitialStore: IProjectsPageStore = {
  projects: [],
  project: {} as IProject
}

/*
 * Редьюсер страницы
 */
export const projectsPageReducer: Redux.Reducer<IProjectsPageStore> = ReduxActions.handleActions({
  [`${projectsPageActions.LOAD_PROJECTS}_${SUCCESS}`]: (state: IProjectsPageStore, action) => {
    return _.assign({}, state, { projects: action.payload });
  },
  [`${projectsPageActions.VIEW_PROJECT}_${SUCCESS}`]: (state: IProjectsPageStore, action: ReduxActions.Action<IProject>) => {
    return _.assign({}, state, { project: action.payload });
  },
  [`${projectsPageActions.EDIT_PROJECT}_${SUCCESS}`]: (state: IProjectsPageStore, action: ReduxActions.Action<IProject>) => {
    return _.assign({}, state, { project: action.payload });
  },
  [`${projectsPageActions.SAVE_PROJECT}_${SUCCESS}`]: (state: IProjectsPageStore, action: ReduxActions.Action<IProject>) => {
    let newProject = action.payload;

    let projects = _.filter(state.projects, (project) => project.id !== newProject.id) as IProject[];
    let newProjects = _.sortBy([...projects, newProject], (o: IProject) => o.priority);

    return _.assign({}, state, { projects: newProjects });
  },
  [`${projectsPageActions.DELETE_PROJECT}_${SUCCESS}`]: (state: IProjectsPageStore, action: ReduxActions.Action<IProject>) => {
    let projectId = action.payload;

    let projects = _.filter(state.projects, (project) => project.id !== projectId) as IProject[];
    let newProjects = _.sortBy([...projects], (o: IProject) => o.priority);

    return _.assign({}, state, {projects: newProjects});
  }
}, projectsPageInitialStore)
