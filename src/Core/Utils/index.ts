import { ESystemPages } from '../Models/Pages';
import { pageToRouteInfo } from './pages';
import { i18n } from './i18n';

export {
  ESystemPages,
  pageToRouteInfo,
  i18n
}
