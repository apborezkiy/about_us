import * as Redux from 'redux';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import reduxThunkMiddleware from 'redux-thunk';

import { loggerMiddleware } from './middlewares';

/*
 * Фабрика хранилища приложения
 */
export function createStore<S>(initialState: S, history, rootReducer): Redux.Store<S> {
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || Redux.compose;

  const store = Redux.createStore(
    connectRouter(history)(rootReducer), // new root reducer with router state
    initialState,
    composeEnhancers(
      Redux.applyMiddleware(routerMiddleware(history)), // for dispatching history actions
      Redux.applyMiddleware(loggerMiddleware),
      Redux.applyMiddleware(reduxThunkMiddleware)
    ),
  );

  return <Redux.Store<S>>store;
}
