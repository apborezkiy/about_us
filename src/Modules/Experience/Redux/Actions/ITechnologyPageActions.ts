import { ITechnology } from 'Common/Models';

/*
 * Actions
 */
export const technologyPageActions = {
  LOAD_TECHNOLOGIES: 'LOAD_TECHNOLOGIES',
  EDIT_TECHNOLOGY: 'EDIT_TECHNOLOGY',
  SAVE_TECHNOLOGY: 'SAVE_TECHNOLOGY',
  DELETE_TECHNOLOGY: 'DELETE_TECHNOLOGY'
};

export interface ITechnologyPageActions {
  loadTechnologies: () => Promise<ITechnology[]>;
  editTechnology: (technology: ITechnology) => void;
  saveTechnology: (technology: ITechnology) => Promise<ITechnology>;
  deleteTechnology: (technology: ITechnology) => Promise<string>;
}
