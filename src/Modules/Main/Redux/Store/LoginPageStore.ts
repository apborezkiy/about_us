import { IUser } from 'Common/Models';

/*
 * Описание хранилища
 */
interface ILoginPageStore {
  isLoggedIn: boolean;
  user: IUser;
}

export {
  ILoginPageStore
}
