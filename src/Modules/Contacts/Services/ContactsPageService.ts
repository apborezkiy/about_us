import { GenericService } from 'Core/Generic';
import { IOffer } from 'Common/Models';

import { IContactsPageService } from 'Modules/Contacts/Services/IContactsPageService';

/*
 * Restul сервис
 */
class ContactsPageService extends GenericService implements IContactsPageService {
  constructor () {
    super('/offers');
  }

  createOffer (offer: IOffer): Promise<void> {
    return this.post<void>('', offer);
  }

  setupMocks (agent) {
    // create offer
    agent.post(this.url, (params) => {
      this.logPost(params);
      return {};
    });
  }
}

const contactsPageService = new ContactsPageService();

export { contactsPageService, IContactsPageService };
