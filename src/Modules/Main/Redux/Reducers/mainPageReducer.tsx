import * as Redux from 'redux';
import * as ReduxActions from 'redux-actions';

import { IMainPageStore } from 'Modules/Main/Redux/Store';

/*
 * Редьюсер главной страницы
 */
export const mainPageReducer: Redux.Reducer<IMainPageStore> = () => {
  return {};
}

/*
 * Начальное состояние главной страницы
 */
export const mainPageInitialStore: IMainPageStore = {

}
