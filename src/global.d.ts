/** Global definitions for developement **/

// for style loader
declare module '*.css' {
  const styles: any;
  export = styles;
}

// for redux devtools extension
declare interface Window {
  devToolsExtension?(): (args?: any) => any;
  jQuery: any;
  __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
}
