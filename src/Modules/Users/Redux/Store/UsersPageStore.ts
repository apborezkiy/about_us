import { IUser } from 'Common/Models';

/*
 * Описание хранилища
 */
interface IUsersPageStore {
  users: IUser[];
  account: IUser;
}

export {
  IUsersPageStore
}
