import * as React from 'react';
import * as Redux from 'redux';
import * as ReactRedux from 'react-redux';
import { Redirect, Route, RouteComponentProps, Switch, withRouter } from 'react-router';
import { Button } from 'react-bootstrap';

import { pageToRouteInfo } from 'Core/Utils';
import { ESystemPages, systemRoutes } from 'Core/Models/Pages';

import { IAppStore } from 'Modules/store';
import { MainPage } from 'Modules/Main/Pages/MainPage';
import { LoginPage } from 'Modules/Main/Pages/LoginPage';
import { mainRoutes, EMainPages } from 'Modules/Main/Pages';
import { ContactsLayout, contactsRoutes, EContactsPages } from 'Modules/Contacts/Pages';
import { EProjectsPages, ProjectsLayout, projectsRoutes } from 'Modules/Projects/Pages';
import { EExperiencePages, ExperienceLayout, experienceRoutes } from 'Modules/Experience/Pages';
import { EUsersPages, UsersLayout, usersRoutes } from 'Modules/Users/Pages';
import { MainMenu } from 'Modules/Main/Components/MainMenu';
import { ErrorsPage } from 'Modules/Main/Pages/ErrorsPage';
import { ILoginPageActions } from 'Modules/Main/Redux/Actions/ILoginPageActions';
import { LoginPageActions } from 'Modules/Main/Redux/Actions/LoginPageActions';
import { loginPageService } from 'Modules/Main/Services/LoginPageService';


const mainPage = pageToRouteInfo<EMainPages>(EMainPages.MAIN_PAGE, mainRoutes);
const loginPage = pageToRouteInfo<EMainPages>(EMainPages.LOGIN_PAGE, mainRoutes);
const errorPage = pageToRouteInfo<ESystemPages>(ESystemPages.ERROR, systemRoutes);
const notFoundPage = pageToRouteInfo<ESystemPages>(ESystemPages.NOT_FOUND, systemRoutes);
const experienceLayout = pageToRouteInfo<EExperiencePages>(EExperiencePages.LAYOUT, experienceRoutes);
const projectsLayout = pageToRouteInfo<EProjectsPages>(EProjectsPages.LAYOUT, projectsRoutes);
const contactsLayout = pageToRouteInfo<EContactsPages>(EContactsPages.LAYOUT, contactsRoutes);
const usersLayout = pageToRouteInfo<EUsersPages>(EUsersPages.LAYOUT, usersRoutes);

/*
 * Передаваемые параметры
 */
interface IProps {
  loginActions: ILoginPageActions;
}

/*
 * Каркас модуля
 */
class CMainLayout extends React.Component<RouteComponentProps<IProps> & IProps> {

  componentWillMount () {
    this.props.loginActions.getUser();
  }

  // отрисовка
  render () {
    return (
      <div className="main-layout">
        <div className="main-layout__header">
          <MainMenu />
        </div>
        <div className="main-layout__body">
          <Switch>
            <Route exact path={mainPage.path} component={MainPage} />
            <Route exact path={loginPage.path} component={LoginPage} />
            <Route path={experienceLayout.path} component={ExperienceLayout} />
            <Route path={usersLayout.path} component={UsersLayout} />
            <Route path={projectsLayout.path} component={ProjectsLayout} />
            <Route path={contactsLayout.path} component={ContactsLayout} />

            <Route exact path={errorPage.path} component={ErrorsPage} />
            <Route exact path={notFoundPage.path} component={ErrorsPage} />

            <Route path="*" render={() => <Redirect to={notFoundPage.path} />} />
          </Switch>
        </div>
      </div>
    )
  }
}

/*
 * Контейнер
 */
const mapStateToProps = function (state: IAppStore): IProps {
  return {} as IProps;
}

const mapDispatchToProps = function (dispatch: Redux.Dispatch<IAppStore>): IProps {
  const loginActions: ILoginPageActions = new LoginPageActions(loginPageService, dispatch);

  return { loginActions } as IProps;
}

const MainLayout = withRouter(ReactRedux.connect(mapStateToProps, mapDispatchToProps)(CMainLayout) as React.ComponentClass<RouteComponentProps<IProps>>);

export { MainLayout };
