import { IProject } from 'Common/Models';

/*
 * Actions
 */
export const projectsPageActions = {
  LOAD_PROJECTS: 'LOAD_PROJECTS',
  VIEW_PROJECT: 'VIEW_PROJECT',
  EDIT_PROJECT: 'EDIT_PROJECT',
  SAVE_PROJECT: 'SAVE_PROJECT',
  DELETE_PROJECT: 'DELETE_PROJECT'
};

export interface IProjectsPageActions {
  loadProjects: () => Promise<IProject[]>;
  viewProject: (project: IProject) => void;
  editProject: (project: IProject) => void;
  saveProject: (project: IProject) => Promise<IProject>;
  deleteProject: (project: IProject) => Promise<string>;
}
