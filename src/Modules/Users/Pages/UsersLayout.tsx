import * as React from 'react';
import * as Redux from 'redux';
import * as ReactRedux from 'react-redux';
import { Route, RouteComponentProps, withRouter } from 'react-router';
import { Button } from 'react-bootstrap';

import { pageToRouteInfo } from 'Core/Utils';

import { IAppStore } from 'Modules/store';
import { UsersListPage } from 'Modules/Users/Pages/UsersListPage';
import { UsersEditPage } from 'Modules/Users/Pages/UsersEditPage';
import { usersRoutes, EUsersPages } from 'Modules/Users/Pages';

const usersListPage = pageToRouteInfo<EUsersPages>(EUsersPages.USER_LIST_PAGE, usersRoutes);
const usersEditPage = pageToRouteInfo<EUsersPages>(EUsersPages.USER_EDIT_PAGE, usersRoutes);

/*
 * Передаваемые параметры
 */
interface IProps {

}

/*
 * Каркас модуля
 */
class CUsersLayout extends React.Component<RouteComponentProps<IProps> & IProps> {

  // отрисовка
  render () {
    return (
      <div className="users-layout">
        <div className="users-layout__header">

        </div>
        <div className="users-layout__body">
          <Route exact path={usersListPage.path} component={UsersListPage} />
          <Route exact path={usersEditPage.path} component={UsersEditPage} />

        </div>
      </div>
    )
  }
}

/*
 * Контейнер
 */
const mapStateToProps = function (state: IAppStore): IProps {
  return {} as IProps;
}

const mapDispatchToProps = function (dispatch: Redux.Dispatch<IAppStore>): IProps {
  return {} as IProps;
}

const UsersLayout = withRouter(ReactRedux.connect(mapStateToProps, mapDispatchToProps)(CUsersLayout) as React.ComponentClass<RouteComponentProps<IProps>>);

export { UsersLayout };
