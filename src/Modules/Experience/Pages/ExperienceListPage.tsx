import * as _ from 'lodash';
import * as React from 'react';
import * as Redux from 'redux';
import * as ReactRedux from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router';
import { Grid, Row, Col, Image, Media, Button } from 'react-bootstrap';

import { i18n } from 'Core/Utils';

import { ITeamMember, IUser, ERoles, ETechnologyKind, IProject, IProjectHeader } from 'Common/Models';

import { IExperiencePageActions } from 'Modules/Experience/Redux/Actions/IExperiencePageActions';
import { ExperiencePageActions } from 'Modules/Experience/Redux/Actions/ExperiencePageActions';
import { experiencePageService } from 'Modules/Experience/Services/ExperiencePageService';
import { IProjectsPageActions } from 'Modules/Projects/Redux/Actions/IProjectsPageActions';
import { ProjectsPageActions } from 'Modules/Projects/Redux/Actions/ProjectsPageActions';
import { projectsPageService } from 'Modules/Projects/Services/ProjectsPageService';
import { IAppStore } from 'Modules/store';

/*
 * Передаваемые параметры
 */
interface IProps {
  members: ITeamMember[];
  projects: IProject[];
  actions: IExperiencePageActions;
  projectsActions: IProjectsPageActions;
  user: IUser;
}

/*
 * Cтраница опыта и технологий
 */
class CExperienceListPage extends React.Component<RouteComponentProps<IProps> & IProps> {

  componentWillMount () {
    if (this.props.members.length === 0) {
      this.props.actions.loadExperience();
    }

    if (this.props.projects.length === 0) {
      this.props.projectsActions.loadProjects();
    }
  }

  handleEdit (id: string) {
    this.props.actions.editExperience(_.find(this.props.members, ((member) => member.id === id)));
  }

  handleDelete (id: string) {
    this.props.actions.deleteExperience(_.find(this.props.members, ((member) => member.id === id)));
  }

  handleViewProject (projectHeader: IProjectHeader) {
    this.props.projectsActions.viewProject(_.find(this.props.projects, ((project) => project.id === projectHeader.id)));
  }

  renderTechnologies () {
    return (
      <Grid>
        <Row>
          <Col className="experience-page__photo-wrapper" sm={4}>
            <Image className="experience-page__photo-react" src="assets/react.png" rounded />
          </Col>
          <Col className="experience-page__photo-wrapper" sm={4}>
            <Image className="experience-page__photo-redux" src="assets/redux.png" rounded />
          </Col>
          <Col className="experience-page__photo-wrapper" sm={4}>
            <Image className="experience-page__photo-angular" src="assets/angular.png" rounded />
          </Col>
        </Row>
      </Grid>
    )
  }

  renderEditButton (id: string) {
    if (this.props.user.role !== ERoles.guest) {
      return (
        <Button bsStyle="link" onClick={this.handleEdit.bind(this, id)}>
          {i18n.t('[edit]', '[редактировать]')}
        </Button>
      )
    }
  }

  renderDeleteButton (id: string) {
    if (this.props.user.role !== ERoles.guest) {
      return (
        <Button bsStyle="link" onClick={this.handleDelete.bind(this, id)}>
          {i18n.t('[delete]', '[удалить]')}
        </Button>
      )
    }
  }

  renderProjectName (projectHeader: IProjectHeader) {
    return (
      <Button bsStyle="link" onClick={this.handleViewProject.bind(this, projectHeader)}>{projectHeader.name}</Button>
    )
  }

  renderMember (member: ITeamMember) {
    return (
      <Col className="experience-page__member-wrapper">
        <div className="experience-page__member">
          <Media>
            <Media.Left>
              <Image width={250} height={400} className="experience-page__member-photo" src={member.image} rounded />
            </Media.Left>
            <Media.Body>
              <Media.Heading>{member.user.name} {this.renderEditButton(member.id)} {this.renderDeleteButton(member.id)}</Media.Heading>
              <p>{member.position}</p>
              <p>{member.experience}</p>
              <p>
                {i18n.t('Front end: ')}
                {_.map(member.technologies, (technology, i) => <span key={i}> {technology.kind === ETechnologyKind.FRONT ? technology.name + ',': ''}</span>)}
              </p>
              <p>
                {i18n.t('Back end: ')}
                {_.map(member.technologies, (technology, i) => <span key={i}> {technology.kind === ETechnologyKind.BACK ? technology.name + ',': ''}</span>)}
              </p>
              <div>
                {i18n.t('Projects: ', 'Проекты: ')}
                <ul>
                  {_.map(member.projects, (project, i) => <li key={i}>{this.renderProjectName(project)}</li>)}
                </ul>
              </div>
            </Media.Body>
          </Media>
        </div>
      </Col>
    )
  }

  renderMembers () {
    const comparator = (member: ITeamMember) => -member.user.priority;

    return (
      <Grid>
        {_.map(_.sortBy(this.props.members, comparator), (member, i) => <Row key={i}>{this.renderMember(member)}</Row>)}
      </Grid>
    )
  }

  // отрисовка
  render () {
    return (
      <div className="experience-page">
        <div className="experience-page__title">
          {this.renderTechnologies()}
        </div>
        <div className="experience-page__body">
          {this.renderMembers()}
        </div>
      </div>
    )
  }
}

/*
 * Контейнер
 */
const mapStateToProps = function (state: IAppStore): IProps {
  const { members } = state.experience.experiencePage;
  const { projects } = state.projects.projectsPage;
  const { user } = state.main.loginPage;

  return { members, user, projects } as IProps;
};

const mapDispatchToProps = function (dispatch: Redux.Dispatch<IAppStore>): IProps {
  const actions: IExperiencePageActions = new ExperiencePageActions(experiencePageService, dispatch);
  const projectsActions: IProjectsPageActions = new ProjectsPageActions(projectsPageService, dispatch);

  return { actions, projectsActions } as IProps;
};

const ExperienceListPage = withRouter(ReactRedux.connect(mapStateToProps, mapDispatchToProps)(CExperienceListPage) as React.ComponentClass<RouteComponentProps<IProps>>);

export { ExperienceListPage };
