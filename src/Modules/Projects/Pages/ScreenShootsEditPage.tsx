import * as _ from 'lodash';
import * as React from 'react';
import * as Redux from 'redux';
import * as ReactRedux from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router';
import { Grid, Row, Col, Image, Media, Button } from 'react-bootstrap';
import { FormGroup, FormControl, ControlLabel, HelpBlock } from 'react-bootstrap';

import { i18n } from 'Core/Utils';
import { IValidationState, IValidation } from 'Core/Models/Validation';

import { TScreenShootKey, IUser, IScreenShoot, ERoles, IResource } from 'Common/Models';
import { Autocomplete } from 'Common/Components/Autocomplete';

import { IScreenShootsPageActions } from 'Modules/Projects/Redux/Actions/IScreenShootsPageActions';
import { ScreenShootsPageActions } from 'Modules/Projects/Redux/Actions/ScreenShootsPageActions';
import { screenShootsPageService } from 'Modules/Projects/Services/SnapShootsPageService';
import { IAppStore } from 'Modules/store';

type TImagesAutocomplete = new() => Autocomplete<IResource>;
const ImagesAutocomplete = Autocomplete as TImagesAutocomplete;

/*
 * Передаваемые параметры
 */
interface IProps {
  screenShoots: IScreenShoot[]; // screenShoots list
  screenShoot: IScreenShoot; // screenShoot to edit
  actions: IScreenShootsPageActions; // CRUD actions
  images: IResource[]; // all available images
  user: IUser; // current user
}

/*
 * Внутреннее состояние
 */
interface IState {
  screenShoot: IScreenShoot;
  validation: IValidation<IScreenShoot>;
  isSended: boolean;
}

/*
 * Cтраница редактирования скриншота.
 */
class CScreenShootsEditPage extends React.Component<RouteComponentProps<IProps> & IProps, IState> {

  state: IState = {
    screenShoot: { } as IScreenShoot,
    validation: {
      fileName: { pattern: /.+/i, required: true } as IValidationState,
      description: { pattern: /.+/i, required: true } as IValidationState,
      image: { pattern: /.+/i, required: true } as IValidationState
    },
    isSended: false
  };

  componentWillMount () {
    this.setState({ screenShoot: _.assign({}, this.state.screenShoot, this.props.screenShoot) });

    if (this.props.screenShoots.length === 0) {
      this.props.actions.loadScreenShoots();
    }

    if (this.props.images.length === 0) {
      this.props.actions.loadScreenShootsAvailableImages();
    }
  }

  handleSubmit = () => {
    let formValid = true;
    for (let field of _.keys(this.state.validation) as TScreenShootKey[]) {
      formValid = formValid && this.handleInput(field);
    }

    if (formValid) {
      let screenShoot: IScreenShoot = _.assign({}, this.props.screenShoot, this.state.screenShoot);
      this.props.actions.saveScreenShoot(screenShoot)
        .then(() => this.setState({ isSended: true }));
    }
  };

  handleSelectImage = (r: IResource) => {
    let screenShoot = _.assign({}, this.state.screenShoot);

    screenShoot.image = r.url;
    screenShoot.fileName = r.name;

    this.setState({ screenShoot })
  }

  handleInput = (key: TScreenShootKey, e?) => {
    let screenShoot = _.assign({}, this.state.screenShoot);
    screenShoot[key] = e ? e.target.value : screenShoot[key];
    let validation = _.assign({}, this.state.validation);
    let validationFiled = validation[key];
    let isValid = true;

    if (validationFiled.required && (!screenShoot[key] || screenShoot[key].length === 0)) {
      isValid = false;
      validationFiled.state = 'warning';
      validationFiled.text = i18n.t('Required field', 'Обязательное поле');
    } else {
      isValid = validationFiled.pattern.test(screenShoot[key]);
      validationFiled.state = isValid ? 'success' : 'error';
      validationFiled.text = isValid ? '' : i18n.t('Incorrect field', 'Некорректное поле');
    }

    this.setState({ screenShoot, validation });

    return isValid;
  };

  renderDescriptionInput () {
    return (
      <FormGroup className="screen-shoots-page__form-item"
                 controlId="formHorizontalDescription"
                 validationState={this.state.validation.description.state}>
        <Col componentClass={ControlLabel}>
          {i18n.t('Description', 'Описание')}
        </Col>
        <Col>
          <FormControl className="screen-shoots-page__form-item-description"
                       type="text"
                       defaultValue={this.props.screenShoot.description}
                       onInput={e => this.handleInput('description', e)}/>
          <HelpBlock>{this.state.validation.description.text}</HelpBlock>
        </Col>
      </FormGroup>
    );
  }

  //
  // images
  //
  renderImagePicker () {
    let images = _.filter(this.props.images, (image) => this.state.screenShoot.fileName !== image.name);

    return (
      <ImagesAutocomplete list={images}
                          placeHolder={i18n.t('Select image', 'Выбрать картинку')}
                          onSelect={this.handleSelectImage}
                          displayFn={(e: IResource) => `${e.id} - ${e.url}`}
                          searchFn={(e: IResource, text: string) => _.includes(e.name, text)}
                          refName="imagesAutocomplete"
      />
    );
  }

  renderSelectedImage () {
    if (!this.state.screenShoot.fileName) {
      return null;
    }

    return (
      <FormGroup className="experience-page__form-item"
                 controlId="formHorizontalScreenShoots">
        <Col componentClass={ControlLabel}>
          {i18n.t('Screen Shoot: ', 'Выбранное изображение: ')}
          <span>{this.state.screenShoot.fileName}</span>
          <span>{this.renderImageDeleteButton()}</span>
        </Col>
      </FormGroup>
    )
  }

  renderImageDeleteButton () {
    const handleDelete = () => {
      let screenShoot = _.assign({}, this.state.screenShoot);

      screenShoot.image = null;
      screenShoot.fileName = null;

      this.setState({ screenShoot })
    }

    return (
      <Button bsStyle="link" onClick={handleDelete}>
        {i18n.t('[delete]', '[удалить]')}
      </Button>
    )
  }

  renderSubmitButton () {
    if (this.props.user.role !== ERoles.guest) {
      return (
        <FormGroup className="screen-shoots-page__form-item">
          <Col>
            <Button type="submit" onClick={this.handleSubmit}>
              {i18n.t('Submit', 'Отправить')}
            </Button>
          </Col>
        </FormGroup>
      );
    }
  }

  //
  // form
  //
  renderTitle () {
    return (
      <FormGroup className="screen-shoots-page__form-item">
        <Col componentClass={ControlLabel}>
          {i18n.t('Screen shoot', 'Проект')}
        </Col>
      </FormGroup>
    );
  }

  renderForm () {
    if (!this.state.isSended) {
      return (
        <form className="screen-shoots-page__form" autoComplete="off">
          {this.renderTitle()}
          {this.renderDescriptionInput()}
          {this.renderImagePicker()}
          {this.renderSelectedImage()}
          {this.renderSubmitButton()}
        </form>
      );
    } else {
      return (
        <form className="screen-shoots-page__form" autoComplete="off">
          <div className="screen-shoots-page__form-notification">
            {i18n.t('Data sended successfully!', 'Данные успешно отправлены!')}
          </div>
        </form>
      );
    }
  }

  // отрисовка
  render () {
    return (
      <div className="screen-shoots-page">
        <div className="screen-shoots-page__title">

        </div>
        <div className="screenShoots-page__body">
          {this.renderForm()}
        </div>
      </div>
    );
  }
}

/*
 * Контейнер
 */
const mapStateToProps = function (state: IAppStore): IProps {
  const { screenShoot, screenShoots, images } = state.projects.screenShootsPage;
  const { user } = state.main.loginPage;

  return { screenShoot, screenShoots, user, images } as IProps;
};

const mapDispatchToProps = function (dispatch: Redux.Dispatch<IAppStore>): IProps {
  const actions: IScreenShootsPageActions = new ScreenShootsPageActions(screenShootsPageService, dispatch);

  return { actions } as IProps;
};

const ScreenShootsEditPage = withRouter(ReactRedux.connect(mapStateToProps, mapDispatchToProps)(CScreenShootsEditPage) as React.ComponentClass<RouteComponentProps<IProps>>);

export { ScreenShootsEditPage };
