import * as Agent from 'superagent';
import * as AgentMockFactory from 'superagent-mocker';

import { config } from 'Core/config';

/*
 * Restul сервис
 */
abstract class GenericService {
  private agent: any;
  private mock: any;

  protected url: string;
  protected abstract setupMocks (agent);

  constructor (url) {
    this.url = config.restApiUrl + url;
    this.agent = Agent;

    if (config.mock) {
      this.mock = AgentMockFactory(this.agent);
      this.setupMocks(this.mock);
    }
  }

  private method<T> (url, chunk) {
    return new Promise<T>((resolve, reject) =>
      chunk(this.agent, this.url + url).end((err, res) => {
          if (err) {
            reject(res);
          } else {
            resolve(res.data);
          }
      })
    )
  }

  protected logPost (params) {
    console.log('POST ' + params.url, params.body);
  }

  protected logPut (params) {
    console.log('PUT ' + params.url, params.body);
  }

  protected logDelete (params) {
    console.log('DELETE ' + params.url, params.body);
  }

  protected logGet (params) {
    console.log('GET ' + params.url, params.query);
  }

  protected get<T> (url = '', data = {}) {
    return this.method<T>(url, (chunk, url) => chunk.get(url).query(data));
  }

  protected post<T> (url = '', data = {}) {
    return this.method<T>(url, (chunk, url) => chunk.post(url, data));
  }

  protected put<T> (url = '', data = {}) {
    return this.method<T>(url, (chunk, url) => chunk.put(url, data));
  }

  protected del (url = '') {
    return this.method<string>(url, (chunk, url) => chunk.del(url));
  }
}

export { GenericService };
