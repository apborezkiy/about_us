import * as _ from 'lodash';
import { push } from 'connected-react-router';

import { GenericActions } from 'Core/Generic';
import { ActionUtils } from 'Core/Redux';
import { pageToRouteInfo } from 'Core/Utils';

import { ITeamMember } from 'Common/Models';
import { IExperiencePageActions, experiencePageActions } from 'Modules/Experience/Redux/Actions/IExperiencePageActions';
import { IExperiencePageService } from 'Modules/Experience/Services/IExperiencePageService';

import { EExperiencePages, experienceRoutes } from '../../Pages';

const routeList: string = pageToRouteInfo<EExperiencePages>(EExperiencePages.EXPERIENCE_LIST_PAGE, experienceRoutes).path;
const routeEdit: string = pageToRouteInfo<EExperiencePages>(EExperiencePages.EXPERIENCE_EDIT_PAGE, experienceRoutes).path;

/*
 * Действия страницы
 */
class ExperiencePageActions extends GenericActions implements IExperiencePageActions {
  constructor (private api: IExperiencePageService, dispatch) {
    super(dispatch);
  }

  // Загрузить список
  loadExperience (): Promise<ITeamMember[]> {
    const loadExperience = () => this.api.loadExperience().catch(this.redirectError) as Promise<ITeamMember[]>;
    return ActionUtils.dispatchAsyncResponseBound<ITeamMember[]>(this.dispatch, experiencePageActions.LOAD_EXPERIENCE, loadExperience);
  }

  // Открыть страницу редактирования имеющегося или нового элемента
  editExperience (member: ITeamMember) {
    async function navigate () {
      return await new Promise<ITeamMember>(resolve => {
        setTimeout(() => this.dispatch(push(routeEdit)), 100);
        resolve(member);
      });
    }

    ActionUtils.dispatchAsyncResponseBound<void>(this.dispatch, experiencePageActions.EDIT_EXPERIENCE, navigate.bind(this));
  }

  // Сохранить изменения имеющегося или нового элемента
  saveExperience (member: ITeamMember): Promise<ITeamMember> {
    const saveExperience = () => {
      if (member.id !== undefined) {
        return this.api.saveExperience(member)
          .then(data => { setTimeout(() => this.dispatch(push(routeList)), 100); return data })
          .catch(this.redirectError) as Promise<ITeamMember>;
      } else {
        return this.api.createExperience(member)
          .then(data => { setTimeout(() => this.dispatch(push(routeList)), 100); return data })
          .catch(this.redirectError) as Promise<ITeamMember>;
      }
    }

    return ActionUtils.dispatchAsyncResponseBound<ITeamMember>(this.dispatch, experiencePageActions.SAVE_EXPERIENCE, saveExperience);
  }

  // Удалить элемент
  deleteExperience (member: ITeamMember): Promise<string> {
    const deleteExperience = () => {
      return this.api.deleteExperience(member).catch(this.redirectError) as Promise<string>;
    }

    return ActionUtils.dispatchAsyncResponseBound<string>(this.dispatch, experiencePageActions.DELETE_EXPERIENCE, deleteExperience);
  }
}

export { ExperiencePageActions };
