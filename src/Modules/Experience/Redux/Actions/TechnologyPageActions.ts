import * as _ from 'lodash';
import { push } from 'connected-react-router';

import { GenericActions } from 'Core/Generic';
import { ActionUtils } from 'Core/Redux';
import { pageToRouteInfo } from 'Core/Utils';

import { ITechnology } from 'Common/Models';
import { ITechnologyPageActions, technologyPageActions } from 'Modules/Experience/Redux/Actions/ITechnologyPageActions';
import { ITechnologyPageService } from 'Modules/Experience/Services/ITechnologyPageService';

import { EExperiencePages, experienceRoutes } from '../../Pages';

const routeList: string = pageToRouteInfo<EExperiencePages>(EExperiencePages.TECHNOLOGY_LIST_PAGE, experienceRoutes).path;
const routeEdit: string = pageToRouteInfo<EExperiencePages>(EExperiencePages.TECHNOLOGY_EDIT_PAGE, experienceRoutes).path;

/*
 * Действия страницы
 */
class TechnologyPageActions extends GenericActions implements ITechnologyPageActions {
  constructor (private api: ITechnologyPageService, dispatch) {
    super(dispatch);
  }

  // Загрузить список
  loadTechnologies (): Promise<ITechnology[]> {
    const loadTechnologies = () => this.api.loadTechnologies().catch(this.redirectError) as Promise<ITechnology[]>;
    return ActionUtils.dispatchAsyncResponseBound<ITechnology[]>(this.dispatch, technologyPageActions.LOAD_TECHNOLOGIES, loadTechnologies);
  }

  // Открыть страницу редактирования имеющегося или нового элемента
  editTechnology (technology: ITechnology) {
    async function navigate () {
      return await new Promise<ITechnology>(resolve => {
        setTimeout(() => this.dispatch(push(routeEdit)), 100);
        resolve(technology);
      });
    }

    ActionUtils.dispatchAsyncResponseBound<void>(this.dispatch, technologyPageActions.EDIT_TECHNOLOGY, navigate.bind(this));
  }

  // Сохранить изменения имеющегося или нового элемента
  saveTechnology (technology: ITechnology): Promise<ITechnology> {
    const saveTechnology = () => {
      if (technology.id !== undefined) {
        return this.api.saveTechnology(technology)
          .then(data => { setTimeout(() => this.dispatch(push(routeList)), 100); return data })
          .catch(this.redirectError) as Promise<ITechnology>;
      } else {
        return this.api.createTechnology(technology)
          .then(data => { setTimeout(() => this.dispatch(push(routeList)), 100); return data })
          .catch(this.redirectError) as Promise<ITechnology>;
      }
    };

    return ActionUtils.dispatchAsyncResponseBound<ITechnology>(this.dispatch, technologyPageActions.SAVE_TECHNOLOGY, saveTechnology);
  }

  // Удалить элемент
  deleteTechnology (technology: ITechnology): Promise<string> {
    const deleteTechnology = () => {
      return this.api.deleteTechnology(technology).catch(this.redirectError) as Promise<string>;
    };

    return ActionUtils.dispatchAsyncResponseBound<string>(this.dispatch, technologyPageActions.DELETE_TECHNOLOGY, deleteTechnology);
  }
}

export { TechnologyPageActions };
