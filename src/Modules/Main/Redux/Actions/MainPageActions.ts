import { push } from 'connected-react-router';

import { GenericActions } from 'Core/Generic';
import { ActionUtils } from 'Core/Redux';

import { IMainPageActions, mainPageActions } from 'Modules/Main/Redux/Actions/IMainPageActions';
import { IMainPageService } from 'Modules/Main/Services/IMainPageService';

/*
 * Действия страницы
 */
class MainPageActions extends GenericActions implements IMainPageActions {
  constructor (private api: IMainPageService, dispatch) {
    super(dispatch);
  }
}

export { MainPageActions };
