import { IOffer } from 'Common/Models';

/*
 * API
 */
export interface IContactsPageService {
  createOffer: (offer: IOffer) => Promise<void>;
}
