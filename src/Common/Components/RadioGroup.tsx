import * as _ from 'lodash';
import * as React from 'react';
import { Radio } from 'react-bootstrap';

/*
 * Radio button description
 */
interface IOption<T> {
  id: number;
  label: string;
  value: T;
}

/*
 * Component properties
 */
interface IProps<T> {
  options: IOption<T>[];
  callback?: (event) => void;
  option?: IOption<T>;
  disabled?: boolean;
}

/*
 * Component state
 */
interface IState<T> {
}

/**
 * Radio group UI component
 */
class RadioGroup<T> extends React.Component<IProps<T>, IState<T>> {

  handleChange = (e) => {
    let { options, callback } = this.props;
    let id = e.target.value;

    if (callback) {
      callback({ target: { value: _.find(options, (o: IOption<T>) => o.id == id).value }});
    }
  };

  // отрисовка
  render () {
    const { options, option } = this.props;

    let content = _.map(options, (o: IOption<T>, index) => {
      return (
        <Radio className="radio-group__item"
          onChange={this.handleChange}
          checked={option && (o.id == option.id)}
          value={o.id}
          disabled={!!this.props.disabled}
          key={index}
          inline>
            {o.label}
        </Radio>
      );
    });

    return (
      <div className="radio-group">
        {content}
      </div>
    );
  }
}

export { RadioGroup, IOption as IRadioGroupSelectOption };
