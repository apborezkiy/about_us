import { config } from '../config';

const locale_en = "en";
const locale_ru = "ru";

class i18n {
  private value: string[];
  private static locale;

  constructor (en, ru) {
    this.value = [];
    this.value[locale_en] = en;
    this.value[locale_ru] = ru;
  }

  get text () {
    return this.value[i18n.locale] || this.value[locale_en];
  }

  static setLocale (locale) {
    i18n.locale = locale;
  }

  static t (en, ru = null) {
    return (new i18n(en, ru)).text;
  }
}

i18n.setLocale (config.lang);

export { i18n };
