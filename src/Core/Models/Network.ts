export enum EErrors {
  NETWORK = 1,
  INTERNAL = 2,
  EXTERNAL = 3,
}

export const errorsMap = {
  0: EErrors.INTERNAL,
  400: EErrors.NETWORK,
  404: EErrors.NETWORK,
  500: EErrors.EXTERNAL,
  502: EErrors.NETWORK,
}

export interface IErrorsResult {

}
