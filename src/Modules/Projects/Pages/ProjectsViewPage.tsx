import * as _ from 'lodash';
import * as React from 'react';
import * as Redux from 'redux';
import * as ReactRedux from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router';
import { Grid, Row, Col, Image, Media, Button } from 'react-bootstrap';
import { FormGroup, FormControl, ControlLabel, HelpBlock, Carousel } from 'react-bootstrap';

import { i18n } from 'Core/Utils';

import { TProjectKey, IUser, IProject, ERoles, EPriorities, IScreenShoot } from 'Common/Models';
import { getRoleName, getPriorityName } from 'Common/Constants';

import { ECompositePages, EMainPages } from 'Modules/Main/Pages';
import { MainMenuActions } from 'Modules/Main/Redux/Actions/MainMenuActions';
import { IProjectsPageActions } from 'Modules/Projects/Redux/Actions/IProjectsPageActions';
import { ProjectsPageActions } from 'Modules/Projects/Redux/Actions/ProjectsPageActions';
import { projectsPageService } from 'Modules/Projects/Services/ProjectsPageService';
import { IAppStore } from 'Modules/store';
import { EProjectsPages } from './index';

/*
 * Передаваемые параметры
 */
interface IProps {
  projects: IProject[]; // projects list
  project: IProject; // project to view
  actions: IProjectsPageActions; // CRUD actions
  user: IUser; // current user
  redirectToListPage: () => void;
}

/*
 * Внутреннее состояние
 */
interface IState {
  project: IProject;
}

/*
 * Cтраница просмотра проекта.
 */
class CProjectsViewPage extends React.Component<RouteComponentProps<IProps> & IProps, IState> {

  state: IState = {
    project: { screenShoots: [] } as IProject,
  };

  componentWillMount () {
    this.setState({ project: _.assign({}, this.state.project, this.props.project) });

    if (!_.isEmpty(this.props.project)) {
      if (this.props.projects.length === 0) {
        this.props.actions.loadProjects();
      }
    } else {
      this.props.redirectToListPage();
    }
  }

  renderNameView () {
    return (
      <FormGroup className="projects-page__form-item"
        controlId="formHorizontalName">
        <Col componentClass={ControlLabel}>
          {this.props.project.name}
        </Col>
      </FormGroup>
    );
  }

  renderForm () {
    return (
      <form className="projects-page__form" autoComplete="off">
        <Media>
          <Media.Left>
            <Image width={128} height={128} className="projects-page__photo" src={this.props.project.avatar} rounded />
          </Media.Left>
          <Media.Body>
            <Media.Heading>{this.renderNameView()}</Media.Heading>
            <p>{this.props.project.description}</p>
          </Media.Body>
        </Media>
      </form>
    );
  }

  renderScreenShoot = (screenShoot: IScreenShoot, i) => {
    return (
      <Carousel.Item key={i}>
        <Image width={500} height={200} className="projects-page__screen-shoot" src={screenShoot.image} rounded />
        <Carousel.Caption>
          <h3>{screenShoot.description}</h3>
        </Carousel.Caption>
      </Carousel.Item>
    );
  }

  renderScreenShoots () {
    if (this.state.project.screenShoots.length === 0) {
      return null;
    }

    return (
      <Grid>
        <Row>
          <Col className="main-page__carousel-wrapper">
            <div className="main-page__carousel">
              <Carousel>
                {_.map(this.state.project.screenShoots, this.renderScreenShoot)}
              </Carousel>
            </div>
          </Col>
        </Row>
      </Grid>
    )
  }

  // отрисовка
  render () {
    return (
      <div className="projects-page">
        <div className="projects-page__title">

        </div>
        <div className="projects-page__body">
          {this.renderForm()}
          {this.renderScreenShoots()}
        </div>
      </div>
    );
  }
}

/*
 * Контейнер
 */
const mapStateToProps = function (state: IAppStore): IProps {
  const { project, projects } = state.projects.projectsPage;
  const { user } = state.main.loginPage;

  return { project, projects, user } as IProps;
};

const mapDispatchToProps = function (dispatch: Redux.Dispatch<IAppStore>): IProps {
  const actions: IProjectsPageActions = new ProjectsPageActions(projectsPageService, dispatch);
  const redirectToListPage = () => new MainMenuActions(null, dispatch).navigateMainMenu(EProjectsPages.PROJECTS_LIST_PAGE as ECompositePages);

  return { actions, redirectToListPage } as IProps;
};

const ProjectsViewPage = withRouter(ReactRedux.connect(mapStateToProps, mapDispatchToProps)(CProjectsViewPage) as React.ComponentClass<RouteComponentProps<IProps>>);

export { ProjectsViewPage };
