import * as _ from 'lodash';

import { GenericService } from 'Core/Generic';

import { ITeamMember, ITechnology, IUser, ETechnologyKind, EPriorities } from 'Common/Models';
import { IExperiencePageService } from 'Modules/Experience/Services/IExperiencePageService';

/*
 * Restul сервис
 */
class ExperiencePageService extends GenericService implements IExperiencePageService {
  constructor () {
    super('/experience');
  }

  loadExperience (): Promise<ITeamMember[]> {
    return this.get<ITeamMember[]>();
  }

  saveExperience (member: ITeamMember): Promise<ITeamMember> {
    return this.put<ITeamMember>(`/${member.id}`, member);
  }

  createExperience (member: ITeamMember): Promise<ITeamMember> {
    return this.post<ITeamMember>('', member);
  }

  deleteExperience (member: ITeamMember): Promise<string> {
    return this.del(`/${member.id}`);
  }

  setupMocks (agent) {
    // load experience
    agent.get(this.url, (params) => {
      this.logGet(params);

      let members: ITeamMember[] = [
        {
          id: '1',
          user: { name: 'Arseniy Boretskiy', id: '1' } as IUser,
          position: 'Senior fullstack developer',
          experience: '8 years developing experience',
          image: 'assets/Apborezkiy_Big.jpg',
          priority: 1,
          technologies: [
              { name: 'React + Redux', id: '1', kind: ETechnologyKind.FRONT} as ITechnology,
              { name: 'Angular 2', id: '1', kind: ETechnologyKind.FRONT} as ITechnology,
              { name: 'Angular 1', id: '1', kind: ETechnologyKind.FRONT } as ITechnology,
              { name: 'TypeScript', id: '1', kind: ETechnologyKind.FRONT } as ITechnology,
              { name: 'Erlang', id: '1', kind: ETechnologyKind.BACK } as ITechnology,
              { name: 'Node js', id: '1', kind: ETechnologyKind.BACK } as ITechnology
            ],
          projects: [
            { name: 'Sberbank business online', id: '1', priority: EPriorities.high }
          ]
        },
        {
          id: '2',
          user: { name: 'Arseniy Borezkiy', id: '1' } as IUser,
          position: 'Senior fullstack developer',
          experience: '8 years developing experience',
          image: 'assets/Apborezkiy_Big.jpg',
          priority: 2,
          technologies: [
            { name: 'React + Redux', id: '1', kind: ETechnologyKind.FRONT} as ITechnology,
            { name: 'Angular 2', id: '1', kind: ETechnologyKind.FRONT} as ITechnology,
            { name: 'Angular 1', id: '1', kind: ETechnologyKind.FRONT } as ITechnology,
            { name: 'TypeScript', id: '1', kind: ETechnologyKind.FRONT } as ITechnology,
            { name: 'Erlang', id: '1', kind: ETechnologyKind.BACK } as ITechnology,
            { name: 'Node js', id: '1', kind: ETechnologyKind.BACK } as ITechnology
          ],
          projects: [
            { name: 'Sberbank business online', id: '2', priority: EPriorities.highest }
          ]
        }
      ];

      return { data: members };
    });

    // save experience
    agent.put(this.url + '/:id', (params) => {
      this.logPut(params);

      let body = params.body;

      let member: ITeamMember = _.assign({}, body, {
        name: body.name
      });

      return { data: member };
    });

    // create experience
    agent.post(this.url, (params) => {
      this.logPut(params);

      let body = params.body;

      let member: ITeamMember = _.assign({}, body, {
        id: (new Date()).getMilliseconds().toString(),
        name: body.name
      });

      return { data: member };
    });

    // delete experience
    agent.del(this.url + '/:id', (params) => {
      this.logDelete(params);

      return { data: params.params.id };
    });
  }
}

const experiencePageService = new ExperiencePageService();

export { experiencePageService, IExperiencePageService };
