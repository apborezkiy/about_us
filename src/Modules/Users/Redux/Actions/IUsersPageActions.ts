import { IUser } from 'Common/Models';

/*
 * Actions
 */
export const usersPageActions = {
  LOAD_USERS: 'LOAD_USERS',
  EDIT_USER: 'EDIT_USER',
  SAVE_USER: 'SAVE_USER',
  DELETE_USER: 'DELETE_USER'
};

export interface IUsersPageActions {
  loadUsers: () => Promise<IUser[]>;
  editUser: (user: IUser) => void;
  saveUser: (user: IUser) => Promise<IUser>;
  deleteUser: (user: IUser) => Promise<string>;
}
