import { IOffer } from 'Common/Models';

/*
 * Actions
 */
export const contactsPageActions = {
  CREATE_OFFER: 'CREATE_OFFER'
};

export interface IContactsPageActions {
  createOffer: (offer: IOffer) => Promise<void>
}
