/*
 * Логирующий усилитель
 */
export function loggerMiddleware (store) {
  return next => action => {
    if (typeof action === "object")
      console.log('Action: ', action);
    return next(action);
  };
}
