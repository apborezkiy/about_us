import * as _ from 'lodash';
import { push } from 'connected-react-router';

import { GenericActions } from 'Core/Generic';
import { ActionUtils } from 'Core/Redux';
import { pageToRouteInfo } from 'Core/Utils';

import { IUser } from 'Common/Models';
import { IUsersPageActions, usersPageActions } from 'Modules/Users/Redux/Actions/IUsersPageActions';
import { IUsersPageService } from 'Modules/Users/Services/IUsersPageService';

import { EUsersPages, usersRoutes } from '../../Pages';

const routeList: string = pageToRouteInfo<EUsersPages>(EUsersPages.USER_LIST_PAGE, usersRoutes).path;
const routeEdit: string = pageToRouteInfo<EUsersPages>(EUsersPages.USER_EDIT_PAGE, usersRoutes).path;

/*
 * Действия страницы
 */
class UsersPageActions extends GenericActions implements IUsersPageActions {
  constructor (private api: IUsersPageService, dispatch) {
    super(dispatch);
  }

  // Загрузить список
  loadUsers (): Promise<IUser[]> {
    const loadUsers = () => this.api.loadUsers().catch(this.redirectError) as Promise<IUser[]>;
    return ActionUtils.dispatchAsyncResponseBound<IUser[]>(this.dispatch, usersPageActions.LOAD_USERS, loadUsers);
  }

  // Открыть страницу редактирования имеющегося или нового элемента
  editUser (user: IUser) {
    async function navigate () {
      return await new Promise<IUser>(resolve => {
        setTimeout(() => this.dispatch(push(routeEdit)), 100);
        resolve(user);
      });
    }

    ActionUtils.dispatchAsyncResponseBound<void>(this.dispatch, usersPageActions.EDIT_USER, navigate.bind(this));
  }

  // Сохранить изменения имеющегося или нового элемента
  saveUser(user: IUser): Promise<IUser> {
    const saveUsers = () => {
      if (user.id !== undefined) {
        return this.api.saveUser(user)
          .then(data => { setTimeout(() => this.dispatch(push(routeList)), 100); return data })
          .catch(this.redirectError) as Promise<IUser>;
      } else {
        return this.api.createUser(user)
          .then(data => { setTimeout(() => this.dispatch(push(routeList)), 100); return data })
          .catch(this.redirectError) as Promise<IUser>;
      }
    }

    return ActionUtils.dispatchAsyncResponseBound<IUser>(this.dispatch, usersPageActions.SAVE_USER, saveUsers);
  }

  // Удалить элемент
  deleteUser (user: IUser): Promise<string> {
    const deleteUsers = () => {
      return this.api.deleteUser(user).catch(this.redirectError) as Promise<string>;
    }

    return ActionUtils.dispatchAsyncResponseBound<string>(this.dispatch, usersPageActions.DELETE_USER, deleteUsers);
  }
}

export { UsersPageActions };
