import { push } from 'connected-react-router';

import { GenericActions } from 'Core/Generic';
import { ActionUtils } from 'Core/Redux';
import { pageToRouteInfo } from 'Core/Utils';

import { IScreenShoot, IResource } from 'Common/Models';
import { IScreenShootsPageActions, screenShootsPageActions } from 'Modules/Projects/Redux/Actions/IScreenShootsPageActions';
import { IScreenShootsPageService } from '../../Services/ISnapShootsPageService';

import { EProjectsPages, projectsRoutes } from '../../Pages';

const routeList: string = pageToRouteInfo<EProjectsPages>(EProjectsPages.SCREENSHOOTS_LIST_PAGE, projectsRoutes).path;
const routeEdit: string = pageToRouteInfo<EProjectsPages>(EProjectsPages.SCREENSHOOTS_EDIT_PAGE, projectsRoutes).path;

/*
 * Действия страницы контактов
 */
class ScreenShootsPageActions extends GenericActions implements IScreenShootsPageActions {
  constructor (private api: IScreenShootsPageService, dispatch) {
    super(dispatch);
  }

  // Загрузить список
  loadScreenShoots (): Promise<IScreenShoot[]> {
    const loadScreenShoots = () => this.api.loadScreenShoots().catch(this.redirectError) as Promise<IScreenShoot[]>;
    return ActionUtils.dispatchAsyncResponseBound<IScreenShoot[]>(this.dispatch, screenShootsPageActions.LOAD_SCREEN_SHOOTS, loadScreenShoots);
  }

  // Загрузить список
  loadScreenShootsAvailableImages (): Promise<IResource[]> {
    const loadScreenShootsImages = () => this.api.loadScreenShootsImages().catch(this.redirectError) as Promise<IResource[]>;
    return ActionUtils.dispatchAsyncResponseBound<IResource[]>(this.dispatch, screenShootsPageActions.LOAD_SCREEN_SHOOTS_AVAILABLE_IMAGES, loadScreenShootsImages);
  }

  // Открыть страницу редактирования имеющегося или нового элемента
  editScreenShoot (screenShoot: IScreenShoot) {
    async function navigate () {
      return await new Promise<IScreenShoot>(resolve => {
        setTimeout(() => this.dispatch(push(routeEdit)), 100);
        resolve(screenShoot);
      });
    }

    ActionUtils.dispatchAsyncResponseBound<void>(this.dispatch, screenShootsPageActions.EDIT_SCREEN_SHOOT, navigate.bind(this));
  }

  // Сохранить изменения имеющегося или нового элемента
  saveScreenShoot (screenShoot: IScreenShoot): Promise<IScreenShoot> {
    const saveScreenShoots = () => {
      if (screenShoot.id !== undefined) {
        return this.api.saveScreenShoot(screenShoot)
          .then(data => { setTimeout(() => this.dispatch(push(routeList)), 100); return data })
          .catch(this.redirectError) as Promise<IScreenShoot>;
      } else {
        return this.api.createScreenShoot(screenShoot)
          .then(data => { setTimeout(() => this.dispatch(push(routeList)), 100); return data })
          .catch(this.redirectError) as Promise<IScreenShoot>;
      }
    }

    return ActionUtils.dispatchAsyncResponseBound<IScreenShoot>(this.dispatch, screenShootsPageActions.SAVE_SCREEN_SHOOT, saveScreenShoots);
  }

  // Удалить элемент
  deleteScreenShoot (screenShoot: IScreenShoot): Promise<string> {
    const deleteScreenShoots = () => {
      return this.api.deleteScreenShoot(screenShoot).catch(this.redirectError) as Promise<string>;
    }

    return ActionUtils.dispatchAsyncResponseBound<string>(this.dispatch, screenShootsPageActions.DELETE_SCREEN_SHOOT, deleteScreenShoots);
  }
}

export { ScreenShootsPageActions };
