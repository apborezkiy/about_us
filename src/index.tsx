require('Styles/index.less');

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Route } from 'react-router';
import { ConnectedRouter } from 'connected-react-router/immutable';
import { createHashHistory } from 'history'

import { createStore } from 'Core/Redux';

import { AppLayout } from 'Modules/AppLayout';
import { IAppStore, appInitialStore } from 'Modules/store';
import { rootReducer } from 'Modules/rootReducer';

// init
const history = createHashHistory();
const store = createStore<IAppStore>(appInitialStore, history, rootReducer);

// hot reloading
if (module.hot) {
  module.hot.accept('Modules/rootReducer', () => {
    const nextReducer = require('Modules/rootReducer').rootReducer;
    store.replaceReducer(nextReducer);
  });
}

// render
ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Route key="/" path="/" component={AppLayout} />
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
);
