import * as Redux from 'redux';

import { contactsPageInitialStore, contactsPageReducer } from 'Modules/Contacts/Redux/Reducers/contactsPageReducer';
import { IContactsStore } from 'Modules/Contacts/Redux/Store';

/*
 * Редьюсер модуля
 */
const reducer: Redux.ReducersMapObject = {
  contactsPage: contactsPageReducer
}

const contactsReducer: Redux.Reducer<IContactsStore> = Redux.combineReducers<IContactsStore>(reducer);

/*
 * Начальное состояние хранилища модуля
 */
const contactsInitialStore: IContactsStore = {
  contactsPage: contactsPageInitialStore
}

export { contactsReducer, contactsInitialStore };
