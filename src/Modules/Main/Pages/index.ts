import { i18n } from 'Core/Utils';

import { EProjectsPages } from 'Modules/Projects';
import { EContactsPages } from 'Modules/Contacts';
import { EExperiencePages } from 'Modules/Experience';
import { EUsersPages } from 'Modules/Users';

/*
 * Сраницы модуля
 */
export enum EMainPages {
  LAYOUT = 10,
  MAIN_PAGE = 11,
  LOGIN_PAGE = 12
}

export const mainRoutes = {
  [EMainPages.LAYOUT]: { path: '/main', name: i18n.t('Main Layout', 'Главный раздел') },
  [EMainPages.MAIN_PAGE]: { path: '/main', name: i18n.t('Main Page', 'Главная') },
  [EMainPages.LOGIN_PAGE]: { path: '/login', name: i18n.t('Login', 'Войти') }
};

export type ECompositePages = EMainPages & EProjectsPages & EContactsPages & EExperiencePages & EUsersPages;

import { MainPage } from 'Modules/Main/Pages/MainPage';
import { LoginPage } from 'Modules/Main/Pages/LoginPage';
import { MainLayout } from 'Modules/Main/Pages/MainLayout';

/*
 * Компоненты страниц
 */
export {
  MainLayout,
  LoginPage,
  MainPage
};
