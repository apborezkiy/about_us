import * as _ from 'lodash';
import * as Redux from 'redux';
import * as ReduxActions from 'redux-actions';

import { SUCCESS } from 'Core/Models/Actions';
import { ITeamMember, ITechnology } from 'Common/Models';
import { ITechnologyPageStore } from 'Modules/Experience/Redux/Store';

import { technologyPageActions } from '../Actions/ITechnologyPageActions';

/*
 * Начальное состояние страницы
 */
export const technologyPageInitialStore: ITechnologyPageStore = {
  technologies: [],
  technology: {} as ITechnology
}

/*
 * Редьюсер страницы
 */
export const technologyPageReducer: Redux.Reducer<ITechnologyPageStore> = ReduxActions.handleActions({
  [`${technologyPageActions.LOAD_TECHNOLOGIES}_${SUCCESS}`]: (state: ITechnologyPageStore, action) => {
    return _.assign({}, state, { technologies: action.payload });
  },
  [`${technologyPageActions.EDIT_TECHNOLOGY}_${SUCCESS}`]: (state: ITechnologyPageStore, action: ReduxActions.Action<ITechnology>) => {
    return _.assign({}, state, { technology: action.payload });
  },
  [`${technologyPageActions.SAVE_TECHNOLOGY}_${SUCCESS}`]: (state: ITechnologyPageStore, action: ReduxActions.Action<ITechnology>) => {
    let newTechnology = action.payload;

    let technologies = _.filter(state.technologies, (technology) => technology.id !== newTechnology.id) as ITechnology[];
    let newTechnologies = _.sortBy([...technologies, newTechnology], (o: ITechnology) => o.name);

    return _.assign({}, state, { technologies: newTechnologies });
  },
  [`${technologyPageActions.DELETE_TECHNOLOGY}_${SUCCESS}`]: (state: ITechnologyPageStore, action: ReduxActions.Action<ITechnology>) => {
    let technologyId = action.payload;

    let technologies = _.filter(state.technologies, (technology) => technology.id !== technologyId) as ITechnology[];
    let newTechnologies = _.sortBy([...technologies], (o: ITechnology) => o.name);

    return _.assign({}, state, { technologies: newTechnologies });
  }
}, technologyPageInitialStore)
