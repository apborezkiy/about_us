import { IContactsStore } from 'Modules/Contacts/Redux/Store';
import { contactsInitialStore, contactsReducer } from 'Modules/Contacts/Redux/Reducers';
import { ContactsLayout } from 'Modules/Contacts/Pages/ContactsLayout';
import { contactsRoutes, EContactsPages } from 'Modules/Contacts/Pages';

export {
  ContactsLayout,
  IContactsStore,
  contactsInitialStore,
  contactsReducer,
  EContactsPages,
  contactsRoutes
};
