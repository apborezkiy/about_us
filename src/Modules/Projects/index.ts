import { IProjectsStore } from 'Modules/Projects/Redux/Store';
import { projectsInitialStore, projectsReducer } from 'Modules/Projects/Redux/Reducers';
import { ProjectsLayout } from 'Modules/Projects/Pages/ProjectsLayout';
import { projectsRoutes, EProjectsPages } from 'Modules/Projects/Pages';

export {
  ProjectsLayout,
  IProjectsStore,
  projectsInitialStore,
  projectsReducer,
  EProjectsPages,
  projectsRoutes
};
