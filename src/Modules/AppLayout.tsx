
import * as React from 'react';
import { RouteComponentProps, Route, Redirect, Switch } from 'react-router';

import { ESystemPages, systemRoutes } from 'Core/Models/Pages';
import { pageToRouteInfo } from 'Core/Utils';

import { MainLayout, EMainPages, mainRoutes } from 'Modules/Main';

// application routes
const rootRoute = pageToRouteInfo<ESystemPages>(ESystemPages.ROOT, systemRoutes);
const errorRoute = pageToRouteInfo<ESystemPages>(ESystemPages.ERROR, systemRoutes);
const notFoundRoute = pageToRouteInfo<ESystemPages>(ESystemPages.NOT_FOUND, systemRoutes);
const loginRoute = pageToRouteInfo<EMainPages>(EMainPages.LOGIN_PAGE, mainRoutes);
const mainRoute = pageToRouteInfo<EMainPages>(EMainPages.LAYOUT, mainRoutes);

/*
 * Main application layout
 */
export class AppLayout extends React.Component<RouteComponentProps<void>> {
  // rendering section
  render () {
    return (
      <div className="app-layout">
        <Switch>
          <Route exact path={rootRoute.path} render={() => <Redirect from={rootRoute.path} to={mainRoute.path} />} />
          <Route path={mainRoute.path} component={MainLayout} />
          <Route path={loginRoute.path} component={MainLayout} />
          <Route path={errorRoute.path} component={MainLayout} />
          <Route path={notFoundRoute.path} component={MainLayout} />
          <Route path="*" render={() => <Redirect to={notFoundRoute.path} />} />
        </Switch>
      </div>
    )
  }
}
