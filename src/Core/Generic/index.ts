import { GenericService } from 'Core/Generic/GenericService';
import { GenericActions } from 'Core/Generic/GenericActions';

export {
  GenericService,
  GenericActions
}
