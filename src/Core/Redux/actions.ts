import * as Redux from 'redux';
import * as ReduxActions from 'redux-actions';

import { IErrorsResult } from 'Core/Models/Network';

export function dispatchError(actionType: string, dispatch) {
  return (error: IErrorsResult) => {
    dispatch({
      type: `${actionType}_FAILURE`,
      payload: error,
      error: true
    });

    return Promise.reject(error);
  }
}

export function dispatchSuccess<T>(actionType: string, dispatch, payloadConverter?: (payload: any) => T) {
  return (payload: T) => {
    dispatch({
      type: `${actionType}_SUCCESS`,
      payload: payloadConverter ? payloadConverter(payload) : payload
    });

    return payload;
  }
}

export function dispatchAsync<TBeginPayload, TResponsePayload>(
  actionType: string,
  asyncCall: () => Promise<TResponsePayload>,
  payload?: TBeginPayload) {

  return (dispatch) => {
    dispatch({type: `${actionType}_BEGIN`, payload});

    const p: Promise<TResponsePayload> = asyncCall();
    if (p) {
      p.then(
        dispatchSuccess<TResponsePayload>(actionType, dispatch),
        dispatchError(actionType, dispatch)
      )
    }

    return p;
  }
}

export function dispatchAsyncResponse<TResponsePayload>(
  actionType: string,
  asyncCall: () => Promise<TResponsePayload>) {

  return dispatchAsync<void, TResponsePayload>(actionType, asyncCall);
}

export function dispatchAsyncBound<TBeginPayload, TResponsePayload>(
  dispatch,
  actionType: string,
  asyncCall: () => Promise<TResponsePayload>,
  payload?: TBeginPayload) {

  return dispatch(dispatchAsync<TBeginPayload, TResponsePayload>(actionType, asyncCall, payload));
}

export function dispatchAsyncResponseBound<TResponsePayload>(
  dispatch,
  actionType: string,
  asyncCall: () => Promise<TResponsePayload>) {

  return dispatch(dispatchAsync<void, TResponsePayload>(actionType, asyncCall));
}
