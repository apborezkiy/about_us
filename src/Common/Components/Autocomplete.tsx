import * as _ from 'lodash';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Grid, Row, Col, Image, Media, Button, FormGroup, FormControl, ControlLabel, HelpBlock } from 'react-bootstrap';

/*
 * Component properties
 */
interface IProps<T> {
  list: Array<T>;
  onSelect: (e: T) => void;
  placeHolder: string;
  displayFn: (e: T) => string;
  searchFn: (e: T, text: string) => boolean;
  refName: string;
  displayMax?: number;
}

/*
 * Component state
 */
interface IState<T> {
  message: string;
  text: string;
  elements: Array<T>;
  displayMax: number;
}

/*
 * Auto completion UI component
 */
class Autocomplete<T> extends React.Component<IProps<T>, IState<T>> {

  state: IState<T> = {
    message: '',
    text: '',
    elements: [],
    displayMax: 5
  };

  componentWillMount () {
    if (_.isNumber(this.props.displayMax)) {
      this.setState({displayMax: this.props.displayMax});
    }
  }

  componentWillReceiveProps (nextProps) {
    if (this.state.text.trim().length > 0) {
      let elements = _
        .filter(this.props.list, (e: T) => this.props.searchFn(e, this.state.text))
        .slice(0, this.state.displayMax);

      this.setState({elements});
    }
  }

  handleSelect = (i: number) => {
    this.setState({ text: '' });
    (ReactDOM.findDOMNode(this.refs[this.props.refName]) as HTMLInputElement).value = '';
    this.props.onSelect(this.state.elements[i]);
  };

  handleInput = (e) => {
    let text = e.target.value;
    let elements = _
      .filter(this.props.list, (e: T) => this.props.searchFn(e, text))
      .slice(0, this.state.displayMax);

    this.setState({text, elements});
  };

  renderInput () {
    return (
      <FormGroup className="autocomplete"
                 controlId="autocomplete">
        <Col componentClass={ControlLabel}>
          {this.props.placeHolder}
        </Col>
        <Col>
          <FormControl className="autocomplete__input"
                       type="text"
                       ref={this.props.refName}
                       onInput={this.handleInput} />
          <HelpBlock>{this.state.message}</HelpBlock>
        </Col>
      </FormGroup>
    );
  }

  renderListElement (e: T, i: number) {
    return (
      <li key={i} onClick={() => this.handleSelect(i)}>{this.props.displayFn(e)}</li>
    )
  }

  renderList () {
    return (
      <ul>
        {_.map(this.state.elements, (e: T, i: number) => this.renderListElement(e, i))}
      </ul>
    )
  }

  // renderings section
  render () {
    return (
      <div>
        {this.renderInput()}
        {this.renderList()}
      </div>
    );
  }
}

export { Autocomplete };
