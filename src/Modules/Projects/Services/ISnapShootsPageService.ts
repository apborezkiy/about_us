import { IScreenShoot, IResource } from 'Common/Models';

/*
 * API
 */
export interface IScreenShootsPageService {
  loadScreenShoots: () => Promise<IScreenShoot[]>;
  loadScreenShootsImages: () => Promise<IResource[]>;
  createScreenShoot: (screenShoot: IScreenShoot) => Promise<IScreenShoot>;
  deleteScreenShoot: (screenShoot: IScreenShoot) => Promise<string>;
  saveScreenShoot: (screenShoot: IScreenShoot) => Promise<IScreenShoot>
}
