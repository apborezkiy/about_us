import { IProject } from 'Common/Models';

/*
 * Описание хранилища
 */
interface IProjectsPageStore {
  projects: IProject[];
  project: IProject; // edit
}

export {
  IProjectsPageStore
}
