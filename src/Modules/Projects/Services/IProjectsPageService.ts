import { IProject } from 'Common/Models';

/*
 * API
 */
export interface IProjectsPageService {
  loadProjects: () => Promise<IProject[]>;
  createProject: (project: IProject) => Promise<IProject>;
  deleteProject: (project: IProject) => Promise<string>;
  saveProject: (project: IProject) => Promise<IProject>
}
