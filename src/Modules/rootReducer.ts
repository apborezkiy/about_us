import * as Redux from 'redux';

import { mainReducer } from 'Modules/Main';
import { projectsReducer } from 'Modules/Projects';
import { experienceReducer } from 'Modules/Experience';
import { contactsReducer } from 'Modules/Contacts';
import { usersReducer } from 'Modules/Users';
import { IAppStore } from 'Modules/store';

/*
 * Root reducer of the application
 */
const reducer: Redux.ReducersMapObject = {
  main: mainReducer,
  projects: projectsReducer,
  experience: experienceReducer,
  contacts: contactsReducer,
  users: usersReducer
}

const rootReducer = Redux.combineReducers<IAppStore>(reducer);

export { rootReducer };
