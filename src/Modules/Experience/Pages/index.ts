import { i18n } from 'Core/Utils';

/*
 * Страницы модуля
 */
export enum EExperiencePages {
  LAYOUT = 20,
  EXPERIENCE_LIST_PAGE = 21,
  EXPERIENCE_EDIT_PAGE = 22,
  TECHNOLOGY_LIST_PAGE = 23,
  TECHNOLOGY_EDIT_PAGE = 24
}

export const experienceRoutes = {
  [EExperiencePages.LAYOUT]: {
    path: '/main/experience',
    name: i18n.t('Experience and technologies layout', 'Раздел опыта и технологий')
  },
  [EExperiencePages.EXPERIENCE_LIST_PAGE]: {
    path: '/main/experience',
    name: i18n.t('Experience', 'Опыт')
  },
  [EExperiencePages.EXPERIENCE_EDIT_PAGE]: {
    path: '/main/experience/edit',
    name: i18n.t('Experience editor', 'Редактировать опыт')
  },
  [EExperiencePages.TECHNOLOGY_LIST_PAGE]: {
    path: '/main/experience/technology',
    name: i18n.t('Technologies', 'Технологии')
  },
  [EExperiencePages.TECHNOLOGY_EDIT_PAGE]: {
    path: '/main/experience/technology/edit',
    name: i18n.t('Technology editor', 'Редактировать технологии')
  }
};

import { ExperienceListPage } from 'Modules/Experience/Pages/ExperienceListPage';
import { ExperienceEditPage } from 'Modules/Experience/Pages/ExperienceEditPage';
import { TechnologyListPage } from 'Modules/Experience/Pages/TechnologyListPage';
import { TechnologyEditPage } from 'Modules/Experience/Pages/TechnologyEditPage';
import { ExperienceLayout } from 'Modules/Experience/Pages/ExperienceLayout';

/*
 * Компоненты страниц
 */
export {
  ExperienceLayout,
  ExperienceListPage,
  ExperienceEditPage,
  TechnologyListPage,
  TechnologyEditPage,
};
