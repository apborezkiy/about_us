import * as _ from 'lodash';

import { GenericService } from 'Core/Generic';

import { IProject, EPriorities } from 'Common/Models';
import { IProjectsPageService } from 'Modules/Projects/Services/IProjectsPageService';

/*
 * Restul сервис
 */
class ProjectsPageService extends GenericService implements IProjectsPageService {
  constructor () {
    super('/projects');
  }

  loadProjects (): Promise<IProject[]> {
    return this.get<IProject[]>('');
  }

  saveProject (project: IProject): Promise<IProject> {
    return this.put<IProject>(`/${project.id}`, project);
  }

  createProject (project: IProject): Promise<IProject> {
    return this.post<IProject>('', project);
  }

  deleteProject (project: IProject): Promise<string> {
    return this.del(`/${project.id}`);
  }

  setupMocks (agent) {
    // load projects
    agent.get(this.url, (params) => {
      this.logGet(params);

      let projects: IProject[] = [
        {
          id: '1',
          name: 'Sberbank business online',
          avatar: 'assets/avatar.jpg',
          avatarFileName: '1.png',
          description: 'Description. Description. Description. Description. Description. Description. Description.',
          priority: EPriorities.high,
          screenShoots: [
            { description: '1 screen shoot', image: 'assets/avatar.jpg', fileName: '1.png', id: '1' },
            { description: '2 screen shoot', image: 'assets/avatar.jpg', fileName: '2.png', id: '2' }
          ]
        },
        {
          id: '2',
          name: 'Project 2',
          avatar: 'assets/avatar.jpg',
          avatarFileName: '2.png',
          description: 'Description. Description. Description. Description. Description. Description. Description.',
          priority: EPriorities.low,
          screenShoots: [
            { description: '3 screen shoot', image: 'assets/avatar.jpg', fileName: '3.png', id: '3' },
            { description: '4 screen shoot', image: 'assets/avatar.jpg', fileName: '4.png', id: '4' }
          ]
        }
      ];

      return { data: projects };
    });

    // save experience
    agent.put(this.url + '/:id', (params) => {
      this.logPut(params);

      let body = params.body;

      let project: IProject = _.assign({}, body, {
        name: body.name
      });

      return { data: project };
    });

    // create experience
    agent.post(this.url, (params) => {
      this.logPut(params);

      let body = params.body;

      let project: IProject = _.assign({}, body, {
        id: (new Date()).getMilliseconds().toString(),
        name: body.name
      });

      return { data: project };
    });

    // delete experience
    agent.del(this.url + '/:id', (params) => {
      this.logDelete(params);

      return { data: params.params.id };
    });
  }
}

const projectsPageService = new ProjectsPageService();

export { projectsPageService, IProjectsPageService };
