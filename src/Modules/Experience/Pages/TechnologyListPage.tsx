import * as _ from 'lodash';
import * as React from 'react';
import * as Redux from 'redux';
import * as ReactRedux from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router';
import { Grid, Row, Col, Image, Media, Button } from 'react-bootstrap';

import { i18n } from 'Core/Utils';

import { IUser, ERoles, ETechnologyKind, ITechnology } from 'Common/Models';
import { getTechnologyKindName } from 'Common/Constants';
import { IAppStore } from 'Modules/store';
import { ITechnologyPageActions } from 'Modules/Experience/Redux/Actions/ITechnologyPageActions';
import { TechnologyPageActions } from 'Modules/Experience/Redux/Actions/TechnologyPageActions';
import { technologyPageService } from 'Modules/Experience/Services/TechnologyPageService';


/*
 * Передаваемые параметры
 */
interface IProps {
  technologies: ITechnology[];
  actions: ITechnologyPageActions;
  user: IUser;
}

/*
 * Cтраница технологий
 */
class CTechnologyListPage extends React.Component<RouteComponentProps<IProps> & IProps> {

  componentWillMount () {
    if (this.props.technologies.length === 0) {
      this.props.actions.loadTechnologies();
    }
  }

  handleCreate () {
    this.props.actions.editTechnology({} as ITechnology);
  }

  handleEdit (id: string) {
    this.props.actions.editTechnology(_.find(this.props.technologies, ((technology) => technology.id === id)));
  }

  handleDelete (id: string) {
    this.props.actions.deleteTechnology(_.find(this.props.technologies, ((technology) => technology.id === id)));
  }

  renderCreateButton () {
    if (this.props.user.role !== ERoles.guest) {
      return (
        <Button bsStyle="link" onClick={this.handleCreate.bind(this)}>
          {i18n.t('[create]', '[создать]')}
        </Button>
      )
    }
  }

  renderEditButton (id: string) {
    if (this.props.user.role !== ERoles.guest) {
      return (
        <Button bsStyle="link" onClick={this.handleEdit.bind(this, id)}>
          {i18n.t('[edit]', '[редактировать]')}
        </Button>
      )
    }
  }

  renderDeleteButton (id: string) {
    if (this.props.user.role !== ERoles.guest) {
      return (
        <Button bsStyle="link" onClick={this.handleDelete.bind(this, id)}>
          {i18n.t('[delete]', '[удалить]')}
        </Button>
      )
    }
  }

  renderTechnology (technology: ITechnology) {
    return (
      <Col className="technology-page__member-wrapper">
        <div className="technology-page__member">
          <Media>
            <Media.Body>
              <Media.Heading>{technology.name} {this.renderEditButton(technology.id)} {this.renderDeleteButton(technology.id)}</Media.Heading>
              <p>{getTechnologyKindName(technology.kind)}</p>
            </Media.Body>
          </Media>
        </div>
      </Col>
    )
  }

  renderTechnologies (technologies: ITechnology[]) {
    return (
      <Grid>
        {_.map(technologies, (technologies, i) => <Row key={i}>{this.renderTechnology(technologies)}</Row>)}
      </Grid>
    )
  }

  // отрисовка
  render() {
    let frontTechnologies = _.filter(this.props.technologies, (technology) => technology.kind === ETechnologyKind.FRONT);
    let backTechnologies = _.filter(this.props.technologies, (technology) => technology.kind === ETechnologyKind.BACK);

    return (
      <div className="technology-page">
        <div className="technology-page__title">
          {this.renderCreateButton()}
        </div>
        <div className="technology-page__body">
          {this.renderTechnologies(frontTechnologies)}
          {this.renderTechnologies(backTechnologies)}
        </div>
      </div>
    )
  }
}

/*
 * Контейнер
 */
const mapStateToProps = function (state: IAppStore): IProps {
  const { technologies } = state.experience.technologyPage;
  const { user } = state.main.loginPage;

  return { technologies, user } as IProps;
};

const mapDispatchToProps = function (dispatch: Redux.Dispatch<IAppStore>): IProps {
  const actions: ITechnologyPageActions = new TechnologyPageActions(technologyPageService, dispatch);

  return { actions } as IProps;
};

const TechnologyListPage = withRouter(ReactRedux.connect(mapStateToProps, mapDispatchToProps)(CTechnologyListPage) as React.ComponentClass<RouteComponentProps<IProps>>);

export { TechnologyListPage };
