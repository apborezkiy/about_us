import * as _ from 'lodash';
import * as React from 'react';
import * as Redux from 'redux';
import * as ReactRedux from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router';
import { Grid, Row, Col, Image, Media, Button, FormGroup, FormControl, ControlLabel, HelpBlock } from 'react-bootstrap';

import { i18n } from 'Core/Utils';
import { IValidationState, IValidation } from 'Core/Models/Validation';

import { ITeamMember, TTeamMemberKey, IUser, ERoles, IProject, IProjectHeader, ITechnology } from 'Common/Models';
import { Autocomplete } from 'Common/Components/Autocomplete';

type TProjectAutocomplete = new() => Autocomplete<IProject>;
const ProjectAutocomplete = Autocomplete as TProjectAutocomplete;

type TTechnologyAutocomplete = new() => Autocomplete<ITechnology>;
const TechnologyAutocomplete = Autocomplete as TTechnologyAutocomplete;

import { ECompositePages, EMainPages } from 'Modules/Main/Pages';
import { MainMenuActions } from 'Modules/Main/Redux/Actions/MainMenuActions';
import { IExperiencePageActions } from 'Modules/Experience/Redux/Actions/IExperiencePageActions';
import { ExperiencePageActions } from 'Modules/Experience/Redux/Actions/ExperiencePageActions';
import { ITechnologyPageActions } from 'Modules/Experience/Redux/Actions/ITechnologyPageActions';
import { TechnologyPageActions } from 'Modules/Experience/Redux/Actions/TechnologyPageActions';
import { IProjectsPageActions } from 'Modules/Projects/Redux/Actions/IProjectsPageActions';
import { ProjectsPageActions } from 'Modules/Projects/Redux/Actions/ProjectsPageActions';
import { projectsPageService } from 'Modules/Projects/Services/ProjectsPageService';
import { experiencePageService } from 'Modules/Experience/Services/ExperiencePageService';
import { technologyPageService } from 'Modules/Experience/Services/TechnologyPageService';
import { IAppStore } from 'Modules/store';
import {EExperiencePages} from "./index";

/*
 * Передаваемые параметры
 */
interface IProps {
  members: ITeamMember[]; // members list
  member: ITeamMember; // member to edit
  actions: IExperiencePageActions; // CRUD actions
  projectsActions: IProjectsPageActions; // reload projects
  technologiesActions: ITechnologyPageActions; // reload technologies
  projects: IProject[]; // all available projects
  technologies: ITechnology[]; // all available technologies
  user: IUser; // current user
  redirectToExperiencePage: () => void;
}

/*
 * Внутреннее состояние
 */
interface IState {
  member: ITeamMember;
  validation: IValidation<ITeamMember>;
  isSended: boolean;
}

/*
 * Cтраница опыта и технологий
 */
class CExperienceEditPage extends React.Component<RouteComponentProps<IProps> & IProps, IState> {

  state: IState = {
    member: {
      projects: [],
      technologies: [] as ITechnology[],
      user: {}
    } as ITeamMember,
    validation: {
      position: { pattern: /.+/i, required: true } as IValidationState,
      experience: { pattern: /.+/i, required: true } as IValidationState
    },
    isSended: false
  };

  componentWillMount () {
    this.setState({ member: _.assign({}, this.state.member, this.props.member) });

    if (this.props.member.user) {
      if (this.props.members.length === 0) {
        this.props.actions.loadExperience();
      }

      if (this.props.projects.length === 0) {
        this.props.projectsActions.loadProjects();
      }

      if (this.props.technologies.length === 0) {
        this.props.technologiesActions.loadTechnologies();
      }
    } else {
      this.props.redirectToExperiencePage();
    }
  }

  handleSubmit = () => {
    let formValid = true;
    for (let field of _.keys(this.state.validation) as TTeamMemberKey[]) {
      formValid = formValid && this.handleInput(field);
    }

    if (formValid) {
      let member: ITeamMember = _.assign({}, this.props.member, this.state.member);
      this.props.actions.saveExperience(member)
        .then(() => this.setState({ isSended: true }));
    }
  };

  handleInput = (key: TTeamMemberKey, e?) => {
    let member = _.assign({}, this.state.member);
    member[key] = e ? e.target.value : member[key];
    let validation = _.assign({}, this.state.validation);
    let validationFiled = validation[key];
    let isValid = true;

    if (validationFiled.required && (!member[key] || member[key].length === 0)) {
      isValid = false;
      validationFiled.state = 'warning';
      validationFiled.text = i18n.t('Required field', 'Обязательное поле');
    } else {
      isValid = validationFiled.pattern.test(member[key]);
      validationFiled.state = isValid ? 'success' : 'error';
      validationFiled.text = isValid ? '' : i18n.t('Incorrect field', 'Некорректное поле');
    }

    this.setState({member, validation});

    return isValid;
  };

  handleAddProject = (project: IProject) => {
    let member = _.assign({}, this.state.member);

    member.projects.push({ name: project.name, id: project.id } as IProjectHeader);

    this.setState({ member });
  }

  handleAddTechnology = (technology: ITechnology) => {
    let member = _.assign({}, this.state.member);

    member.technologies.push(technology);

    this.setState({ member });
  }

  //
  // experience
  //
  renderPositionInput () {
    return (
      <FormGroup className="experience-page__form-item"
                 controlId="formHorizontalPosition"
                 validationState={this.state.validation.position.state}>
        <Col componentClass={ControlLabel}>
          {i18n.t('Position', 'Позиция')}
        </Col>
        <Col>
          <FormControl className="experience-page__form-item-position"
                       type="text"
                       defaultValue={this.props.member.position}
                       onInput={e => this.handleInput('position', e)} />
          <HelpBlock>{this.state.validation.position.text}</HelpBlock>
        </Col>
      </FormGroup>
    );
  }

  renderExperienceInput () {
    return (
      <FormGroup className="experience-page__form-item"
                 controlId="formHorizontalExperience"
                 validationState={this.state.validation.experience.state}>
        <Col componentClass={ControlLabel}>
          {i18n.t('Experience', 'Опыт')}
        </Col>
        <Col>
          <FormControl className="experience-page__form-item-experience"
                       type="text"
                       defaultValue={this.props.member.experience}
                       onInput={e => this.handleInput('experience', e)} />
          <HelpBlock>{this.state.validation.experience.text}</HelpBlock>
        </Col>
      </FormGroup>
    );
  }

  renderUserDetails () {
    return (
      <FormGroup className="experience-page__form-item"
                 controlId="formHorizontalUser">
        <Col componentClass={ControlLabel}>
          {i18n.t('Holder', 'Держатель')}
        </Col>
        <Col>
          <FormControl className="experience-page__form-item-user"
                       type="text"
                       readOnly={true}
                       defaultValue={this.state.member.user.name} />
        </Col>
      </FormGroup>
    )
  }

  //
  // projects
  //
  renderProjectsPicker () {
    let projects = _.filter(this.props.projects, (e1) => !_.find(this.state.member.projects, (e2) => e1.id === e2.id));

    return (
      <ProjectAutocomplete list={projects}
                           placeHolder={i18n.t('Add project', 'Добавить проект')}
                           onSelect={this.handleAddProject}
                           displayFn={(e: IProject) => `${e.id} - ${e.name}`}
                           searchFn={(e: IProject, text: string) => _.includes(e.name, text)}
                           refName="projectsAutocomplete"
      />
    );
  }

  renderProjectsListElement (e: IProjectHeader, i: number) {
    return (
      <li key={i}>
        <span>{e.name}</span>
        <span>{this.renderProjectDeleteButton(e)}</span>
      </li>
    )
  }

  renderProjectsList () {
    if (this.state.member.projects.length === 0) {
      return null;
    }

    return (
      <FormGroup className="experience-page__form-item"
                 controlId="formHorizontalProjects">
        <Col componentClass={ControlLabel}>
          {i18n.t('Projects', 'Проекты')}
        </Col>
        <ul>
          {_.map(this.state.member.projects, (e: IProjectHeader, i: number) => this.renderProjectsListElement(e, i))}
        </ul>
      </FormGroup>
    )
  }

  renderProjectDeleteButton (e1: IProjectHeader) {
    const handleDelete = () => {
      let member = _.assign({}, this.state.member);

      _.remove(member.projects, (e2: IProjectHeader) => e1 === e2);

      this.setState({ member })
    }

    return (
      <Button bsStyle="link" onClick={handleDelete}>
        {i18n.t('[delete]', '[удалить]')}
      </Button>
    )
  }

  //
  // technologies
  //
  renderTechnologiesPicker () {
    let technologies = _.filter(this.props.technologies, (e1) => !_.find(this.state.member.technologies, (e2) => e1.id === e2.id));

    return (
      <TechnologyAutocomplete list={technologies}
                           placeHolder={i18n.t('Add technology', 'Добавить технологию')}
                           onSelect={this.handleAddTechnology}
                           displayFn={(e: ITechnology) => `${e.id} - ${e.name}`}
                           searchFn={(e: ITechnology, text: string) => _.includes(e.name, text)}
                           refName="technologiesAutocomplete"
      />
    );
  }

  renderTechnologiesListElement (e: ITechnology, i: number) {
    return (
      <li key={i}>
        <span>{e.name}</span>
        <span>{this.renderTechnologyDeleteButton(e)}</span>
      </li>
    )
  }

  renderTechnologiesList () {
    if (this.state.member.technologies.length === 0) {
      return null;
    }

    return (
      <FormGroup className="experience-page__form-item"
                 controlId="formHorizontalTechnologies">
        <Col componentClass={ControlLabel}>
          {i18n.t('Technologies', 'Проекты')}
        </Col>
        <ul>
          {_.map(this.state.member.technologies, (e: ITechnology, i: number) => this.renderTechnologiesListElement(e, i))}
        </ul>
      </FormGroup>
    )
  }

  renderTechnologyDeleteButton (e1: ITechnology) {
    const handleDelete = () => {
      let member = _.assign({}, this.state.member);

      _.remove(member.technologies, (e2: ITechnology) => e1 === e2);

      this.setState({ member })
    }

    return (
      <Button bsStyle="link" onClick={handleDelete}>
        {i18n.t('[delete]', '[удалить]')}
      </Button>
    )
  }

  //
  // form
  //
  renderSubmitButton () {
    if (this.props.user.role !== ERoles.guest) {
      return (
        <FormGroup className="experience-page__form-item">
          <Col>
            <Button type="submit" onClick={this.handleSubmit}>
              {i18n.t('Submit', 'Отправить')}
            </Button>
          </Col>
        </FormGroup>
      );
    }
  }

  renderTitle () {
    return (
      <FormGroup className="experience-page__form-item">
        <Col componentClass={ControlLabel}>
          {i18n.t('Experience of the member', 'Опыт участника команды')}
        </Col>
      </FormGroup>
    );
  }

  renderContactsForm () {
    if (!this.state.isSended) {
      return (
        <form className="experience-page__form" autoComplete="off">
          {this.renderTitle()}
          {this.renderUserDetails()}
          {this.renderPositionInput()}
          {this.renderExperienceInput()}
          {this.renderProjectsPicker()}
          {this.renderProjectsList()}
          {this.renderTechnologiesPicker()}
          {this.renderTechnologiesList()}
          {this.renderSubmitButton()}
        </form>
      );
    } else {
      return (
        <form className="experience-page__form" autoComplete="off">
          <div className="experience-page__form-notification">
            {i18n.t('Data sended successfully!', 'Данные успешно отправлены!')}
          </div>
        </form>
      )
    }
  }

  // отрисовка
  render () {
    return (
      <div className="experience-page">
        <div className="experience-page__title">

        </div>
        <div className="experience-page__body">
          {this.renderContactsForm()}
        </div>
      </div>
    );
  }
}

/*
 * Контейнер
 */
const mapStateToProps = function (state: IAppStore): IProps {
  const { member, members } = state.experience.experiencePage;
  const { projects } = state.projects.projectsPage;
  const { technologies } = state.experience.technologyPage;
  const { user } = state.main.loginPage;

  return { members, member, user, projects, technologies } as IProps;
};

const mapDispatchToProps = function (dispatch: Redux.Dispatch<IAppStore>): IProps {
  const actions: IExperiencePageActions = new ExperiencePageActions(experiencePageService, dispatch);
  const projectsActions: IProjectsPageActions = new ProjectsPageActions(projectsPageService, dispatch);
  const technologiesActions: ITechnologyPageActions = new TechnologyPageActions(technologyPageService, dispatch);
  const redirectToExperiencePage = () => new MainMenuActions(null, dispatch).navigateMainMenu(EExperiencePages.EXPERIENCE_LIST_PAGE as ECompositePages);

  return { actions, projectsActions, technologiesActions, redirectToExperiencePage } as IProps;
};

const ExperienceEditPage = withRouter(ReactRedux.connect(mapStateToProps, mapDispatchToProps)(CExperienceEditPage) as React.ComponentClass<RouteComponentProps<IProps>>);

export { ExperienceEditPage };
