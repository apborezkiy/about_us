import { push } from 'connected-react-router';

import { GenericActions } from 'Core/Generic';
import { ActionUtils } from 'Core/Redux';
import { pageToRouteInfo } from 'Core/Utils';

import { IProject } from 'Common/Models';
import { IProjectsPageActions, projectsPageActions } from 'Modules/Projects/Redux/Actions/IProjectsPageActions';
import { IProjectsPageService } from 'Modules/Projects/Services/IProjectsPageService';

import { EProjectsPages, projectsRoutes } from '../../Pages';

const routeList: string = pageToRouteInfo<EProjectsPages>(EProjectsPages.PROJECTS_LIST_PAGE, projectsRoutes).path;
const routeEdit: string = pageToRouteInfo<EProjectsPages>(EProjectsPages.PROJECTS_EDIT_PAGE, projectsRoutes).path;
const routeView: string = pageToRouteInfo<EProjectsPages>(EProjectsPages.PROJECTS_VIEW_PAGE, projectsRoutes).path;

/*
 * Действия страницы контактов
 */
class ProjectsPageActions extends GenericActions implements IProjectsPageActions {
  constructor (private api: IProjectsPageService, dispatch) {
    super(dispatch);
  }

  // Загрузить список
  loadProjects (): Promise<IProject[]> {
    const loadProjects = () => this.api.loadProjects().catch(this.redirectError) as Promise<IProject[]>;
    return ActionUtils.dispatchAsyncResponseBound<IProject[]>(this.dispatch, projectsPageActions.LOAD_PROJECTS, loadProjects);
  }

  // Открыть страницу порсмотра имеющегося или нового элемента
  viewProject (project: IProject) {
    async function navigate () {
      return await new Promise<IProject>(resolve => {
        setTimeout(() => this.dispatch(push(routeView)), 100);
        resolve(project);
      });
    }

    ActionUtils.dispatchAsyncResponseBound<void>(this.dispatch, projectsPageActions.VIEW_PROJECT, navigate.bind(this));
  }

  // Открыть страницу редактирования имеющегося или нового элемента
  editProject (project: IProject) {
    async function navigate () {
      return await new Promise<IProject>(resolve => {
        setTimeout(() => this.dispatch(push(routeEdit)), 100);
        resolve(project);
      });
    }

    ActionUtils.dispatchAsyncResponseBound<void>(this.dispatch, projectsPageActions.EDIT_PROJECT, navigate.bind(this));
  }

  // Сохранить изменения имеющегося или нового элемента
  saveProject (project: IProject): Promise<IProject> {
    const saveProjects = () => {
      if (project.id !== undefined) {
        return this.api.saveProject(project)
          .then(data => { setTimeout(() => this.dispatch(push(routeList)), 100); return data })
          .catch(this.redirectError) as Promise<IProject>;
      } else {
        return this.api.createProject(project)
          .then(data => { setTimeout(() => this.dispatch(push(routeList)), 100); return data })
          .catch(this.redirectError) as Promise<IProject>;
      }
    }

    return ActionUtils.dispatchAsyncResponseBound<IProject>(this.dispatch, projectsPageActions.SAVE_PROJECT, saveProjects);
  }

  // Удалить элемент
  deleteProject (project: IProject): Promise<string> {
    const deleteProjects = () => {
      return this.api.deleteProject(project).catch(this.redirectError) as Promise<string>;
    }

    return ActionUtils.dispatchAsyncResponseBound<string>(this.dispatch, projectsPageActions.DELETE_PROJECT, deleteProjects);
  }
}

export { ProjectsPageActions };
