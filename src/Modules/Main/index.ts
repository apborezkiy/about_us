import { IMainStore } from 'Modules/Main/Redux/Store';
import { mainInitialStore, mainReducer } from 'Modules/Main/Redux/Reducers';
import { MainLayout } from 'Modules/Main/Pages/MainLayout';
import { mainRoutes, EMainPages } from 'Modules/Main/Pages';

export {
  MainLayout,
  IMainStore,
  mainInitialStore,
  mainReducer,
  EMainPages,
  mainRoutes
};
