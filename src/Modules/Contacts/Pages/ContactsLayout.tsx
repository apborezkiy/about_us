import * as React from 'react';
import * as Redux from 'redux';
import * as ReactRedux from 'react-redux';
import { Route, RouteComponentProps, withRouter } from 'react-router';
import { Button } from 'react-bootstrap';

import { pageToRouteInfo } from 'Core/Utils';

import { IAppStore } from 'Modules/store';
import { ContactsPage } from 'Modules/Contacts/Pages/ContactsPage';
import { contactsRoutes, EContactsPages } from 'Modules/Contacts/Pages';

const contactsPage = pageToRouteInfo<EContactsPages>(EContactsPages.CONTACTS_PAGE, contactsRoutes);

/*
 * Передаваемые параметры
 */
interface IProps {

}

/*
 * Каркас модуля
 */
class CContactsLayout extends React.Component<RouteComponentProps<IProps> & IProps> {

  // отрисовка
  render () {
    return (
      <div className="contacts-layout">
        <div className="contacts-layout__header">

        </div>
        <div className="contacts-layout__body">
          <Route exact path={contactsPage.path} component={ContactsPage} />

        </div>
      </div>
    )
  }
}

/*
 * Контейнер
 */
const mapStateToProps = function (state: IAppStore): IProps {
  return {} as IProps;
}

const mapDispatchToProps = function (dispatch: Redux.Dispatch<IAppStore>): IProps {
  return {} as IProps;
}

const ContactsLayout = withRouter(ReactRedux.connect(mapStateToProps, mapDispatchToProps)(CContactsLayout) as React.ComponentClass<RouteComponentProps<IProps>>);

export { ContactsLayout };
