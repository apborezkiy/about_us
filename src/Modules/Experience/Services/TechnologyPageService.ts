import * as _ from 'lodash';

import { GenericService } from 'Core/Generic';

import { ITechnology, ETechnologyKind } from 'Common/Models';
import { ITechnologyPageService } from 'Modules/Experience/Services/ITechnologyPageService';

/*
 * Restul сервис
 */
class TechnologyPageService extends GenericService implements ITechnologyPageService {
  constructor () {
    super('/technologies');
  }

  loadTechnologies (): Promise<ITechnology[]> {
    return this.get<ITechnology[]>();
  }

  saveTechnology (technology: ITechnology): Promise<ITechnology> {
    return this.put<ITechnology>(`/${technology.id}`, technology);
  }

  createTechnology (technology: ITechnology): Promise<ITechnology> {
    return this.post<ITechnology>('', technology);
  }

  deleteTechnology (technology: ITechnology): Promise<string> {
    return this.del(`/${technology.id}`);
  }

  setupMocks (agent) {
    // load experience
    agent.get(this.url, (params) => {
      this.logGet(params);

      let technologies: ITechnology[] = [
        {
          id: '1',
          name: 'React',
          kind: ETechnologyKind.FRONT
        },
        {
          id: '2',
          name: 'React Native',
          kind: ETechnologyKind.FRONT
        },
        {
          id: '3',
          name: 'Node js',
          kind: ETechnologyKind.BACK
        }
      ];

      return { data: technologies };
    });

    // save experience
    agent.put(this.url + '/:id', (params) => {
      this.logPut(params);

      let body = params.body;

      let technology: ITechnology = _.assign({}, body, {
        name: body.name
      });

      return { data: technology };
    });

    // create experience
    agent.post(this.url, (params) => {
      this.logPut(params);

      let body = params.body;

      let technology: ITechnology = _.assign({}, body, {
        id: (new Date()).getMilliseconds().toString(),
        name: body.name
      });

      return { data: technology };
    });

    // delete experience
    agent.del(this.url + '/:id', (params) => {
      this.logDelete(params);

      return { data: params.params.id };
    });
  }
}

const technologyPageService = new TechnologyPageService();

export { technologyPageService, ITechnologyPageService };
