import { ECompositePages } from 'Modules/Main/Pages';

/*
 * Actions
 */
export const mainMenuActions = {
  NAVIGATE_MAIN_MENU: 'NAVIGATE_MAIN_MENU'
};

export interface IMainMenuActions {
  navigateMainMenu: (page: ECompositePages) => void;
}
