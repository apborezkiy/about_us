import { IUser } from 'Common/Models';

/*
 * API
 */
export interface IUsersPageService {
  loadUsers: () => Promise<IUser[]>;
  createUser: (user: IUser) => Promise<IUser>;
  deleteUser: (user: IUser) => Promise<string>;
  saveUser: (user: IUser) => Promise<IUser>
}
