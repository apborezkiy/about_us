import { ITeamMember } from 'Common/Models';

/*
 * Actions
 */
export const experiencePageActions = {
  LOAD_EXPERIENCE: 'LOAD_EXPERIENCE',
  EDIT_EXPERIENCE: 'EDIT_EXPERIENCE',
  SAVE_EXPERIENCE: 'SAVE_EXPERIENCE',
  DELETE_EXPERIENCE: 'DELETE_EXPERIENCE'
};

export interface IExperiencePageActions {
  loadExperience: () => Promise<ITeamMember[]>;
  editExperience: (member: ITeamMember) => void;
  saveExperience: (member: ITeamMember) => Promise<ITeamMember>;
  deleteExperience: (member: ITeamMember) => Promise<string>;
}
