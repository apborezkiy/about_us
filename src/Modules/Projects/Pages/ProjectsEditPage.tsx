import * as _ from 'lodash';
import * as React from 'react';
import * as Redux from 'redux';
import * as ReactRedux from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router';
import { Grid, Row, Col, Image, Media, Button } from 'react-bootstrap';
import { FormGroup, FormControl, ControlLabel, HelpBlock } from 'react-bootstrap';

import { i18n } from 'Core/Utils';
import { IValidationState, IValidation } from 'Core/Models/Validation';

import { TProjectKey, IUser, IProject, ERoles, EPriorities, IScreenShoot, IResource } from 'Common/Models';
import { getRoleName, getPriorityName } from 'Common/Constants';
import { RadioGroup, IRadioGroupSelectOption } from 'Common/Components/RadioGroup';
import { Autocomplete } from 'Common/Components/Autocomplete';

import { IProjectsPageActions } from 'Modules/Projects/Redux/Actions/IProjectsPageActions';
import { ProjectsPageActions } from 'Modules/Projects/Redux/Actions/ProjectsPageActions';
import { projectsPageService } from 'Modules/Projects/Services/ProjectsPageService';
import { IScreenShootsPageActions } from 'Modules/Projects/Redux/Actions/IScreenShootsPageActions';
import { ScreenShootsPageActions } from 'Modules/Projects/Redux/Actions/ScreenShootsPageActions';
import { screenShootsPageService } from '../Services/SnapShootsPageService';
import { IAppStore } from 'Modules/store';

type TProjectScreenShootAutocomplete = new() => Autocomplete<IScreenShoot>;
const ProjectScreenShootAutocomplete = Autocomplete as TProjectScreenShootAutocomplete;

type TImagesAutocomplete = new() => Autocomplete<IResource>;
const ImagesAutocomplete = Autocomplete as TImagesAutocomplete;

type TPriorityRadioGroup = new() => RadioGroup<EPriorities>;
const PriorityRadioGroup = RadioGroup as TPriorityRadioGroup;

/*
 * Передаваемые параметры
 */
interface IProps {
  projects: IProject[]; // projects list
  project: IProject; // project to edit
  screenShoots: IScreenShoot[]; // all screen shoots available
  images: IResource[];
  actions: IProjectsPageActions; // CRUD actions
  screenShootsActions: IScreenShootsPageActions;
  user: IUser; // current user
}

/*
 * Внутреннее состояние
 */
interface IState {
  project: IProject;
  validation: IValidation<IProject>;
  isSended: boolean;
}

/*
 * Отображения для RadioGroup
 */
function getRoleOptions (): IRadioGroupSelectOption<ERoles>[] {
  return _.map([ERoles.admin, ERoles.moderator, ERoles.member], (value: ERoles, index: number) => {
    return {
      id: index,
      label: getRoleName(value),
      value: value
    };
  });
}

function getRoleOption (value: ERoles): IRadioGroupSelectOption<ERoles> {
  return _.find(getRoleOptions(), (selector: IRadioGroupSelectOption<ERoles>) => selector.value === value);
}

function getPriorityOptions (): IRadioGroupSelectOption<EPriorities>[] {
  return _.map([EPriorities.low, EPriorities.medium, EPriorities.high, EPriorities.highest], (value: EPriorities, index: number) => {
    return {
      id: index,
      label: getPriorityName(value),
      value: value
    };
  });
}

function getPriorityOption (value: EPriorities): IRadioGroupSelectOption<EPriorities> {
  return _.find(getPriorityOptions(), (selector: IRadioGroupSelectOption<EPriorities>) => selector.value === value);
}

/*
 * Cтраница редактирования проекта.
 */
class CProjectsEditPage extends React.Component<RouteComponentProps<IProps> & IProps, IState> {

  state: IState = {
    project: { screenShoots: [] } as IProject,
    validation: {
      name: { pattern: /.+/i, required: true } as IValidationState,
      description: { pattern: /.+/i, required: true } as IValidationState,
      priority: { pattern: /.+/i, required: true } as IValidationState
    },
    isSended: false
  };

  componentWillMount () {
    this.setState({ project: _.assign({}, this.state.project, this.props.project) });

    if (this.props.projects.length === 0) {
      this.props.actions.loadProjects();
    }

    if (this.props.screenShoots.length === 0) {
      this.props.screenShootsActions.loadScreenShoots();
    }

    if (this.props.images.length === 0) {
      this.props.screenShootsActions.loadScreenShootsAvailableImages();
    }
  }

  handleSubmit = () => {
    let formValid = true;
    for (let field of _.keys(this.state.validation) as TProjectKey[]) {
      formValid = formValid && this.handleInput(field);
    }

    if (formValid) {
      let project: IProject = _.assign({}, this.props.project, this.state.project);
      this.props.actions.saveProject(project)
        .then(() => this.setState({ isSended: true }));
    }
  };

  handleInput = (key: TProjectKey, e?) => {
    let project = _.assign({}, this.state.project);
    project[key] = e ? e.target.value : project[key];
    let validation = _.assign({}, this.state.validation);
    let validationFiled = validation[key];
    let isValid = true;

    if (validationFiled.required && (!project[key] || project[key].length === 0)) {
      isValid = false;
      validationFiled.state = 'warning';
      validationFiled.text = i18n.t('Required field', 'Обязательное поле');
    } else {
      isValid = validationFiled.pattern.test(project[key]);
      validationFiled.state = isValid ? 'success' : 'error';
      validationFiled.text = isValid ? '' : i18n.t('Incorrect field', 'Некорректное поле');
    }

    this.setState({ project, validation });

    return isValid;
  };

  handleAddScreenShoot = (screenShoot: IScreenShoot) => {
    let project = _.assign({}, this.state.project);

    project.screenShoots.push(screenShoot);

    this.setState({ project });
  }

  handleSelectImage = (r: IResource) => {
    let project = _.assign({}, this.state.project);

    project.avatar = r.url;
    project.avatarFileName = r.name;

    this.setState({ project })
  }

  renderNameInput () {
    return (
      <FormGroup className="projects-page__form-item"
        controlId="formHorizontalName"
        validationState={this.state.validation.name.state}>
        <Col componentClass={ControlLabel}>
          {i18n.t('Name', 'Название')}
        </Col>
        <Col>
          <FormControl className="projects-page__form-item-name"
            type="text"
            defaultValue={this.props.project.name}
            onInput={e => this.handleInput('name', e)}/>
          <HelpBlock>{this.state.validation.name.text}</HelpBlock>
        </Col>
      </FormGroup>
    );
  }

  renderDescriptionInput () {
    return (
      <FormGroup className="projects-page__form-item"
                 controlId="formHorizontalDescription"
                 validationState={this.state.validation.description.state}>
        <Col componentClass={ControlLabel}>
          {i18n.t('Description', 'Описание')}
        </Col>
        <Col>
          <FormControl className="projects-page__form-item-description"
                       type="text"
                       defaultValue={this.props.project.description}
                       onInput={e => this.handleInput('description', e)}/>
          <HelpBlock>{this.state.validation.description.text}</HelpBlock>
        </Col>
      </FormGroup>
    );
  }

  renderPriorityInput () {
    return (
      <FormGroup className="projects-page__form-item"
                 controlId="formHorizontalPriority"
                 validationState={this.state.validation.priority.state}>
        <Col componentClass={ControlLabel}>
          {i18n.t('Priority', 'Приоритет')}
        </Col>
        <PriorityRadioGroup
          option={getPriorityOption(this.state.project.priority)}
          options={getPriorityOptions()}
          callback={e => this.handleInput('priority', e)}
        />
      </FormGroup>
    );
  }

  //
  // screen shoots
  //
  renderScreenShootsPicker () {
    let screenShoots = _.filter(this.props.screenShoots, (e1) => !_.find(this.state.project.screenShoots, (e2) => e1.id === e2.id));
console.log(screenShoots)
    return (
      <ProjectScreenShootAutocomplete list={screenShoots}
                              placeHolder={i18n.t('Add screen shoot', 'Добавить скриншот')}
                              onSelect={this.handleAddScreenShoot}
                              displayFn={(e: IScreenShoot) => `${e.id} - ${e.fileName}`}
                              searchFn={(e: IScreenShoot, text: string) => _.includes(e.fileName, text)}
                              refName="screenShootsAutocomplete"
      />
    );
  }

  renderScreenShootsListElement (e: IScreenShoot, i: number) {
    return (
      <li key={i}>
        <span>{e.description}</span>
        <span>{this.renderScreenShootDeleteButton(e)}</span>
      </li>
    )
  }

  renderScreenShootsList () {
    if (this.state.project.screenShoots.length === 0) {
      return null;
    }

    return (
      <FormGroup className="experience-page__form-item"
                 controlId="formHorizontalScreenShoots">
        <Col componentClass={ControlLabel}>
          {i18n.t('ScreenShoots', 'Проекты')}
        </Col>
        <ul>
          {_.map(this.state.project.screenShoots, (e: IScreenShoot, i: number) => this.renderScreenShootsListElement(e, i))}
        </ul>
      </FormGroup>
    )
  }

  renderScreenShootDeleteButton (e1: IScreenShoot) {
    const handleDelete = () => {
      let project = _.assign({}, this.state.project);

      _.remove(project.screenShoots, (e2: IScreenShoot) => e1.id === e2.id);

      this.setState({ project })
    }

    return (
      <Button bsStyle="link" onClick={handleDelete}>
        {i18n.t('[delete]', '[удалить]')}
      </Button>
    )
  }

  //
  // images
  //
  renderImagePicker () {
    let images = _.filter(this.props.images, (image) => this.state.project.avatarFileName !== image.name);

    return (
      <ImagesAutocomplete list={images}
                          placeHolder={i18n.t('Select avatar', 'Выбрать аватар')}
                          onSelect={this.handleSelectImage}
                          displayFn={(e: IResource) => `${e.id} - ${e.url}`}
                          searchFn={(e: IResource, text: string) => _.includes(e.name, text)}
                          refName="avatarsAutocomplete"
      />
    );
  }

  renderSelectedImage () {
    if (!this.state.project.avatarFileName) {
      return null;
    }

    return (
      <FormGroup className="experience-page__form-item"
                 controlId="formHorizontalScreenShoots">
        <Col componentClass={ControlLabel}>
          {i18n.t('Avatar: ', 'Выбранный аватар: ')}
          <span>{this.state.project.avatarFileName}</span>
          <span>{this.renderImageDeleteButton()}</span>
        </Col>
      </FormGroup>
    )
  }

  renderImageDeleteButton () {
    const handleDelete = () => {
      let project = _.assign({}, this.state.project);

      project.avatar = null;
      project.avatarFileName = null;

      this.setState({ project })
    }

    return (
      <Button bsStyle="link" onClick={handleDelete}>
        {i18n.t('[delete]', '[удалить]')}
      </Button>
    )
  }

  renderSubmitButton () {
    if (this.props.user.role !== ERoles.guest) {
      return (
        <FormGroup className="projects-page__form-item">
          <Col>
            <Button type="submit" onClick={this.handleSubmit}>
              {i18n.t('Submit', 'Отправить')}
            </Button>
          </Col>
        </FormGroup>
      );
    }
  }

  //
  // form
  //
  renderTitle () {
    return (
      <FormGroup className="projects-page__form-item">
        <Col componentClass={ControlLabel}>
          {i18n.t('Project', 'Проект')}
        </Col>
      </FormGroup>
    );
  }

  renderForm () {
    if (!this.state.isSended) {
      return (
        <form className="projects-page__form" autoComplete="off">
          {this.renderTitle()}
          {this.renderNameInput()}
          {this.renderDescriptionInput()}
          {this.renderPriorityInput()}
          {this.renderImagePicker()}
          {this.renderSelectedImage()}
          {this.renderScreenShootsPicker()}
          {this.renderScreenShootsList()}
          {this.renderSubmitButton()}
        </form>
      );
    } else {
      return (
        <form className="projects-page__form" autoComplete="off">
          <div className="projects-page__form-notification">
            {i18n.t('Data sended successfully!', 'Данные успешно отправлены!')}
          </div>
        </form>
      );
    }
  }

  // отрисовка
  render () {
    return (
      <div className="projects-page">
        <div className="projects-page__title">

        </div>
        <div className="projects-page__body">
          {this.renderForm()}
        </div>
      </div>
    );
  }
}

/*
 * Контейнер
 */
const mapStateToProps = function (state: IAppStore): IProps {
  const { project, projects } = state.projects.projectsPage;
  const { screenShoots, images } = state.projects.screenShootsPage;
  const { user } = state.main.loginPage;

  return { project, projects, user, screenShoots, images } as IProps;
};

const mapDispatchToProps = function (dispatch: Redux.Dispatch<IAppStore>): IProps {
  const actions: IProjectsPageActions = new ProjectsPageActions(projectsPageService, dispatch);
  const screenShootsActions: IScreenShootsPageActions = new ScreenShootsPageActions(screenShootsPageService, dispatch);

  return { actions, screenShootsActions } as IProps;
};

const ProjectsEditPage = withRouter(ReactRedux.connect(mapStateToProps, mapDispatchToProps)(CProjectsEditPage) as React.ComponentClass<RouteComponentProps<IProps>>);

export { ProjectsEditPage };
