import { i18n } from 'Core/Utils'

export enum ESystemPages {
  ROOT = 1,
  NOT_FOUND = 2,
  ERROR = 3,
}

export interface RouteInfo {
  path: string;
  name: string;
}

export interface RouteInfoMap {
  [key: string]: RouteInfo;
}

export const systemRoutes: RouteInfoMap = {
  [ESystemPages.ROOT]: { path: '/', name: null },
  [ESystemPages.NOT_FOUND]: { path: '/not-found', name: i18n.t('Page Not Found', 'Страница не найдена') },
  [ESystemPages.ERROR]: { path: '/error', name: i18n.t('Error', 'Ошибка') },
}
