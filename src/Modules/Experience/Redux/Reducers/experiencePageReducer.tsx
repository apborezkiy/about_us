import * as _ from 'lodash';
import * as Redux from 'redux';
import * as ReduxActions from 'redux-actions';

import { SUCCESS } from 'Core/Models/Actions';
import { ITeamMember } from 'Common/Models';
import { IExperiencePageStore } from 'Modules/Experience/Redux/Store';

import { experiencePageActions } from '../Actions/IExperiencePageActions';

/*
 * Начальное состояние страницы
 */
export const experiencePageInitialStore: IExperiencePageStore = {
  members: [],
  member: {} as ITeamMember
}

/*
 * Редьюсер страницы
 */
export const experiencePageReducer: Redux.Reducer<IExperiencePageStore> = ReduxActions.handleActions({
  [`${experiencePageActions.LOAD_EXPERIENCE}_${SUCCESS}`]: (state: IExperiencePageStore, action) => {
    return _.assign({}, state, { members: action.payload });
  },
  [`${experiencePageActions.EDIT_EXPERIENCE}_${SUCCESS}`]: (state: IExperiencePageStore, action: ReduxActions.Action<ITeamMember>) => {
    return _.assign({}, state, { member: action.payload });
  },
  [`${experiencePageActions.SAVE_EXPERIENCE}_${SUCCESS}`]: (state: IExperiencePageStore, action: ReduxActions.Action<ITeamMember>) => {
    let newMember = action.payload;

    let members = _.filter(state.members, (member) => member.id !== newMember.id) as ITeamMember[];
    let newMembers = _.sortBy([...members, newMember], (o: ITeamMember) => o.priority);

    return _.assign({}, state, { members: newMembers });
  },
  [`${experiencePageActions.DELETE_EXPERIENCE}_${SUCCESS}`]: (state: IExperiencePageStore, action: ReduxActions.Action<ITeamMember>) => {
    let memberId = action.payload;

    let members = _.filter(state.members, (member) => member.id !== memberId) as ITeamMember[];
    let newMembers = _.sortBy([...members], (o: ITeamMember) => o.priority);

    return _.assign({}, state, { members: newMembers });
  }
}, experiencePageInitialStore)
