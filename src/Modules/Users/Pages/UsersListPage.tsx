import * as _ from 'lodash';
import * as React from 'react';
import * as Redux from 'redux';
import * as ReactRedux from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router';
import { Grid, Row, Col, Image, Media, Button } from 'react-bootstrap';

import { i18n } from 'Core/Utils';

import { IUser, ERoles } from 'Common/Models';
import { getRoleName } from 'Common/Constants';

import { IUsersPageActions } from 'Modules/Users/Redux/Actions/IUsersPageActions';
import { UsersPageActions } from 'Modules/Users/Redux/Actions/UsersPageActions';
import { usersPageService } from 'Modules/Users/Services/UsersPageService';
import { IAppStore } from 'Modules/store';


/*
 * Передаваемые параметры
 */
interface IProps {
  users: IUser[];
  actions: IUsersPageActions;
  user: IUser;
}

/*
 * Cтраница пользователей
 */
class CUsersListPage extends React.Component<RouteComponentProps<IProps> & IProps> {

  componentWillMount () {
    if (this.props.users.length === 0) {
      this.props.actions.loadUsers();
    }
  }

  handleCreate () {
    this.props.actions.editUser({} as IUser);
  }

  handleEdit (id: string) {
    this.props.actions.editUser(_.find(this.props.users, ((user) => user.id === id)));
  }

  handleDelete (id: string) {
    this.props.actions.deleteUser(_.find(this.props.users, ((user) => user.id === id)));
  }

  renderCreateButton () {
    if (this.props.user.role !== ERoles.guest) {
      return (
        <Button bsStyle="link" onClick={this.handleCreate.bind(this)}>
          {i18n.t('[create]', '[создать]')}
        </Button>
      );
    }
  }

  renderEditButton (id: string) {
    if (this.props.user.role !== ERoles.guest) {
      return (
        <Button bsStyle="link" onClick={this.handleEdit.bind(this, id)}>
          {i18n.t('[edit]', '[редактировать]')}
        </Button>
      );
    }
  }

  renderDeleteButton (id: string) {
    if (this.props.user.role !== ERoles.guest) {
      return (
        <Button bsStyle="link" onClick={this.handleDelete.bind(this, id)}>
          {i18n.t('[delete]', '[удалить]')}
        </Button>
      );
    }
  }

  renderUser (user: IUser) {
    return (
      <Col className="users-page__user-wrapper">
        <div className="users-page__user">
          <Media>
            <Media.Left>
              <Image width={250} height={400} className="users-page__user-photo" src={user.image} rounded />
            </Media.Left>
            <Media.Body>
              <Media.Heading>{user.name} {this.renderEditButton(user.id)} {this.renderDeleteButton(user.id)}</Media.Heading>
              <p>{user.name}</p>
              <p>{user.email}</p>
              <p>{getRoleName(user.role)}</p>
            </Media.Body>
          </Media>
        </div>
      </Col>
    );
  }

  renderUsers () {
    return (
      <Grid>
        {_.map(this.props.users, (user, i) => <Row key={i}>{this.renderUser(user)}</Row>)}
      </Grid>
    );
  }

  // отрисовка
  render () {
    return (
      <div className="users-page">
        <div className="users-page__title">
          {this.renderCreateButton()}
        </div>
        <div className="users-page__body">
          {this.renderUsers()}
        </div>
      </div>
    );
  }
}

/*
 * Контейнер
 */
const mapStateToProps = function (state: IAppStore): IProps {
  const { users } = state.users.usersPage;
  const { user } = state.main.loginPage;

  return { users, user } as IProps;
};

const mapDispatchToProps = function (dispatch: Redux.Dispatch<IAppStore>): IProps {
  const actions: IUsersPageActions = new UsersPageActions(usersPageService, dispatch);

  return { actions } as IProps;
};

const UsersListPage = withRouter(ReactRedux.connect(mapStateToProps, mapDispatchToProps)(CUsersListPage) as React.ComponentClass<RouteComponentProps<IProps>>);

export { UsersListPage };
